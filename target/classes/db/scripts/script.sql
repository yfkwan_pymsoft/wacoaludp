USE [udp]
GO
/****** Object:  Table [dbo].[tb_outlet]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_outlet](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[outlet_type_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_buyer_order]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_buyer_order](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accessory_remarks] [varchar](500) NULL,
	[accessory_value] [numeric](17, 2) NULL,
	[additional_remarks] [varchar](500) NULL,
	[amount_payable] [varchar](50) NULL,
	[approve_remarks] [varchar](500) NULL,
	[approved_date] [datetime] NULL,
	[bank_name_for_loan] [varchar](50) NULL,
	[bank_name_for_pay] [varchar](50) NULL,
	[bank_office_name] [varchar](50) NULL,
	[buyer_order_no] [varchar](50) NULL,
	[cheque_date] [datetime] NULL,
	[coverage_type] [varchar](50) NULL,
	[credit_card_expiry] [datetime] NULL,
	[customer_bank_acc] [varchar](50) NULL,
	[discount] [numeric](17, 2) NULL,
	[expiry_date] [datetime] NULL,
	[flag] [bit] NULL,
	[grand_total] [numeric](17, 2) NULL,
	[gross_premium] [varchar](50) NULL,
	[gst_code] [varchar](100) NULL,
	[gst_name] [varchar](100) NULL,
	[gst_tax_percent] [numeric](17, 2) NULL,
	[insurance_ap_date] [datetime] NULL,
	[insurance_expiry] [datetime] NULL,
	[insurance_remarks] [varchar](500) NULL,
	[insurance_status] [varchar](100) NULL,
	[issuing_bank_name] [varchar](50) NULL,
	[loading] [varchar](50) NULL,
	[loan_duration] [varchar](50) NULL,
	[loan_required] [bit] NULL,
	[loan_status] [varchar](100) NULL,
	[loan_type] [varchar](50) NULL,
	[make_year] [varchar](50) NULL,
	[ncb_or_ncd] [numeric](17, 2) NULL,
	[nett_premium] [varchar](50) NULL,
	[next_shipment] [varchar](50) NULL,
	[opt_accessory_price] [numeric](17, 2) NULL,
	[opt_accessory_tax] [numeric](17, 2) NULL,
	[order_date] [datetime] NULL,
	[ownership_fee] [numeric](17, 2) NULL,
	[pay_amount] [numeric](17, 2) NULL,
	[payment_date] [datetime] NULL,
	[payment_number] [varchar](50) NULL,
	[payment_remarks] [varchar](500) NULL,
	[policy_type] [varchar](50) NULL,
	[re_road_tax] [bit] NULL,
	[receipt_no] [varchar](50) NULL,
	[refund] [bit] NULL,
	[remarks] [varchar](500) NULL,
	[request_amount] [varchar](50) NULL,
	[required_amount] [numeric](17, 2) NULL,
	[resubmit_remarks] [varchar](500) NULL,
	[road_tax] [numeric](17, 2) NULL,
	[service_package_name] [varchar](50) NULL,
	[service_tax] [varchar](50) NULL,
	[stamp_duty] [varchar](50) NULL,
	[total_coverage] [varchar](50) NULL,
	[total_retail_price] [varchar](50) NULL,
	[total_tax] [numeric](17, 2) NULL,
	[trade_in_price] [numeric](17, 2) NULL,
	[transfer_fee] [numeric](17, 2) NULL,
	[variant_price] [numeric](17, 2) NULL,
	[variant_tax] [numeric](17, 2) NULL,
	[warranty_plan_name] [varchar](50) NULL,
	[bank_id] [numeric](19, 0) NULL,
	[bank_in_bank_id] [numeric](19, 0) NULL,
	[cheque_type_id] [numeric](19, 0) NULL,
	[colour_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[dealer_company_id] [numeric](19, 0) NULL,
	[first_opt_colour_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[insurance_company_id] [numeric](19, 0) NULL,
	[issuing_bank_id] [numeric](19, 0) NULL,
	[payment_mode_id] [numeric](19, 0) NULL,
	[quotation_id] [numeric](19, 0) NULL,
	[sales_executive_id] [numeric](19, 0) NULL,
	[second_opt_colour_id] [numeric](19, 0) NULL,
	[service_id] [numeric](19, 0) NULL,
	[service_package_id] [numeric](19, 0) NULL,
	[service_plan_id] [numeric](19, 0) NULL,
	[trade_in_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[usage_type_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[vehvar_type_id] [numeric](19, 0) NULL,
	[warranty_plan_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_commercial_colour]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_commercial_colour](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[paint_type] [varchar](20) NULL,
	[government_colour_id] [numeric](19, 0) NULL,
	[manufacturer_colour_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_company]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_company](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[acc_outlet_code] [varchar](50) NULL,
	[check_branch_edc] [bit] NULL,
	[check_invoice] [bit] NULL,
	[check_payment] [bit] NULL,
	[dealer_code] [varchar](20) NULL,
	[doc_display_name] [varchar](255) NULL,
	[edaftar_payment] [bit] NULL,
	[fax_no] [varchar](20) NULL,
	[foreign_com] [bit] NULL,
	[initial] [varchar](50) NULL,
	[jpj_required] [bit] NULL,
	[mega_dealer] [bit] NULL,
	[need_request] [bit] NULL,
	[operating_hours] [varchar](255) NULL,
	[own_taxable] [bit] NULL,
	[parent_company_id] [numeric](19, 0) NULL,
	[pdi_approver] [bit] NULL,
	[phone_no] [varchar](20) NULL,
	[restrictive_sell] [bit] NULL,
	[roc_no] [varchar](20) NULL,
	[web] [varchar](255) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_credit_note]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_credit_note](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[available_amount] [numeric](17, 2) NULL,
	[credit_note_date] [datetime] NULL,
	[credit_note_no] [varchar](50) NULL,
	[credit_note_ref] [varchar](50) NULL,
	[gst_amount] [numeric](17, 2) NULL,
	[payment_description] [varchar](255) NULL,
	[price] [numeric](17, 2) NULL,
	[remarks] [varchar](2000) NULL,
	[tax_name] [varchar](255) NULL,
	[tax_percent] [numeric](17, 2) NULL,
	[tax_code] [varchar](50) NULL,
	[total_pay] [numeric](17, 2) NULL,
	[vehicle_reimburse] [bit] NULL,
	[creator_company_id] [numeric](19, 0) NULL,
	[credit_note_type_id] [numeric](19, 0) NULL,
	[dealer_invoice_item_id] [numeric](19, 0) NULL,
	[debit_note_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[outlet_company_id] [numeric](19, 0) NULL,
	[promotion_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NOT NULL,
	[customer_type] [varchar](50) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[preferred_name] [varchar](255) NULL,
	[receive_email] [bit] NULL,
	[receive_sms] [bit] NULL,
	[sensitive] [bit] NULL,
	[special] [bit] NULL,
	[staff] [bit] NULL,
	[status] [bit] NULL,
	[vip] [bit] NULL,
	[company_id] [numeric](19, 0) NULL,
	[customer_category_id] [numeric](19, 0) NULL,
	[language_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[other_ethnicity] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_request_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_request_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[loan_request] [bit] NULL,
	[loan_requried] [bit] NULL,
	[made_year] [int] NULL,
	[mobile_buyer_order_no] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[buyer_order_id] [numeric](19, 0) NULL,
	[colour_id] [numeric](19, 0) NULL,
	[dealer_request_order_id] [numeric](19, 0) NULL,
	[first_opt_colour_id] [numeric](19, 0) NULL,
	[second_opt_colour_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_email]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_email](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[email] [varchar](100) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[preferred] [bit] NULL,
	[update_date] [datetime] NULL,
	[sync_delete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_phone]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_phone](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[phone_no] [varchar](20) NULL,
	[phone_type] [varchar](50) NOT NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[preferred] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_invoice]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_invoice](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[dealer_company_id] [numeric](19, 0) NULL,
	[dealer_request_order_id] [numeric](19, 0) NULL,
	[price_plan_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[create_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_invoice_item]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_invoice_item](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[invoice_amount] [numeric](17, 2) NULL,
	[invoice_no] [varchar](30) NULL,
	[invoice_date] [datetime] NULL,
	[remarks] [varchar](1000) NULL,
	[total_tax] [numeric](17, 2) NULL,
	[dealer_invoice_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_request_order]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_request_order](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[bo_flag] [bit] NULL,
	[by_mobile] [bit] NULL,
	[request_date] [datetime] NULL,
	[request_order_no] [varchar](50) NULL,
	[required_date] [datetime] NULL,
	[submission_date] [datetime] NULL,
	[dealer_company_id] [numeric](19, 0) NULL,
	[disti_company_id] [numeric](19, 0) NULL,
	[requester_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_user]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_user](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[access_code] [varchar](50) NULL,
	[account_no] [varchar](50) NULL,
	[designation] [varchar](100) NULL,
	[email] [varchar](200) NULL,
	[employee_no] [varchar](20) NULL,
	[gender] [varchar](20) NULL,
	[ic_no] [varchar](20) NULL,
	[login_id] [varchar](50) NULL,
	[password] [varchar](100) NULL,
	[phone] [varchar](20) NULL,
	[status] [bit] NULL,
	[username] [varchar](200) NULL,
	[update_date] [datetime] NULL,
	[user_type] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_variant]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_variant](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[ckd_export_duty] [numeric](17, 2) NULL,
	[ckd_import_duty] [numeric](17, 2) NULL,
	[omv_price] [numeric](17, 2) NULL,
	[avg_consumption] [float] NULL,
	[axle_num] [int] NULL,
	[brake_horse_power] [int] NULL,
	[co2_emission] [varchar](255) NULL,
	[door_num] [int] NULL,
	[driver_side] [varchar](50) NULL,
	[engine_capacity] [int] NULL,
	[make_code] [varchar](50) NULL,
	[make_description] [varchar](255) NULL,
	[model_year] [int] NULL,
	[origin_type] [varchar](50) NULL,
	[seat_num] [int] NULL,
	[transmission_type] [varchar](50) NULL,
	[weight] [float] NULL,
	[wheel_base] [int] NULL,
	[fuel_type_id] [numeric](19, 0) NULL,
	[variant_type_id] [numeric](19, 0) NULL,
	[body_style_id] [numeric](19, 0) NULL,
	[body_type_id] [numeric](19, 0) NULL,
	[vehicle_model_id] [numeric](19, 0) NULL,
	[bdm] [float] NULL,
	[bera_kerb] [float] NULL,
	[bg1] [float] NULL,
	[bg10] [float] NULL,
	[bg11] [float] NULL,
	[bg12] [float] NULL,
	[bg2] [float] NULL,
	[bg3] [float] NULL,
	[bg4] [float] NULL,
	[bg5] [float] NULL,
	[bg6] [float] NULL,
	[bg7] [float] NULL,
	[bg8] [float] NULL,
	[bg9] [float] NULL,
	[btm] [float] NULL,
	[btt] [float] NULL,
	[duration_month] [int] NULL,
	[duty] [numeric](17, 2) NULL,
	[excise_remarks] [varchar](500) NULL,
	[highlight_variant] [varchar](10) NULL,
	[length] [int] NULL,
	[manu_warranty_period] [numeric](17, 0) NULL,
	[market_launch_date] [datetime] NULL,
	[metal_body_price] [numeric](17, 2) NULL,
	[normal_body_price] [numeric](17, 2) NULL,
	[publish_to_order] [varchar](10) NULL,
	[remarks] [varchar](500) NULL,
	[tariff_code] [numeric](17, 0) NULL,
	[vehicle_4wd] [varchar](10) NULL,
	[vta_code] [varchar](50) NULL,
	[width] [int] NULL,
	[currency_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[manufacturer_id] [numeric](19, 0) NULL,
	[original_status_id] [numeric](19, 0) NULL,
	[vehicle_use_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accel_fitted] [bit] NULL,
	[ap_no] [varchar](50) NULL,
	[assembly_date] [datetime] NULL,
	[batch_no] [varchar](100) NULL,
	[bdm] [float] NULL,
	[bg1] [float] NULL,
	[bg10] [float] NULL,
	[bg11] [float] NULL,
	[bg12] [float] NULL,
	[bg2] [float] NULL,
	[bg3] [float] NULL,
	[bg4] [float] NULL,
	[bg5] [float] NULL,
	[bg6] [float] NULL,
	[bg7] [float] NULL,
	[bg8] [float] NULL,
	[bg9] [float] NULL,
	[bl_date] [datetime] NULL,
	[booklet_no] [varchar](20) NULL,
	[btm] [float] NULL,
	[btt] [float] NULL,
	[certificate_of_origin] [varchar](50) NULL,
	[chassis_no] [varchar](50) NOT NULL,
	[ckd_verified] [bit] NULL,
	[ckd_verify_remarks] [varchar](500) NULL,
	[commercial_invoice] [varchar](50) NULL,
	[company_own] [bit] NULL,
	[dealer_margin] [numeric](17, 2) NULL,
	[doc_received_date] [datetime] NULL,
	[duty] [numeric](17, 2) NULL,
	[eda] [datetime] NULL,
	[engine_no] [varchar](50) NOT NULL,
	[erd] [datetime] NULL,
	[etd] [datetime] NULL,
	[faulty] [bit] NULL,
	[fs_booklet_no] [varchar](20) NULL,
	[gst_tax] [numeric](17, 2) NULL,
	[immobiliser_code] [varchar](50) NULL,
	[imported_date] [datetime] NULL,
	[inernal_no] [varchar](50) NULL,
	[k1_approve_date] [datetime] NULL,
	[k1_list] [varchar](50) NULL,
	[k1_paid_date] [datetime] NULL,
	[k1_price] [numeric](17, 2) NULL,
	[k1_resit_no] [varchar](50) NULL,
	[k8_approve_date] [datetime] NULL,
	[k8_list] [varchar](50) NULL,
	[kastam_duty_exempted] [bit] NULL,
	[kdrm_ref_no] [varchar](50) NULL,
	[key_no] [varchar](50) NULL,
	[landing_bill] [varchar](50) NULL,
	[lot_no] [varchar](50) NULL,
	[made_year] [int] NULL,
	[manufacturer_certificate] [varchar](500) NULL,
	[manu_warr_period] [numeric](17, 2) NULL,
	[market_value] [numeric](17, 2) NULL,
	[mc_invoice_date] [datetime] NULL,
	[mc_invoice_no] [varchar](50) NULL,
	[mcaicbu] [varchar](50) NULL,
	[mcaickd] [varchar](50) NULL,
	[model_year] [int] NULL,
	[owner_name] [varchar](500) NULL,
	[pack_list_no] [varchar](50) NULL,
	[priority] [bit] NULL,
	[production_date] [datetime] NULL,
	[radio_security_code] [varchar](50) NULL,
	[ready_sale] [bit] NULL,
	[receipt_no] [varchar](50) NULL,
	[registered] [bit] NULL,
	[registration_card_no] [varchar](50) NULL,
	[registration_date] [datetime] NULL,
	[registration_no] [varchar](50) NULL,
	[registration_status] [varchar](50) NULL,
	[remarks] [varchar](500) NULL,
	[remarks1] [varchar](500) NULL,
	[remarks2] [varchar](500) NULL,
	[road_tax_expiry_date] [datetime] NULL,
	[seq_no] [varchar](50) NULL,
	[smk_no] [varchar](50) NULL,
	[status] [bit] NULL,
	[stock_no] [varchar](50) NULL,
	[tariff_code] [numeric](17, 2) NULL,
	[upload_date] [datetime] NULL,
	[warranty_no] [varchar](50) NULL,
	[allocation_status_id] [numeric](19, 0) NULL,
	[commercial_colour_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[location_id] [numeric](19, 0) NULL,
	[delivery_status_id] [numeric](19, 0) NULL,
	[department_id] [numeric](19, 0) NULL,
	[dest_country_id] [numeric](19, 0) NULL,
	[distributor_id] [numeric](19, 0) NULL,
	[doc_release_status_id] [numeric](19, 0) NULL,
	[kastam_status_id] [numeric](19, 0) NULL,
	[location_type_id] [numeric](19, 0) NULL,
	[miti_status_id] [numeric](19, 0) NULL,
	[new_delivery_id] [numeric](19, 0) NULL,
	[old_delivery_id] [numeric](19, 0) NULL,
	[origin_company_id] [numeric](19, 0) NULL,
	[pay_voucher_status_id] [numeric](19, 0) NULL,
	[pdi_status_id] [numeric](19, 0) NULL,
	[reimbursement_status_id] [numeric](19, 0) NULL,
	[sublet_po_status_id] [numeric](19, 0) NULL,
	[tagged_company_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[body_style_id] [numeric](19, 0) NULL,
	[body_type_id] [numeric](19, 0) NULL,
	[model_id] [numeric](19, 0) NULL,
	[vehicle_status_id] [numeric](19, 0) NULL,
	[vehicle_type_id] [numeric](19, 0) NULL,
	[endorsement_status_id] [numeric](19, 0) NULL,
	[kdrm_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[company_own_type_id] [numeric](19, 0) NULL,
	[company_location_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_allocation]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_allocation](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[expiry_date] [datetime] NULL,
	[loan_required] [bit] NULL,
	[remarks] [varchar](1000) NULL,
	[dealer_id] [numeric](19, 0) NULL,
	[from_company_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[OUTSTANDING_BOOKING]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[OUTSTANDING_BOOKING] AS
select
	distinct DATEDIFF(DD,reqorder.submission_date, GETDATE()) as aging,
	variant.name as variant, colour.name as colour, vehicle.made_year, bo.buyer_order_no, 
	bo.order_date as "buy_order_date", reqorder.request_order_no as "stock_request_no", 
	company.name as outlet, customer.name as "Customer Name", cusphone.phone_no as "customer_phone", 
	cusemail.email as "customer_email", salesperson.username as "sales_exec_name", 
	salesperson.phone as "sales_exec_phone", cusemail.preferred
from 
	tb_buyer_order bo
	join tb_request_detail reqdetail on reqdetail.buyer_order_id = bo.id
	join tb_dealer_request_order reqorder on reqorder.id = reqdetail.dealer_request_order_id
	left join tb_variant variant on variant.id = reqdetail.variant_id
	left join tb_customer customer on customer.id = bo.customer_id
	left join tb_vehicle vehicle on vehicle.id = bo.vehicle_id
	left join tb_commercial_colour colour on colour.id = reqdetail.colour_id
	left join tb_company company on company.id = bo.dealer_company_id
	left join tb_user salesperson on salesperson.id = bo.sales_executive_id
	left join tb_customer_email cusemail on cusemail.customer_id = customer.id and cusemail.preferred = 1
	left join tb_customer_phone cusphone on cusphone.customer_id = customer.id and cusphone.preferred = 1
where 
	--bo.buyer_order_no = 'ADK-BO000616'
	reqorder.workflow_status_id in (163,168)
	and reqorder.disti_company_id = 1
	and reqdetail.loan_request = 0
	--and reqorder.request_date>=CONVERT(varchar(10),DATEADD(dd,-120,GETDATE()),120)
	--and reqorder.request_date<=GETDATE()	
	and exists (
		select bo1.id from tb_buyer_order bo1 
		where
			bo1.id = reqdetail.buyer_order_id  
			and bo1.workflow_status_id not in (89, 91, 127) 
	)
	and not exists (
		select 1 from tb_allocation al
		where 
			al.workflow_status_id = 46 
			and al.vehicle_id = reqdetail.vehicle_id
	)
	and not exists (
		select di.vehicle_id from tb_dealer_invoice di
			left join tb_dealer_invoice_item difi on difi.dealer_invoice_id=di.id
		where 
			di.workflow_status_id = 117 
			and di.vehicle_id = reqdetail.vehicle_id
			and not exists (
				select cn.id from tb_credit_note cn 
				where 
					cn.dealer_invoice_item_id = difi.id 
					and cn.workflow_status_id = 40
					and cn.credit_note_type_id = 1 
			)
	)
	and exists (
		select 1 from tb_company com where reqorder.dealer_company_id = com.id 
			and exists (
			select 1 from tb_outlet oot where oot.company_id=com.id
		)
	)
GO
/****** Object:  Table [dbo].[tb_payment_voucher]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_voucher](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[approved_date] [datetime] NULL,
	[bank_name_for_pay] [varchar](200) NULL,
	[cash_in_date] [datetime] NULL,
	[cheque_date] [datetime] NULL,
	[expiry_date] [datetime] NULL,
	[field] [varchar](50) NULL,
	[payment_date] [datetime] NULL,
	[payment_number] [varchar](50) NULL,
	[reference_no] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[voucher_no] [varchar](50) NULL,
	[approver_id] [numeric](19, 0) NULL,
	[bank_id] [numeric](19, 0) NULL,
	[cheque_type_id] [numeric](19, 0) NULL,
	[credit_note_id] [numeric](19, 0) NULL,
	[cust_credit_note_id] [numeric](19, 0) NULL,
	[outlet_id] [numeric](19, 0) NULL,
	[payment_mode_id] [numeric](19, 0) NULL,
	[sales_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[business_process_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_pdi]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_pdi](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[approve_date] [datetime] NULL,
	[company_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_workflow_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_workflow_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[description] [varchar](500) NULL,
	[module_name] [varchar](50) NULL,
	[status] [varchar](100) NULL,
	[status_desc] [varchar](500) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_payment_voucher]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_payment_voucher](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[amount] [numeric](17, 2) NULL,
	[payment_voucher_id] [numeric](19, 0) NOT NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_delivery_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_delivery_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[added_to_inv] [bit] NULL,
	[added_to_pl] [bit] NULL,
	[delivery_date] [datetime] NULL,
	[delivery_order_no] [varchar](50) NULL,
	[status] [bit] NULL,
	[delivery_id] [numeric](19, 0) NOT NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[INVOICE_SALES]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[INVOICE_SALES] AS
select 
	distinct variant.name as model, vehicle.chassis_no, vehicle.engine_no, colour.name as colour, 
	vehicle.production_date, dealer.name as "allocated to", item.invoice_no, item.invoice_amount,
	item.invoice_date, 
	--creditnote.credit_note_no, creditnote.credit_note_date,
	previous_invoice_no = COALESCE((
		select 
			top 1 difi2.invoice_no
		from 
			tb_dealer_invoice_item as difi2
			left outer join tb_dealer_invoice as di2 on (difi2.dealer_invoice_id = di2.id)
		where 
			di2.vehicle_id = invoice.vehicle_id
			and difi2.id < item.id 
		order by 
			difi2.id desc 
	),''),
	previous_invoice_date = COALESCE((
		select 
			top 1 CONVERT(VARCHAR(10), difi2.invoice_date, 120) 
		from 
			tb_dealer_invoice_item as difi2
			left outer join tb_dealer_invoice as di2 on (difi2.dealer_invoice_id = di2.id)
		where 
			di2.vehicle_id = invoice.vehicle_id
			and difi2.id < item.id 
		order by 
			difi2.id desc 
	),''), 
	credit_note_no = COALESCE((
		select 
			top 1 cn.credit_note_no
		from 
			tb_credit_note cn
			left join tb_dealer_invoice_item cndi on cndi.id = cn.dealer_invoice_item_id
		where 
			cndi.id = item.id 
			and cn.workflow_status_id = 40
		order by 
			cn.id desc 
	),''),
	credit_note_date = COALESCE((
		select 
			top 1 CONVERT(VARCHAR(10), cn.credit_note_date, 120) 
		from 
			tb_credit_note cn
			left join tb_dealer_invoice_item cndi on cndi.id = cn.dealer_invoice_item_id
		where 
			cndi.id = item.id 
			and cn.workflow_status_id = 40
		order by 
			cn.id desc 
		),''),
		(case 
			when variant.origin_type = 'CKD' -- for CKD type
				then (
					case 
						when vehicle.status = 1 and vehicle.ckd_verified <> 1 
						then 'Total Buy Off'
							
						--when exists(
						--	select id 
						--	from tb_company_type_ref comref
						--	where comref.company_type_id=3 
						--		and comref.company_id = vehicle.company_id)
						--		and vehicle.status = 1 and vehicle.ckd_verified = 1 
								--and not exists(
								--	select endVehicleID from tb_Endorse e where e.endVehicleID=b.id
								--)
						--then 'Not Endorsed, at Assembler'
							
						--when exists(
						--	select 1 
						--	from tb_nik_records ni 
						--	where ni.nikStatus=570004 and ni.nikVehicleId=b.id) and b.statusId=802901 
						--		and exists(
						--			select id 
						--			from tb_comCompanyType 
						--			where companyTypeID=3 and companyID=b.ownComOutID) 
						--		and exists(
						--			select 1 
						--			from tb_Endorse ed 
						--			where ed.endVehicleID=b.id and ed.endStatus = 18)
						--then 'Endorsed & NIK, at Assembler'
							
						when vehicle.status = 1 and vehicle.location_type_id=1 
							and exists(
									select 1 
									from 
										tb_vehicle_pdi pdi
										left join tb_workflow_status pdiws on pdiws.id = pdi.workflow_status_id
									where 
										pdi.vehicle_id = vehicle.id 
										and pdiws.status<>'Approved' 
										and pdiws.status<>'Cancelled' 
										and pdi.workflow_status_id is not null)
						then 'WIP'
							
						--when vehicle.status = 1 and vehicle.location_type_id=1  
						--	and not exists(
						--		select pdi.id 
						--		from tb_vehicle_pdi pdi 
						--		where 
						--			pdi.vehicle_id=vehicle.id 
						--			and pdi.workflow_status_id is not null) 
									--and exists(
									--	select 1 from tb_nik_records ni 
									--	where ni.nikStatus=570004 and ni.nikVehicleId=b.id) 
									--		and exists(
									--			select 1 
									--			from tb_Endorse ed 
									--			where ed.endVehicleID=b.id and ed.endStatus = 18)
						--then 'Endorsed & NIK, at PDI'
							
						when vehicle.accel_fitted=1 and vehicle.location_type_id=1 
							and exists(
								select 1 
								from tb_vehicle_pdi vpdi  
									left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
								where vpdi_wfs.status = 'Approved' 
								and vpdi.vehicle_id = vehicle.id
							) 
						then 'Ready, at PDI'
							
						when vehicle.accel_fitted=1  and vehicle.location_type_id <> 1 
							and  exists(
								select 1 
								from tb_vehicle_pdi vpdi  
									left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
								where vpdi_wfs.status = 'Approved' 
								and vpdi.vehicle_id = vehicle.id
							)
							  
						then 'Ready, at Outlet' 
						else ''
				end 
			)else(    -- for CBU type   
				case 
				when vehicle.status = 1 and (vehicle.location_type_id<>2 or vehicle.location_type_id is null) 
					and not exists(
						select 1 
						from tb_delivery_detail dd 
						where dd.vehicle_id=vehicle.id
						)
				then 'Shipped'
							
				when vehicle.status = 1 and vehicle.location_type_id=2
				then 'Arrived at Port'
							
				when vehicle.status = 1 and vehicle.location_type_id=7 
					and not exists ( 
						select 1 
						from tb_vehicle_payment_voucher pvv 
							left join tb_payment_voucher pv on pv.id=pvv.payment_voucher_id
							left join tb_workflow_status pvws on pvws.id=pv.workflow_status_id
						where 
							pvv.vehicle_id= vehicle.id 
							--and pv.processid=1 
							and pvws.status='Payment Received' 
							and pvws.status is not null)
				then 'Bonded'
							
				when vehicle.status = 1 and vehicle.location_type_id=1 
					and exists(
						select 1 
						from tb_vehicle_pdi pdi
							left join tb_workflow_status pdiws on pdiws.id=pdi.workflow_status_id
						where 
							pdi.vehicle_id=vehicle.id 
							and pdiws.status<>'Approved' 
							and pdiws.status<>'Cancelled' 
							and pdi.workflow_status_id is not null)
				then 'WIP'
							
				when vehicle.location_type_id=1 and vehicle.status = 1
					and not exists(
						select pdi.id 
						from tb_vehicle_pdi pdi 
						where 
							pdi.vehicle_id=vehicle.id 
							and pdi.workflow_status_id is not null)
				then 'Receive at PDI'
							
				when vehicle.accel_fitted=1 and vehicle.location_type_id = 1 
					and exists(
						select * 
						from tb_vehicle_pdi vpdi  
							left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
						where 
							vpdi_wfs.status = 'Approved' 
							and vpdi.vehicle_id = vehicle.id
					) 
							   
				then 'Ready, at PDI'
							
				when vehicle.accel_fitted=1 and vehicle.location_type_id <> 1 
					and  exists(
						select * 
						from tb_vehicle_pdi vpdi  
							left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
						where vpdi_wfs.status = 'Approved' and vpdi.vehicle_id = vehicle.id
					)
							       
				then 'Ready, at Outlet' 
							
				else ''
				end
			)
			end) as pdistatus,
		isnull(vehicle.ckd_verify_remarks,'') as  ckd_verify_remarks
from 
	tb_vehicle vehicle
	--tb_dealer_invoice_item item 
    --left join tb_dealer_invoice invoice on item.dealer_invoice_id=invoice.id
    left join tb_dealer_invoice invoice on invoice.vehicle_id = vehicle.id
 	left join tb_dealer_invoice_item item on item.dealer_invoice_id = invoice.id
	left join tb_company dealer on invoice.dealer_company_id=dealer.id
    --left join tb_vehicle vehicle on invoice.vehicle_id=vehicle.id
    left join tb_workflow_status workstatus on vehicle.pdi_status_id=workstatus.id
    left join tb_buyer_order buyer on vehicle.id=buyer.vehicle_id
    left join tb_variant variant on vehicle.variant_id=variant.id
    left join tb_commercial_colour colour on vehicle.commercial_colour_id=colour.id
	left join tb_credit_note creditnote on creditnote.dealer_invoice_item_id = creditnote.id
where 
	invoice.workflow_status_id = 117
GO
/****** Object:  Table [dbo].[tb_vehicle_edaftar]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_edaftar](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[aoc] [varchar](50) NULL,
	[aoc_num] [varchar](50) NULL,
	[application_date] [datetime] NULL,
	[bdm] [varchar](50) NULL,
	[bg1] [varchar](50) NULL,
	[bg10] [varchar](50) NULL,
	[bg11] [varchar](50) NULL,
	[bg12] [varchar](50) NULL,
	[bg2] [varchar](50) NULL,
	[bg3] [varchar](50) NULL,
	[bg4] [varchar](50) NULL,
	[bg5] [varchar](50) NULL,
	[bg6] [varchar](50) NULL,
	[bg7] [varchar](50) NULL,
	[bg9] [varchar](50) NULL,
	[owner_dob] [varchar](50) NULL,
	[body_type] [varchar](50) NULL,
	[btm] [varchar](50) NULL,
	[btt] [varchar](50) NULL,
	[chassis_no] [varchar](50) NULL,
	[engine_capacity] [varchar](50) NULL,
	[engine_no] [varchar](50) NULL,
	[colour] [varchar](50) NULL,
	[error_desc] [varchar](500) NULL,
	[curb_weight] [varchar](50) NULL,
	[e_4wd] [varchar](50) NULL,
	[edaftar_flag] [int] NULL,
	[error_status] [int] NULL,
	[finance_co] [varchar](500) NULL,
	[finance_reference] [varchar](50) NULL,
	[fuel_type] [varchar](50) NULL,
	[owner_gender] [varchar](50) NULL,
	[insurance_co] [varchar](500) NULL,
	[insurance_end_date] [varchar](50) NULL,
	[insurance_id] [varchar](50) NULL,
	[insurance_start_date] [varchar](50) NULL,
	[made_year] [varchar](50) NULL,
	[make] [varchar](50) NULL,
	[model] [varchar](50) NULL,
	[original_status] [varchar](50) NULL,
	[address1] [varchar](500) NULL,
	[address2] [varchar](500) NULL,
	[address3] [varchar](500) NULL,
	[owner_category] [varchar](500) NULL,
	[owner_city_code] [varchar](50) NULL,
	[owner_id] [varchar](50) NULL,
	[owner_name1] [varchar](500) NULL,
	[owner_name2] [varchar](500) NULL,
	[owner_name3] [varchar](500) NULL,
	[owner_postcode] [varchar](50) NULL,
	[seat_num] [varchar](50) NULL,
	[usage] [varchar](500) NULL,
	[vehicle_type] [varchar](20) NOT NULL,
	[vta_code] [varchar](50) NULL,
	[ref_id] [numeric](19, 0) NULL,
	[bg8] [varchar](50) NULL,
	[finance_company] [varchar](500) NULL,
	[insurance_company] [varchar](500) NULL,
	[owner_address1] [varchar](500) NULL,
	[owner_address2] [varchar](500) NULL,
	[owner_address3] [varchar](500) NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[sales_order_id] [numeric](19, 0) NULL,
	[user_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[status_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[DEALER_FREE_STOCK]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DEALER_FREE_STOCK] AS
select
	distinct vehicle.chassis_no, company.name as outlet, 
	variant.code as variant, vehicle.engine_no,
	colour.name as colour, 
	vehicle.production_date, vehicle.made_year,
	DATEDIFF(DD,dlrinv.create_date, GETDATE()) as aging
from 
	tb_vehicle vehicle
	left join tb_dealer_invoice dlrinv on dlrinv.vehicle_id = vehicle.id
		and dlrinv.id in (select MAX(a.id) from tb_dealer_invoice a group by a.vehicle_id)
	--left join tb_dealer_invoice_item dlrinvitem on dlrinvitem.dealer_invoice_id = dlrinv.id
	left join tb_variant variant on  variant.id = vehicle.variant_id 
	left join tb_company company on company.id = vehicle.company_id
	left join tb_commercial_colour colour on colour.id = vehicle.commercial_colour_id
	--left join tb_vehicle_model mo on mo.id=variant.vehicle_model_id
	--left join tb_vehicle_series se on se.id=mo.vehicle_series_id
	--left join tb_vehicle_brand br on br.id=mo.vehicle_brand_id
	--left join tb_vehicle_edaftar vehdaf on vehdaf.vehicle_id = vehicle.id 
where
	vehicle.location_type_id = 5
	and ( vehicle.registration_status <> 2007 or vehicle.registration_status is null )
	--and vehicle.company_own <> 1
	--and variant.publish_to_order = 'Yes'
	--and vehdaf.id is null
	and exists (
		select 
			di.vehicle_id 
		from 
			tb_dealer_invoice di
			left join tb_dealer_invoice_item difi on difi.dealer_invoice_id=di.id
		where 
			di.workflow_status_id = 117 and di.vehicle_id= vehicle.id
			and not exists (
				select 
					cn.id 
				from 
					tb_credit_note cn 
				where 
					cn.dealer_invoice_item_id = difi.id 
					and cn.workflow_status_id = 40
					and cn.credit_note_type_id = 1
			) 
	)
	and not exists(
      select 1 from tb_vehicle_edaftar vehdaf where vehdaf.vehicle_id = vehicle.id 
    )
	and exists (
		select 1 from tb_company com where dlrinv.dealer_company_id = com.id 
		and exists (
			select 1 from tb_outlet oot where oot.company_id=com.id
		)
	)
GO
/****** Object:  Table [dbo].[tb_vehicle_model]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_model](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[chassis_no_length] [varchar](100) NULL,
	[chassis_prefix] [varchar](500) NULL,
	[engine_no_length] [varchar](100) NULL,
	[engine_no_prefix] [varchar](500) NULL,
	[government_name] [varchar](100) NULL,
	[kastam_name] [varchar](100) NULL,
	[tariff] [varchar](100) NULL,
	[upload_name] [varchar](100) NULL,
	[vehicle_category_id] [numeric](19, 0) NULL,
	[vehicle_series_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_series]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_series](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[status] [bit] NULL,
	[vehicle_brand_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_company_own_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_company_own_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[name] [varchar](200) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_department]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_department](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[address_1] [varchar](500) NULL,
	[address_2] [varchar](500) NULL,
	[address_3] [varchar](500) NULL,
	[address_4] [varchar](500) NULL,
	[address_5] [varchar](500) NULL,
	[postcode] [varchar](50) NULL,
	[initial] [varchar](50) NULL,
	[parent_department_id] [numeric](19, 0) NULL,
	[city_id] [numeric](19, 0) NULL,
	[country_id] [numeric](19, 0) NULL,
	[region_id] [numeric](19, 0) NULL,
	[state_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_brand]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_brand](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[status] [bit] NULL,
	[custom_name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[COMPANY_OWN_VEHICLES]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[COMPANY_OWN_VEHICLES] AS
select distinct 
		com.name as outlet,
		variant.name + ' ( '+variant.code+' )' as variant,
		c.chassis_no as chassis_no, 
		c.engine_no as engine_no, 
		co.name as colour,
		COALESCE(CONVERT(VARCHAR(10), c.upload_date, 120),'') as upload_date,
		c.made_year, st.name as company_type,
		isnull(d.name,'') as custodian, c.owner_name, c.ready_sale
		from 
			tb_vehicle c
			left join tb_company com on com.id = c.company_id
			left join tb_variant variant on variant.id = c.variant_id
			left join tb_commercial_colour co on co.id = c.commercial_colour_id
			left join tb_vehicle_model mo on mo.id = variant.vehicle_model_id
			left join tb_vehicle_series se on se.id = mo.vehicle_series_id
			left join tb_vehicle_brand br on br.id = se.vehicle_brand_id
			left join tb_company_own_type st on st.id = c.company_own_type_id
			left join tb_department d on d.id = c.department_id
			left join tb_buyer_order bo on bo.vehicle_id = c.id
			left join tb_workflow_status wstatus on wstatus.id = bo.workflow_status_id
		where c.company_own = 1
		and variant.publish_to_order = 'yes'
GO
/****** Object:  Table [dbo].[tb_location]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_location](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[address_1] [varchar](500) NULL,
	[address_2] [varchar](500) NULL,
	[address_3] [varchar](500) NULL,
	[address_4] [varchar](500) NULL,
	[address_5] [varchar](500) NULL,
	[postcode] [varchar](50) NULL,
	[fax_no] [varchar](20) NULL,
	[phone_no] [varchar](20) NULL,
	[city_id] [numeric](19, 0) NULL,
	[country_id] [numeric](19, 0) NULL,
	[region_id] [numeric](19, 0) NULL,
	[state_id] [numeric](19, 0) NULL,
	[location_type_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[PDI_READY]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PDI_READY] AS
select 
	distinct co.name as outlet,variant.name variant,
	--convert(VARCHAR(10),pdi.lastUpdateDate,120) as PDIApprovedDate,
	--convert(VARCHAR(10),difi.invoice_date,120) as invoiceDate,ve.chassis_no as chassis, 
	--(select datediff(d,difi.invoiceDate,getDate()) ) as agingDays,  
	--pdi_approved_date = (select max(res.inspection_date) 
	--	from tb_vehicle_pdi_result res 
	--	where res.vehicle_pdi_id = pdi.id),
	ve.chassis_no as chassis, pdi.approve_date as pdi_approved_date,
	difi.invoice_date, 
	DATEDIFF(DD,difi.invoice_date, GETDATE()) as aging,
	case
	when ve.location_type_id = 3 or ve.location_type_id = 5 
		then (select comLo.name from tb_company comLo where comLo.id = ve.company_location_id)
		else loc.name
		end as location,  
	'edaftarStatus' = case
		when ve.registration_status = 2007 then 'Registered'
		else ''
		end
	from tb_vehicle ve
	left join tb_variant variant on variant.id=ve.variant_id
	--left join tb_allocation allocation on allocation.vehicle_id=ve.id
	left join tb_dealer_invoice di on di.vehicle_id=ve.id
	--left join tb_workflow_status diws on di.workflow_status_id=diws.id 
	left join tb_company co on co.id = di.dealer_company_id
	left join tb_dealer_invoice_item difi on difi.dealer_invoice_id=di.id
	left join tb_vehicle_pdi pdi on pdi.vehicle_id=ve.id
	--left join tb_vehicle_pdi_result pdiresult on pdiresult.vehicle_pdi_id=pdi.id
	--left join tb_Company comLo on comLo.id = ve.currentLocation 
	--left join tb_location_type loctype on loctype.id = ve.location_type_id 
	left join tb_location loc on loc.id = ve.location_id
	left join tb_commercial_colour g on ve.commercial_colour_id=g.id
	left join tb_vehicle_model mo on mo.id = variant.vehicle_model_id
	left join tb_vehicle_series se on se.id = mo.vehicle_series_id
	left join tb_vehicle_brand br on br.id = se.vehicle_brand_id
	where
	ve.location_type_id = 1 
	and ve.accel_fitted = 1
	-- Approved 
	and pdi.workflow_status_id = 16
	-- Generated
	and di.workflow_status_id = 117
	and not exists (
		select cn.id 
		from 
			tb_credit_note cn 
		where 
			cn.dealer_invoice_item_id=difi.id 
			-- Approved
			and cn.workflow_status_id = 40
			and cn.credit_note_type_id=1 
	) 
GO
/****** Object:  Table [dbo].[tb_city]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_city](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[state_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_address]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_address](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[address_1] [varchar](500) NULL,
	[address_2] [varchar](500) NULL,
	[address_3] [varchar](500) NULL,
	[address_4] [varchar](500) NULL,
	[address_5] [varchar](500) NULL,
	[postcode] [varchar](50) NULL,
	[address_type] [varchar](50) NOT NULL,
	[city_id] [numeric](19, 0) NULL,
	[country_id] [numeric](19, 0) NULL,
	[region_id] [numeric](19, 0) NULL,
	[state_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
	[preferred_billing] [bit] NULL,
	[preferred_mailing] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_use]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_use](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](20) NULL,
	[description] [varchar](500) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_category]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_category](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_company]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_company](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[roc_no] [varchar](100) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[gender] [varchar](20) NULL,
	[ethnicity_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sales]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sales](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[discount] [numeric](17, 2) NULL,
	[loan_amount] [numeric](17, 2) NULL,
	[payment_status] [varchar](100) NULL,
	[remarks] [varchar](2000) NULL,
	[total_vehicle_amount] [numeric](17, 2) NULL,
	[buyer_order_id] [numeric](19, 0) NULL,
	[colour_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[dealer_id] [numeric](19, 0) NULL,
	[invoice_id] [numeric](19, 0) NULL,
	[quotation_id] [numeric](19, 0) NULL,
	[sales_executive_id] [numeric](19, 0) NULL,
	[sales_order_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_person]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_person](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[birth_date] [datetime] NULL,
	[first_name] [varchar](100) NULL,
	[gender] [varchar](20) NULL,
	[ic_no] [varchar](20) NULL,
	[initials] [varchar](100) NULL,
	[surname] [varchar](100) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[ethnicity_id] [numeric](19, 0) NULL,
	[marriage_status_id] [numeric](19, 0) NULL,
	[saluation_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_registration]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_registration](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[claim_no] [varchar](50) NULL,
	[finance_company] [varchar](200) NULL,
	[old_chassis_no] [varchar](50) NULL,
	[old_engine_no] [varchar](50) NULL,
	[old_registration_no] [varchar](50) NULL,
	[registration_card_no] [varchar](50) NULL,
	[registration_date] [datetime] NULL,
	[registration_no] [varchar](50) NULL,
	[use_old] [bit] NULL,
	[vehicle_use_code] [varchar](200) NULL,
	[sales_order_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_state]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_state](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[region_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_ethnicity]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_ethnicity](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_insurance_company]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_insurance_company](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](500) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[EDAFTAR_SUBMIT]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[EDAFTAR_SUBMIT] AS
select
	case when  
			veh.registration_no = '' 
		 then 
			''
		 else 
			veh.registration_no
	end as registration_no,
	
	cbu.application_date as submission_date,
	veh.registration_date as registration_date,
	co.name as company_name,
	va.name + ' (' + va.code + ')' as variant,
	cbu.chassis_no as chassis_no,
	cbu.owner_name1 as owner_name,
	cbu.owner_address1 as owner_address1,
	cbu.owner_address2 as owner_address2,
	reg.finance_company as finance_company,
	ca.postcode as postcode,
	st.name as state_name,
	ic.name as insurance_company,
	u.username as sales_executive_name,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.ic_no
		else cc.roc_no
	  end
	) as customer_ic,
	cusCate.description,
	(select top 1 cp2.phone_no 
		from tb_customer_phone cp2 
		where cp2.customer_id = cu.id
		and cp2.preferred = 1) as customer_phone,
	vuse.description as usage_type,
	cemail.email as email_addr,
	cbu.owner_address3  as owner_address3,
	city.name  as cityName,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.gender
		else cc.gender
	  end
	) as genderName,
	(case 
		when (cperson.birth_date is not null) 
		then DATEDIFF(YY, cperson.birth_date, GETDATE())
		else 0 
	end) age,
	(case 
		when race.id = 5 
		then isnull(cu.other_ethnicity,'')
		else isnull(race.name,'') 
	end )as race_name
	from tb_vehicle veh 
	left outer join tb_vehicle_edaftar cbu on veh.id = cbu.vehicle_id
	left outer join tb_sales si on si.vehicle_id = veh.id 
		and si.id in (select MAX(s1.id) from tb_sales s1 group by s1.vehicle_id)
	left join tb_workflow_status wfs on wfs.id=si.workflow_status_id
	left join tb_variant va on va.id=veh.variant_id
	left join tb_vehicle_model mo on mo.id=va.vehicle_model_id
	left join tb_vehicle_series se on se.id=mo.vehicle_series_id
	left join tb_vehicle_brand br on br.id = se.vehicle_brand_id  																		   
	left join tb_company co on co.id = si.dealer_id
	left join tb_buyer_order bo on bo.id=si.buyer_order_id 
	left join tb_insurance_company ic on ic.id=bo.insurance_company_id
	left join tb_customer cu on cu.id=bo.customer_id
	left join tb_customer_person cperson on cperson.customer_id = cu.id and cu.customer_type = 'Individual'
	left join tb_customer_company cc on cc.customer_id = cu.id and cu.customer_type = 'Company'
	left join tb_ethnicity race on race.id = cperson.ethnicity_id
	left join tb_customer_address ca on ca.customer_id=cu.id and ca.id = (select top 1 ca1.id from tb_customer_address ca1 where ca1.customer_id = cu.id order by ca1.preferred_billing desc)
	left join tb_state st on st.id = ca.state_id 
	left join tb_customer_email  cemail on cemail.customer_id=cu.id and cemail.id = (select top 1 ce1.id from tb_customer_email ce1 where ce1.customer_id = cu.id order by ce1.preferred desc)
	left join tb_city city on city.id=ca.city_id
	left join tb_vehicle_registration reg on reg.vehicle_id= cbu.vehicle_id  
		and reg.id in ( select MAX(id) from tb_vehicle_registration vr group by vr.vehicle_id ) 
	left join tb_user u on u.id = si.sales_executive_id
	left join tb_customer_category cusCate on cusCate.id = cu.customer_category_id
	left join tb_customer_phone cp on cp.customer_id=cu.id and cp.id = (select top 1 cp1.id from tb_customer_phone cp1 where cp1.customer_id = cu.id order by cp1.preferred desc)
	left join tb_vehicle_use vuse on vuse.code= cbu.usage	 
	where cbu.id in ( 
				select MAX(ve1.id) 
				from tb_vehicle_edaftar ve1 
				group by ve1.vehicle_id ) 
		and ((cbu.status_id = 192 and cbu.edaftar_flag = 0) or veh.registration_status=2007) 							
		and veh.status=1
union all
select
	case when  
			veh.registration_no = '' 
		 then 
			''
		 else 
			veh.registration_no
	end as registration_no,
	
	cbu.application_date as submission_date,
	veh.registration_date as registration_date,
	co.name as company_name,
	va.name + ' (' + va.code + ')' as variant,
	cbu.chassis_no as chassis_no,
	cbu.owner_name1 as owner_name,
	cbu.owner_address1 as owner_address1,
	cbu.owner_address2 as owner_address2,
	reg.finance_company as finance_company,
	ca.postcode as postcode, 
	st.name as state_name,
	ic.name as insurance_company,
	u.username as sales_executive_name,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.ic_no
		else cc.roc_no
	  end
	) as customer_ic,
	cusCate.description,
	(select top 1 cp2.phone_no 
		from tb_customer_phone cp2 
		where cp2.customer_id = cu.id
		and cp2.preferred = 1) as customer_phone,
	vuse.description as usage_type,
	cemail.email as email_addr,
	cbu.owner_address3  as owner_address3,
	city.name  as cityName,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.gender
		else cc.gender
	  end
	) as genderName,
	(case 
		when (cperson.birth_date is not null) 
		then DATEDIFF(YY, cperson.birth_date, GETDATE())
		else 0 
	end) age,
	(case 
		when race.id = 5 
		then isnull(cu.other_ethnicity,'')
		else isnull(race.name,'') 
	end )as race_name
	from tb_vehicle veh 
	left outer join tb_vehicle_edaftar cbu on veh.id = cbu.vehicle_id
	left outer join tb_sales si on si.vehicle_id = veh.id 
		and si.id in (select MAX(s1.id) from tb_sales s1 group by s1.vehicle_id)
	left join tb_workflow_status wfs on wfs.id=si.workflow_status_id
	left join tb_variant va on va.id=veh.variant_id
	left join tb_vehicle_model mo on mo.id=va.vehicle_model_id
	left join tb_vehicle_series se on se.id=mo.vehicle_series_id
	left join tb_vehicle_brand br on br.id = se.vehicle_brand_id  																		   
	left join tb_company co on co.id = si.dealer_id
	left join tb_buyer_order bo on bo.id=si.buyer_order_id 
	left join tb_insurance_company ic on ic.id=bo.insurance_company_id
	left join tb_customer cu on cu.id=bo.customer_id
	left join tb_customer_person cperson on cperson.customer_id = cu.id and cu.customer_type = 'Individual'
	left join tb_customer_company cc on cc.customer_id = cu.id and cu.customer_type = 'Company'
	left join tb_ethnicity race on race.id = cperson.ethnicity_id
	left join tb_customer_address ca on ca.customer_id=cu.id and ca.id = (select top 1 ca1.id from tb_customer_address ca1 where ca1.customer_id = cu.id order by ca1.preferred_billing desc)
	left join tb_state st on st.id = ca.state_id 
	left join tb_customer_email  cemail on cemail.customer_id=cu.id and cemail.id = (select top 1 ce1.id from tb_customer_email ce1 where ce1.customer_id = cu.id order by ce1.preferred desc)
	left join tb_city city on city.id=ca.city_id
	left join tb_vehicle_registration reg on reg.vehicle_id= cbu.vehicle_id  
		and reg.id in ( select MAX(id) from tb_vehicle_registration vr group by vr.vehicle_id ) 
	left join tb_user u on u.id = si.sales_executive_id
	left join tb_customer_category cusCate on cusCate.id = cu.customer_category_id
	left join tb_customer_phone cp on cp.customer_id=cu.id and cp.id = (select top 1 cp1.id from tb_customer_phone cp1 where cp1.customer_id = cu.id order by cp1.preferred desc)
	left join tb_vehicle_use vuse on vuse.code= cbu.usage	 
	where veh.registration_status = 2007		
		and veh.status=1
		and not exists (select cbu.id from tb_vehicle_edaftar cbu where cbu.vehicle_id = veh.id)

GO
/****** Object:  Table [dbo].[tb_vehicle_excise]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_excise](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[chassis_num] [varchar](50) NULL,
	[cni_designation] [varchar](100) NULL,
	[cni_ic_no] [varchar](100) NULL,
	[cni_prepare_by] [varchar](200) NULL,
	[cni_remarks] [varchar](100) NULL,
	[custom_sn_out] [varchar](100) NULL,
	[ex8_printed] [bit] NULL,
	[ex8_ref_no] [varchar](100) NULL,
	[intd_removal_date] [datetime] NULL,
	[seq_running_no] [varchar](100) NULL,
	[submission_date] [datetime] NULL,
	[submitted] [bit] NULL,
	[successful] [bit] NULL,
	[successful_date] [datetime] NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_country]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_country](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_com_invoice]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_com_invoice](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[account_no] [varchar](50) NULL,
	[bank_name] [varchar](100) NULL,
	[discount] [numeric](18, 2) NULL,
	[final_total_price] [numeric](18, 2) NULL,
	[invoice_date] [datetime] NULL,
	[invoice_no] [varchar](50) NULL,
	[paid_amount] [numeric](18, 2) NULL,
	[payment_due_date] [datetime] NULL,
	[payment_mode] [varchar](50) NULL,
	[remarks] [varchar](500) NULL,
	[swift_code] [varchar](50) NULL,
	[tax] [numeric](18, 2) NULL,
	[tax_code] [varchar](50) NULL,
	[tax_name] [varchar](50) NULL,
	[tax_percent] [numeric](18, 2) NULL,
	[total_price] [numeric](18, 2) NULL,
	[company_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[invoice_company_id] [numeric](19, 0) NULL,
	[owner_company_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[config_code_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_com_invoice_info]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_com_invoice_info](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[dl_num] [varchar](50) NULL,
	[dl_date] [datetime] NULL,
	[dl_status] [varchar](50) NULL,
	[group_dl_num] [varchar](50) NULL,
	[vehicle_num] [int] NULL,
	[company_id] [numeric](19, 0) NULL,
	[com_invoice_id] [numeric](19, 0) NULL,
	[delivery_id] [numeric](19, 0) NULL,
	[group_dl_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_com_invoice_info_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_com_invoice_info_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[chassis_no] [varchar](50) NULL,
	[colour] [varchar](50) NULL,
	[engine_no] [varchar](50) NULL,
	[exchange_rate] [numeric](18, 6) NULL,
	[model] [varchar](50) NULL,
	[price] [numeric](18, 2) NULL,
	[target_price] [numeric](18, 2) NULL,
	[variant] [varchar](50) NULL,
	[company_id] [numeric](19, 0) NULL,
	[com_invoice_info_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[config_code_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_group_dl]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_group_dl](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[arrival_date] [datetime] NULL,
	[bl_date] [datetime] NULL,
	[departure_date] [datetime] NULL,
	[from_contact_person] [varchar](50) NULL,
	[from_location] [varchar](50) NULL,
	[group_dl_date] [datetime] NULL,
	[group_dl_num] [varchar](50) NULL,
	[remarks] [varchar](500) NULL,
	[shipment_no] [varchar](50) NULL,
	[to_contact_person] [varchar](50) NULL,
	[to_location] [varchar](50) NULL,
	[vehicle_num] [int] NULL,
	[vessel_name] [varchar](50) NULL,
	[vessel_no] [varchar](50) NULL,
	[company_id] [numeric](19, 0) NULL,
	[country_id] [numeric](19, 0) NULL,
	[delivery_type_id] [numeric](19, 0) NULL,
	[from_company_id] [numeric](19, 0) NULL,
	[to_company_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_group_dl_info_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_group_dl_info_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[dl_num] [varchar](50) NULL,
	[chassis_no] [varchar](50) NULL,
	[colour] [varchar](50) NULL,
	[engine_no] [varchar](50) NULL,
	[model] [varchar](50) NULL,
	[remarks] [varchar](500) NULL,
	[variant] [varchar](50) NULL,
	[company_id] [numeric](19, 0) NULL,
	[delivery_id] [numeric](19, 0) NULL,
	[group_dl_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_group_dl_vessel]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_group_dl_vessel](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[remarks] [varchar](500) NULL,
	[status_date] [datetime] NULL,
	[company_id] [numeric](19, 0) NULL,
	[group_dl_id] [numeric](19, 0) NULL,
	[shipping_status_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_shipping_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_shipping_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_delivery]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_delivery](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[current_contact_per] [varchar](100) NULL,
	[delivery_contact_per] [varchar](100) NULL,
	[delivery_date] [datetime] NULL,
	[delivery_list_no] [varchar](100) NULL,
	[driver_name] [varchar](200) NULL,
	[from_app_date] [datetime] NULL,
	[reason] [varchar](1000) NULL,
	[reference_no] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[to_app_date] [datetime] NULL,
	[delivery_type_id] [numeric](19, 0) NULL,
	[dest_country_id] [numeric](19, 0) NULL,
	[from_location_id] [numeric](19, 0) NULL,
	[from_location_type_id] [numeric](19, 0) NULL,
	[from_outlet_id] [numeric](19, 0) NULL,
	[to_location_id] [numeric](19, 0) NULL,
	[to_location_type_id] [numeric](19, 0) NULL,
	[to_outlet_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[from_company_location_id] [numeric](19, 0) NULL,
	[to_company_location_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_delivery_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_delivery_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_config_code]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_config_code](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_location_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_location_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[CKD_BUY_OFF]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CKD_BUY_OFF] AS
SELECT tv.chassis_no,tv2.name AS modelName,tv.production_date,
 	isnull(tv.mcaicbu,'') as mcaicbu,isnull(tv.mcaickd,'') as mcaickd,
  	isnull(Q.destCountry,'') AS destCountry, 
 	tv.engine_no,tv.key_no,tcc.name AS color,tv.lot_no AS process,tm.name AS model,
 	tv.assembly_date AS completedDt,B.delivery_date as buyOffDate,tve.submission_date,
	tve.successful_Date,tcm.name, isnull(tl.name,'')locationName,O.delivery_date AS dateOut,
	isnull(V.vessel_name,'')AS vesselName, isnull(v.vessel_no,'') AS voyageNo,V.loadingportInDate,
	V.vessDepETDDate,V.vessDepActualDate,V.vessAvalETDDate, V.vessAvalActualDate,
	isnull(F.invoice_no,'') AS fininvNo,F.invoice_date AS salesDt,isnull(F.price,0) AS salesPrice,
 	isnull(F.code,'')AS salesPriceCurrency
 	FROM  tb_vehicle  tv
 	LEFT JOIN tb_variant tv2 ON tv2.id=tv.variant_id
 	LEFT JOIN tb_commercial_colour tcc ON tcc.id=tv.commercial_colour_id
 	LEFT JOIN tb_vehicle_model tm ON tm.id=tv.model_id
 	LEFT JOIN tb_location tl ON tl.id=tv.location_id
 	LEFT JOIN tb_company tcm ON tcm.id=tv.company_id
 	LEFT JOIN tb_vehicle_excise tve ON tve.vehicle_id=tv.id
 	LEFT JOIN (
 		  SELECT  tgdld.vehicle_id,P.loadingportInDate ,tgd.departure_date AS vessDepETDDate
 		  ,D.vessDepActualDate,tgd.arrival_date AS vessAvalETDDate,R.vessAvalActualDate,
 		  tgd.vessel_no,tgd.vessel_name,tgd.update_date
 		  FROM  tb_group_dl tgd 
 		  LEFT JOIN 
 			--Arrived at Local Port
 			(	
 				SELECT tgds.group_dl_id AS PLId,tgds.status_date AS loadingportInDate
 				FROM  tb_group_dl_vessel tgds
 				LEFT JOIN tb_shipping_status tcm ON tcm.id=tgds.shipping_status_id
 				WHERE  tcm.code='LOCALPORT'
 			)P	ON P.PLId = tgd.id
 			LEFT JOIN 
 			--vessel departure date 
 			(
 				SELECT tgds.group_dl_id AS PLId,tgds.status_date AS vessDepActualDate
 				FROM  tb_group_dl_vessel tgds
 				LEFT JOIN tb_shipping_status tcm ON tcm.id=tgds.shipping_status_id
 				WHERE  tcm.code='DIP'
 			)D ON D.PLId=tgd.id
 			LEFT JOIN 
 			--for Received at Destination Port
 			(
 				SELECT tgds.group_dl_id AS PLId,tgds.status_date  AS vessAvalActualDate
 				FROM  tb_group_dl_vessel tgds
 				LEFT JOIN tb_shipping_status tcm ON tcm.id=tgds.shipping_status_id
 				WHERE tcm.code='RADP'
 			)R ON R.PLId=tgd.id
 			--LEFT JOIN tb_groupdlinfo tgi ON tgi.groupDLId=tgd.id
 			LEFT JOIN tb_group_dl_info_detail tgdld ON tgdld.delivery_id=tgd.id
 
 	)V ON v.vehicle_id=tv.id
 	
 	--financial Information
 	LEFT JOIN(
 		SELECT tci.invoice_no,tci.invoice_date,tciid.vehicle_id,tciid.target_price AS price,
 		(case when taccn.code='YEN' THEN 'JPY' ELSE taccn.code end) AS code,tci.update_date
 		FROM tb_com_invoice tci
 		LEFT join tb_com_invoice_info tcii ON tcii.com_invoice_id=tci.id
 		LEFT JOIN tb_com_invoice_info_detail tciid ON tciid.com_invoice_info_id=tcii.id
 		LEFT JOIN tb_config_code taccn ON taccn.id=tciid.config_code_id
 		LEFT JOIN tb_workflow_status tws ON tws.id=tci.workflow_status_id
 		LEFT JOIN tb_company tcOwn ON tcOwn.id = tci.owner_company_id
 		LEFT JOIN tb_company tcToCom on tcToCom.id = tci.invoice_company_id
 		WHERE tws.module_name='Company Invoice' AND tws.[status]='Generated'	
 		AND tcOwn.code='MMSB' AND tcToCom.code='MMC'
 	)F ON F.vehicle_id=tv.id
 	
 	--dest country
 	LEFT JOIN(
 		SELECT tdd.vehicle_id,isnull(tc.name,'')AS destCountry,tc.id AS destCountryId
 		  FROM tb_delivery td
 		LEFT JOIN tb_delivery_detail tdd ON tdd.delivery_id=td.id
 		LEFT JOIN tb_workflow_status tws ON tws.id=td.workflow_status_id
 		LEFT JOIN tb_country tc ON tc.id=td.dest_country_id
 		WHERE tws.module_name='vehicleDelivery' AND 
 		(tws.[status]='Delivery Approved' OR tws.[status]='Delivery In Progress')
 		AND tdd.status = 1	
 	)Q ON Q.vehicle_id=tv.id
 	
 	--buy off day
 	LEFT JOIN (
 		SELECT tdd.vehicle_id,td.delivery_date FROM tb_delivery td
 		LEFT JOIN tb_delivery_detail tdd ON tdd.delivery_id=td.id
 		LEFT JOIN tb_workflow_status tws ON tws.id=td.workflow_status_id
  		LEFT JOIN tb_delivery_type tdt ON tdt.id=td.delivery_type_id
 		WHERE tws.module_name='vehicleDelivery' AND (tws.[status]='Received')
 		AND tdd.status = 1
 		AND td.delivery_type_id =(
			SELECT 
				tdt.id 
			FROM 
				tb_delivery_type tdt 
 		    where 
				tdt.name='Assembler to Manufacturer'
		)	
 	)B ON B.vehicle_id=tv.id
 	
 	--date out
 	LEFT JOIN (
 		SELECT 
			tdd.vehicle_id,td.delivery_date 
		FROM 
			tb_delivery td
 			LEFT JOIN tb_delivery_detail tdd ON tdd.delivery_id=td.id
 			LEFT JOIN tb_workflow_status tws ON tws.id=td.workflow_status_id
 		WHERE 
			tws.module_name='vehicleDelivery' AND (tws.status='Received')
 			AND tdd.status = 1
 			AND td.delivery_type_id=(
				SELECT id FROM tb_delivery_type WHERE name='Manufacturer to Distributor')
 			AND td.to_outlet_id =(
				SELECT tc.id FROM tb_company tc WHERE tc.code='MMC')
 			AND td.to_location_type_id=(
				SELECT tlt.id FROM tb_location_type tlt WHERE tlt.name='Port')
 	)O ON O.vehicle_id = tv.id
 	WHERE tv2.origin_type='CKD' 
GO
/****** Object:  Table [dbo].[tb_accessory]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_accessory](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[factory_fit] [bit] NULL,
	[price] [numeric](17, 2) NULL,
	[wholesale_price] [numeric](17, 2) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_allocation_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_allocation_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[access_code] [varchar](100) NULL,
	[expiry_date] [datetime] NULL,
	[loan_required] [bit] NULL,
	[other_bank_name] [varchar](100) NULL,
	[remarks] [varchar](1000) NULL,
	[allocation_id] [numeric](19, 0) NOT NULL,
	[bank_id] [numeric](19, 0) NULL,
	[from_company_id] [numeric](19, 0) NULL,
	[outlet_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_bank]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_bank](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[address] [varchar](500) NULL,
	[bank_flag] [int] NULL,
	[cash_account_code] [varchar](50) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_business_function]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_business_function](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_business_process]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_business_process](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[name] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cancel_reason]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cancel_reason](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cheque_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cheque_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_company_address]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_company_address](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[address_1] [varchar](500) NULL,
	[address_2] [varchar](500) NULL,
	[address_3] [varchar](500) NULL,
	[address_4] [varchar](500) NULL,
	[address_5] [varchar](500) NULL,
	[postcode] [varchar](50) NULL,
	[address_type] [varchar](50) NOT NULL,
	[city_id] [numeric](19, 0) NULL,
	[country_id] [numeric](19, 0) NULL,
	[region_id] [numeric](19, 0) NULL,
	[state_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_company_department]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_company_department](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NOT NULL,
	[department_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_company_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_company_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_company_type_ref]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_company_type_ref](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NOT NULL,
	[company_type_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_config_field]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_config_field](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[check_type] [varchar](255) NULL,
	[default_value] [numeric](17, 2) NULL,
	[flag] [bit] NULL,
	[length] [numeric](17, 2) NULL,
	[level1] [varchar](255) NULL,
	[level2] [varchar](255) NULL,
	[module_name] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[need_check] [bit] NULL,
	[short_num] [varchar](255) NULL,
	[show_name] [varchar](255) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cost]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cost](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cost_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cost_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_credit_note_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_credit_note_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_activity]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_activity](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[activity_source] [varchar](100) NULL,
	[activity_type] [varchar](100) NULL,
	[cc_list] [varchar](100) NULL,
	[content] [varchar](1000) NULL,
	[description] [ntext] NULL,
	[due_date] [datetime] NULL,
	[read_only] [bit] NULL,
	[receive_date] [datetime] NULL,
	[received] [bit] NULL,
	[remarks] [varchar](1000) NULL,
	[reminder_date] [datetime] NULL,
	[status] [varchar](100) NULL,
	[subject] [varchar](100) NULL,
	[campaign_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[lead_id] [numeric](19, 0) NULL,
	[outlet_id] [numeric](19, 0) NULL,
	[owner_id] [numeric](19, 0) NULL,
	[receive_outlet_id] [numeric](19, 0) NULL,
	[reward_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_campaign]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_campaign](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[campaign_num] [varchar](255) NULL,
	[campaign_owner] [varchar](255) NULL,
	[campaign_type] [varchar](50) NULL,
	[criteria] [varchar](255) NULL,
	[description] [varchar](500) NULL,
	[end_date] [datetime] NULL,
	[execute_days] [int] NULL,
	[execution_date] [datetime] NULL,
	[execution_days_unit] [varchar](255) NULL,
	[from_pa] [int] NULL,
	[name] [varchar](255) NULL,
	[reject_date] [datetime] NULL,
	[start_date] [datetime] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_config]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_config](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_lead]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_lead](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[amor_pm_for_preferred_call] [bit] NULL,
	[b_card] [varchar](100) NULL,
	[birth_date] [datetime] NULL,
	[code] [varchar](50) NULL,
	[company_name] [varchar](100) NULL,
	[credit_limit] [numeric](17, 2) NULL,
	[customer_category] [varchar](100) NULL,
	[customer_ic_no] [varchar](100) NULL,
	[customer_type] [varchar](100) NULL,
	[dependents_num] [varchar](100) NULL,
	[education_level] [varchar](100) NULL,
	[ethnicity] [varchar](100) NULL,
	[ethnicity_other] [varchar](100) NULL,
	[famliy_income] [varchar](100) NULL,
	[gender] [varchar](100) NULL,
	[hobbies] [varchar](1000) NULL,
	[id_type] [varchar](100) NULL,
	[initials] [varchar](100) NULL,
	[interests] [varchar](1000) NULL,
	[lead_type] [varchar](100) NULL,
	[lost_date] [datetime] NULL,
	[lost_reason] [varchar](100) NULL,
	[marriage_status] [varchar](100) NULL,
	[monthly_income] [varchar](100) NULL,
	[name] [varchar](100) NULL,
	[note] [varchar](1000) NULL,
	[other_reason] [varchar](1000) NULL,
	[ownership_type] [varchar](100) NULL,
	[personal_income] [varchar](100) NULL,
	[point] [numeric](17, 2) NULL,
	[prefered_name] [varchar](100) NULL,
	[preferred_call_time] [varchar](100) NULL,
	[profession] [varchar](100) NULL,
	[quotation] [bit] NULL,
	[quotation_date] [datetime] NULL,
	[reason] [varchar](100) NULL,
	[receive_call] [bit] NULL,
	[receive_email] [bit] NULL,
	[receive_sms] [bit] NULL,
	[religion] [varchar](100) NULL,
	[remarks] [varchar](1000) NULL,
	[roc_no] [varchar](100) NULL,
	[salutation] [varchar](100) NULL,
	[selling_cycle] [varchar](100) NULL,
	[send_bp_email] [bit] NULL,
	[send_thank_sms] [bit] NULL,
	[source_type] [varchar](100) NULL,
	[spouse_gender] [varchar](100) NULL,
	[spouse_initial] [varchar](100) NULL,
	[spouse_name] [varchar](100) NULL,
	[spouse_salutation] [varchar](100) NULL,
	[status] [varchar](100) NULL,
	[test_drive] [bit] NULL,
	[test_drive_date] [datetime] NULL,
	[used_car_valuation] [bit] NULL,
	[used_car_valuation_date] [datetime] NULL,
	[vehicle_no] [varchar](100) NULL,
	[company_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[language_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_product_interest]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_product_interest](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[customer_type] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[lead_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_brand_id] [numeric](19, 0) NULL,
	[vehicle_model_id] [numeric](19, 0) NULL,
	[vehicle_series_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_reward]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_reward](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[reward_type] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_crm_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_crm_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[module_name] [varchar](50) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_currency]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_currency](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cust_credit_note]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cust_credit_note](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[credit_note_date] [datetime] NULL,
	[credit_note_no] [varchar](50) NULL,
	[gst_amount] [numeric](17, 2) NULL,
	[payment_description] [varchar](255) NULL,
	[price] [numeric](17, 2) NULL,
	[reason_of_issuance] [varchar](2000) NULL,
	[remarks] [varchar](2000) NULL,
	[tax_name] [varchar](255) NULL,
	[tax_percent] [numeric](17, 2) NULL,
	[tax_code] [varchar](50) NULL,
	[total_pay] [numeric](17, 2) NULL,
	[buyer_order_id] [numeric](19, 0) NULL,
	[creator_company_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[cust_credit_note_type_id] [numeric](19, 0) NULL,
	[cust_debit_note_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[invoice_id] [numeric](19, 0) NULL,
	[outlet_company_id] [numeric](19, 0) NULL,
	[promotion_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cust_credit_note_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cust_credit_note_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cust_debit_note]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cust_debit_note](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[debit_note_date] [datetime] NULL,
	[debit_note_no] [varchar](50) NULL,
	[gst_amount] [numeric](17, 2) NULL,
	[payment_description] [varchar](255) NULL,
	[price] [numeric](17, 2) NULL,
	[reason_of_issuance] [varchar](500) NULL,
	[remarks] [varchar](2000) NULL,
	[tax_name] [varchar](255) NULL,
	[tax_percent] [numeric](17, 2) NULL,
	[tax_code] [varchar](50) NULL,
	[total_pay] [numeric](17, 2) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[cust_credit_note_id] [numeric](19, 0) NULL,
	[cust_debit_note_type_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[invoice_id] [numeric](19, 0) NULL,
	[outlet_company_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cust_debit_note_pay]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cust_debit_note_pay](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cust_debit_note_track]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cust_debit_note_track](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_cust_debit_note_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_cust_debit_note_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_spouse]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_spouse](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[gender] [varchar](20) NULL,
	[initials] [varchar](100) NULL,
	[name] [varchar](100) NULL,
	[customer_person_id] [numeric](19, 0) NOT NULL,
	[ethnicity_id] [numeric](19, 0) NULL,
	[saluation_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_customer_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_customer_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_invoice_history]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_invoice_history](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[brand_name] [varchar](255) NULL,
	[buyer_order_no] [varchar](50) NULL,
	[chassis_no] [varchar](50) NULL,
	[commercial_colour_name] [varchar](255) NULL,
	[company_address1] [varchar](500) NULL,
	[company_address2] [varchar](500) NULL,
	[company_address3] [varchar](500) NULL,
	[company_fax] [varchar](50) NULL,
	[company_name] [varchar](255) NULL,
	[company_phone] [varchar](50) NULL,
	[credit_note_no] [varchar](50) NULL,
	[dealer_invoice_item_status] [varchar](50) NULL,
	[dealer_invoice_status] [varchar](50) NULL,
	[doc_display_name] [varchar](255) NULL,
	[engine_no] [varchar](50) NULL,
	[invoice_amount] [numeric](17, 2) NULL,
	[invoice_date] [datetime] NULL,
	[invoice_no] [varchar](50) NULL,
	[made_year] [varchar](50) NULL,
	[model_name] [varchar](100) NULL,
	[outlet_address1] [varchar](500) NULL,
	[outlet_address2] [varchar](500) NULL,
	[outlet_address3] [varchar](500) NULL,
	[outlet_name] [varchar](200) NULL,
	[outstanding_balance] [varchar](50) NULL,
	[payment_amount] [numeric](17, 2) NULL,
	[payment_due_date] [datetime] NULL,
	[payment_status] [varchar](50) NULL,
	[payment_terms] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[request_date] [datetime] NULL,
	[request_order_no] [varchar](50) NULL,
	[roc_no] [varchar](50) NULL,
	[series_name] [varchar](255) NULL,
	[stock_no] [varchar](50) NULL,
	[total_tax] [numeric](17, 2) NULL,
	[trim_package_name] [varchar](255) NULL,
	[variant_code] [varchar](100) NULL,
	[variant_name] [varchar](255) NULL,
	[commercial_colour_id] [numeric](19, 0) NULL,
	[dealer_invoice_id] [numeric](19, 0) NULL,
	[dealer_invoice_item_id] [numeric](19, 0) NULL,
	[outlet_id] [numeric](19, 0) NULL,
	[price_plan_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_invoice_payment_history]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_invoice_payment_history](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[cash_in_date] [datetime] NULL,
	[cheque_date] [datetime] NULL,
	[description] [varchar](1000) NULL,
	[display_status] [varchar](100) NULL,
	[expiry_date] [datetime] NULL,
	[flag] [bit] NULL,
	[issuing_bank_name] [varchar](100) NULL,
	[payment_amount] [numeric](17, 2) NULL,
	[payment_bank_name] [varchar](100) NULL,
	[payment_date] [datetime] NULL,
	[payment_mode_name] [varchar](100) NULL,
	[payment_no] [varchar](50) NULL,
	[receipt_no] [varchar](50) NULL,
	[dealer_invoice_history_id] [numeric](19, 0) NULL,
	[payment_mode_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_invoice_selected_item]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_invoice_selected_item](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[gst_percent] [numeric](17, 2) NULL,
	[gst_code] [varchar](50) NULL,
	[selected] [int] NULL,
	[value] [numeric](17, 2) NULL,
	[dealer_invoice_item_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_dealer_promotion]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_dealer_promotion](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[hq_id] [numeric](19, 0) NULL,
	[outlet_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_debit_note]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_debit_note](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[debit_note_date] [datetime] NULL,
	[debit_note_no] [varchar](50) NULL,
	[gst_amount] [numeric](17, 2) NULL,
	[payment_description] [varchar](255) NULL,
	[price] [numeric](17, 2) NULL,
	[reason_of_issuance] [varchar](500) NULL,
	[remarks] [varchar](2000) NULL,
	[tax_name] [varchar](255) NULL,
	[tax_percent] [numeric](17, 2) NULL,
	[tax_code] [varchar](50) NULL,
	[total_pay] [numeric](17, 2) NULL,
	[creator_company_id] [numeric](19, 0) NULL,
	[credit_note_id] [numeric](19, 0) NULL,
	[dealer_invoice_item_id] [numeric](19, 0) NULL,
	[debit_note_type_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[outlet_company_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_debit_note_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_debit_note_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_delivery_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_delivery_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[delivery_date] [datetime] NULL,
	[remarks] [varchar](1000) NULL,
	[delivery_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_excise]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_excise](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[alert_timeframe] [varchar](50) NULL,
	[dig_length] [int] NULL,
	[excise_no] [varchar](50) NULL,
	[excise_station] [varchar](50) NULL,
	[excise_type] [varchar](50) NULL,
	[excise_year] [int] NULL,
	[expiry_date] [datetime] NULL,
	[prefix] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[company_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_fuel_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_fuel_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_government_colour]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_government_colour](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_group_dl_info]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_group_dl_info](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
	[dl_date] [datetime] NULL,
	[dl_num] [varchar](50) NULL,
	[vehicle_num] [int] NULL,
	[company_id] [numeric](19, 0) NULL,
	[delivery_id] [numeric](19, 0) NULL,
	[group_dl_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_gst_tax]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_gst_tax](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[gst_percent] [numeric](17, 2) NULL,
	[standard] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_incentive]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_incentive](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_incentive_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_incentive_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[incentive_id] [numeric](19, 0) NULL,
	[incentive_type_id] [numeric](19, 0) NULL,
	[range_id] [numeric](19, 0) NULL,
	[model_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_incentive_detail_tier]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_incentive_detail_tier](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_incentive_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_incentive_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[end_date] [datetime] NULL,
	[name] [varchar](200) NULL,
	[start_date] [datetime] NULL,
	[dealer_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_inspection]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_inspection](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[inspection_type_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_inspection_checklist]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_inspection_checklist](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[variant_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_inspection_checklist_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_inspection_checklist_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[inspection_id] [numeric](19, 0) NOT NULL,
	[inspection_checklist_id] [numeric](19, 0) NOT NULL,
	[inspection_level_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_inspection_level]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_inspection_level](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_inspection_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_inspection_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_invoice]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_invoice](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[app_delivery_date] [datetime] NULL,
	[cancel_remarks] [varchar](1000) NULL,
	[delivery_no] [varchar](50) NULL,
	[delivery_remarks] [varchar](1000) NULL,
	[delivery_to] [varchar](1000) NULL,
	[delivery_date] [datetime] NULL,
	[invoice_no] [varchar](50) NULL,
	[invoice_date] [datetime] NULL,
	[payout] [bit] NULL,
	[phone] [varchar](20) NULL,
	[receive_date] [datetime] NULL,
	[reject_date] [datetime] NULL,
	[remarks] [varchar](1000) NULL,
	[status] [bit] NULL,
	[customer_id] [numeric](19, 0) NULL,
	[sales_id] [numeric](19, 0) NULL,
	[service_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_kastam_reason]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_kastam_reason](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_language]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_language](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_loan_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_loan_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[only_loan] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_loose_item]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_loose_item](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_manufacturer_colour]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_manufacturer_colour](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_marriage_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_marriage_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_origin_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_origin_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_original_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_original_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_outlet_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_outlet_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_pay_voucher_vehicle]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_pay_voucher_vehicle](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[amount] [numeric](17, 2) NULL,
	[payment_voucher_id] [numeric](19, 0) NOT NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[payment_bike] [varchar](100) NULL,
	[progress] [varchar](100) NULL,
	[rpd_tail] [varchar](100) NULL,
	[rpn_tail] [varchar](100) NULL,
	[status] [varchar](50) NULL,
	[trading_partner1] [varchar](100) NULL,
	[trading_partner2] [varchar](100) NULL,
	[trading_partner3] [varchar](100) NULL,
	[payment_account_id] [numeric](19, 0) NULL,
	[payment_user_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_account]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_account](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[account_no] [varchar](50) NULL,
	[custom_account_no] [varchar](50) NULL,
	[custom_bank] [varchar](50) NULL,
	[pay_bank] [varchar](50) NULL,
	[pay_station] [varchar](50) NULL,
	[payment_pin] [varchar](50) NULL,
	[remarks] [varchar](1000) NULL,
	[excise_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[engine_no] [varchar](100) NULL,
	[chassis_no] [varchar](20) NULL,
	[progress] [varchar](100) NULL,
	[payment_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_history]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_history](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[bank_draft_date] [datetime] NULL,
	[booking_fee] [bit] NULL,
	[cashier_order_date] [datetime] NULL,
	[cheque_date] [datetime] NULL,
	[expiry_date] [datetime] NULL,
	[flag] [bit] NULL,
	[issuing_bank_name] [varchar](100) NULL,
	[payment_amount] [numeric](17, 2) NULL,
	[payment_bank_name] [varchar](100) NULL,
	[payment_date] [datetime] NULL,
	[payment_no] [varchar](50) NULL,
	[receipt_no] [varchar](50) NULL,
	[refund] [bit] NULL,
	[remarks] [varchar](1000) NULL,
	[tt_date] [datetime] NULL,
	[bank_id] [numeric](19, 0) NULL,
	[bank_in_id] [numeric](19, 0) NULL,
	[cheque_type_id] [numeric](19, 0) NULL,
	[payment_mode_id] [numeric](19, 0) NULL,
	[sales_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_mode]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_mode](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_request]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_request](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[other_bank_name] [varchar](100) NULL,
	[payment_confirm] [bit] NULL,
	[payment_date] [datetime] NULL,
	[reference_no] [varchar](50) NULL,
	[remarks] [varchar](2000) NULL,
	[request_amount] [numeric](17, 2) NULL,
	[request_no] [varchar](50) NULL,
	[request_total_amount] [numeric](17, 2) NULL,
	[bank_id] [numeric](19, 0) NULL,
	[dealer_company_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_terms_variant]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_terms_variant](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[booking_fee] [numeric](17, 2) NULL,
	[deposit] [numeric](17, 2) NULL,
	[down_payment] [numeric](17, 2) NULL,
	[full_amount] [numeric](17, 2) NULL,
	[outstanding] [numeric](17, 2) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_payment_term_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_payment_voucher_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_payment_voucher_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[remarks] [varchar](1000) NULL,
	[payment_voucher_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_price_plan]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_price_plan](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[end_date] [datetime] NULL,
	[name] [varchar](200) NULL,
	[paint_type] [varchar](50) NULL,
	[price_type] [varchar](50) NULL,
	[start_date] [datetime] NULL,
	[suspend_reason] [varchar](255) NULL,
	[company_id] [numeric](19, 0) NULL,
	[currency_id] [numeric](19, 0) NULL,
	[customer_type_id] [numeric](19, 0) NULL,
	[usage_type_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_price_plan_variant]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_price_plan_variant](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[dealer_margin] [numeric](17, 2) NULL,
	[retail_price] [numeric](17, 2) NULL,
	[total_retail_sale] [numeric](17, 2) NULL,
	[total_wholesale] [numeric](17, 2) NULL,
	[wholesale_price] [numeric](17, 2) NULL,
	[price_plan_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_price_plan_variant_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_price_plan_variant_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[value] [numeric](17, 2) NULL,
	[config_field_id] [numeric](19, 0) NULL,
	[price_plan_variant_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_promotion]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_promotion](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[amount] [bit] NULL,
	[description] [varchar](255) NULL,
	[discount] [numeric](17, 2) NULL,
	[end_date] [datetime] NULL,
	[hq_only] [bit] NULL,
	[make_year] [varchar](50) NULL,
	[name] [varchar](255) NULL,
	[quantity_bought] [numeric](17, 2) NULL,
	[special_promotion] [bit] NULL,
	[start_date] [datetime] NULL,
	[suspend_reason] [varchar](255) NULL,
	[promotion_type_id] [numeric](19, 0) NULL,
	[service_plan_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_promotion_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_promotion_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_quotation]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_quotation](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accessory_remark] [varchar](1000) NULL,
	[accessory_value] [numeric](17, 2) NULL,
	[discount] [numeric](17, 2) NULL,
	[expiry_date] [datetime] NULL,
	[insurance] [numeric](17, 2) NULL,
	[opt_accessory_price] [numeric](17, 2) NULL,
	[quotation_date] [datetime] NULL,
	[quotation_no] [varchar](255) NULL,
	[re_road_tax] [bit] NULL,
	[road_tax] [numeric](17, 2) NULL,
	[status] [bit] NULL,
	[total_retail] [numeric](17, 2) NULL,
	[trade_in] [bit] NULL,
	[trade_in_price] [numeric](17, 2) NULL,
	[transfer_fee] [numeric](17, 2) NULL,
	[variant_price] [numeric](17, 2) NULL,
	[colour_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[service_plan_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[usage_type_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[warranty_plan_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_quotation_accessory]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_quotation_accessory](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[price] [numeric](17, 2) NULL,
	[accessory_id] [numeric](19, 0) NULL,
	[quotation_id] [numeric](19, 0) NOT NULL,
	[sta_accessory_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_quotation_price_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_quotation_price_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[value] [numeric](17, 2) NULL,
	[config_field_id] [numeric](19, 0) NULL,
	[quotation_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_quotation_track]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_quotation_track](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[status] [bit] NULL,
	[quotation_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_range]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_range](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_region]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_region](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[country_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_register_out_pay]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_register_out_pay](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[reason] [varchar](1000) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_register_out_pay_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_register_out_pay_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[register_out_pay_id] [numeric](19, 0) NOT NULL,
	[sales_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_reimbursement_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_reimbursement_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_request_track]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_request_track](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[status] [bit] NULL,
	[request_order_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_role]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_role](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[company_id] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_role_function]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_role_function](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[access] [int] NULL,
	[business_function_id] [numeric](19, 0) NOT NULL,
	[role_id] [numeric](19, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sales_order]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sales_order](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accessory_price] [numeric](17, 2) NULL,
	[approved_amount] [varchar](50) NULL,
	[approved_date] [datetime] NULL,
	[bank_name_for_loan] [varchar](50) NULL,
	[bank_office_name] [varchar](50) NULL,
	[coverage_type] [varchar](50) NULL,
	[cust_bank_account] [varchar](50) NULL,
	[grand_total] [numeric](17, 2) NULL,
	[gross_premium] [varchar](50) NULL,
	[gst_tax_code] [varchar](100) NULL,
	[gst_tax_name] [varchar](100) NULL,
	[gst_tax_percent] [numeric](17, 2) NULL,
	[insurance_ap_date] [datetime] NULL,
	[insurance_expiry] [datetime] NULL,
	[insurance_expiry_date] [datetime] NULL,
	[insurance_remarks] [varchar](2000) NULL,
	[insurance_status] [varchar](100) NULL,
	[loading] [varchar](50) NULL,
	[loan_duration] [varchar](50) NULL,
	[loan_status] [varchar](100) NULL,
	[loan_type] [varchar](50) NULL,
	[ncb_or_ncd] [varchar](200) NULL,
	[nett_premium] [varchar](50) NULL,
	[ownership_fee] [numeric](17, 2) NULL,
	[payable_amount] [varchar](50) NULL,
	[policy_type] [varchar](50) NULL,
	[re_road_tax] [bit] NULL,
	[remarks] [varchar](2000) NULL,
	[request_amount] [varchar](50) NULL,
	[road_tax] [numeric](17, 2) NULL,
	[sales_date] [datetime] NULL,
	[sales_order_no] [varchar](50) NULL,
	[service_tax] [varchar](50) NULL,
	[stamp_duty] [varchar](50) NULL,
	[total_coverage] [varchar](50) NULL,
	[total_retail_price] [varchar](50) NULL,
	[total_tax] [numeric](17, 2) NULL,
	[transfer_fee] [numeric](17, 2) NULL,
	[variant_price] [numeric](17, 2) NULL,
	[variant_tax] [numeric](17, 2) NULL,
	[bank_id] [numeric](19, 0) NULL,
	[buyer_order_id] [numeric](19, 0) NULL,
	[dealer_company_id] [numeric](19, 0) NULL,
	[gst_tax_id] [numeric](19, 0) NULL,
	[insurance_company_id] [numeric](19, 0) NULL,
	[sales_executive_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sales_order_accessory]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sales_order_accessory](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[price] [numeric](17, 2) NULL,
	[accessory_id] [numeric](19, 0) NOT NULL,
	[sales_order_id] [numeric](19, 0) NOT NULL,
	[sta_accessory_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sales_order_price_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sales_order_price_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[value] [numeric](17, 2) NULL,
	[config_field_id] [numeric](19, 0) NULL,
	[sales_order_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sales_order_track]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sales_order_track](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[sales_order_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_salutation]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_salutation](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_send_email]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_send_email](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[alert_type] [int] NULL,
	[attachment_path_name] [varchar](200) NULL,
	[bcc_user] [varchar](255) NULL,
	[cc_user] [varchar](255) NULL,
	[content_format] [int] NULL,
	[contents] [ntext] NULL,
	[creation_time] [datetime] NULL,
	[data_encoding] [int] NULL,
	[duration_days] [int] NULL,
	[finish_time] [datetime] NULL,
	[from_user] [varchar](255) NULL,
	[max_retry_times] [int] NULL,
	[meeting_end_time] [datetime] NULL,
	[meeting_start_time] [datetime] NULL,
	[meeting_address] [varchar](2000) NULL,
	[mt_price] [numeric](17, 2) NULL,
	[product_type] [int] NULL,
	[reattachment_path_name] [varchar](255) NULL,
	[response_code] [int] NULL,
	[retry_times] [int] NULL,
	[status_code] [int] NULL,
	[subject] [varchar](500) NULL,
	[to_user] [varchar](255) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_service]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_service](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_service_package]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_service_package](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[end_date] [datetime] NULL,
	[free_service] [bit] NULL,
	[price] [numeric](17, 2) NULL,
	[promotion] [bit] NULL,
	[start_date] [datetime] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_service_plan]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_service_plan](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_service_record]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_service_record](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[service_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sublet_contractor]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sublet_contractor](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sublet_contractor_country]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sublet_contractor_country](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[country_id] [numeric](19, 0) NULL,
	[sublet_contractor_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sublet_contractor_outlet]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sublet_contractor_outlet](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[sublet_contractor_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sublet_contractor_region]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sublet_contractor_region](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[region_id] [numeric](19, 0) NULL,
	[sublet_contractor_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sublet_po]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sublet_po](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sublet_po_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sublet_po_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_supplier_code]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_supplier_code](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](200) NULL,
	[supp_code] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_token]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_token](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[date] [datetime] NULL,
	[ip_address] [varchar](100) NULL,
	[series] [varchar](100) NULL,
	[user_agent] [varchar](1000) NULL,
	[user_login] [varchar](100) NULL,
	[value] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_trade_in]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_trade_in](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[bank_settlement_price] [numeric](17, 2) NULL,
	[car_type] [varchar](50) NULL,
	[colour] [varchar](50) NULL,
	[in_security_compound] [bit] NULL,
	[insurance_no] [varchar](50) NULL,
	[made_year] [int] NULL,
	[model] [varchar](50) NULL,
	[nett_balance] [numeric](17, 2) NULL,
	[owner_address] [varchar](2000) NULL,
	[owner_ic_no] [varchar](20) NULL,
	[owner_name] [varchar](255) NULL,
	[price] [numeric](17, 2) NULL,
	[registration] [bit] NULL,
	[registration_no] [varchar](50) NULL,
	[reselling_price] [numeric](17, 2) NULL,
	[surrender_date] [datetime] NULL,
	[trade_in_no] [varchar](50) NULL,
	[insurance_company_id] [numeric](19, 0) NULL,
	[trade_in_brand_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_trade_in_brand]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_trade_in_brand](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_trim_package]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_trim_package](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_trim_package_category]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_trim_package_category](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[trim_type] [varchar](20) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_trim_package_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_trim_package_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[status] [bit] NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[trim_package_category_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_usage_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_usage_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](200) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_user_role]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_user_role](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[role_id] [numeric](19, 0) NOT NULL,
	[user_id] [numeric](19, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_variant_accessory]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_variant_accessory](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accessory_type] [varchar](100) NOT NULL,
	[accessory_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_variant_colour]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_variant_colour](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[bin_size] [int] NULL,
	[status] [bit] NULL,
	[commercial_colour_id] [numeric](19, 0) NOT NULL,
	[variant_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_variant_loose_item]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_variant_loose_item](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[loose_item_id] [numeric](19, 0) NOT NULL,
	[variant_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_variant_trim_package]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_variant_trim_package](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NOT NULL,
	[variant_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_variant_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_variant_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_accessory]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_accessory](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accessory_type] [varchar](50) NULL,
	[install_user] [varchar](255) NULL,
	[install_end_date] [datetime] NULL,
	[install_start_date] [datetime] NULL,
	[price] [numeric](17, 2) NULL,
	[stalled] [bit] NULL,
	[accessory_id] [numeric](19, 0) NOT NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_body_style]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_body_style](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_body_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_body_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[status] [bit] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_category]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_category](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[avg_consumption] [float] NULL,
	[axle_num] [int] NULL,
	[brake_horse_power] [int] NULL,
	[co2_emission] [varchar](255) NULL,
	[door_num] [int] NULL,
	[driver_side] [varchar](50) NULL,
	[engine_capacity] [int] NULL,
	[make_code] [varchar](50) NULL,
	[make_description] [varchar](255) NULL,
	[model_year] [int] NULL,
	[origin_type] [varchar](50) NULL,
	[seat_num] [int] NULL,
	[transmission_type] [varchar](50) NULL,
	[weight] [float] NULL,
	[wheel_base] [int] NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_excise_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_excise_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[approved] [bit] NULL,
	[approved_date] [datetime] NULL,
	[chassis_no] [varchar](50) NULL,
	[approver_id] [numeric](19, 0) NULL,
	[approver_company_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_fit_accessory]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_fit_accessory](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[install_user] [varchar](255) NULL,
	[install_end_date] [datetime] NULL,
	[install_start_date] [datetime] NULL,
	[stalled] [bit] NULL,
	[accessory_id] [numeric](19, 0) NOT NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_hold]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_hold](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[access_code] [varchar](50) NULL,
	[other_bank_name] [varchar](100) NULL,
	[remarks] [varchar](100) NULL,
	[bank_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_hold_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_hold_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[access_code] [varchar](50) NULL,
	[other_bank_name] [varchar](100) NULL,
	[remarks] [varchar](100) NULL,
	[bank_id] [numeric](19, 0) NULL,
	[company_id] [numeric](19, 0) NULL,
	[vehicle_hold_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_kastam]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_kastam](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[app_from_date] [datetime] NULL,
	[app_to_date] [datetime] NULL,
	[k1_submission_date] [datetime] NULL,
	[k8_submission_date] [datetime] NULL,
	[reference_no] [varchar](50) NULL,
	[returned_comment] [varchar](1000) NULL,
	[returned_date] [datetime] NULL,
	[send_comment] [varchar](1000) NULL,
	[send_date] [datetime] NULL,
	[kastam_reason_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_kastam_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_kastam_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[app_from_date] [datetime] NULL,
	[app_to_date] [datetime] NULL,
	[reference_no] [varchar](50) NULL,
	[returned_comment] [varchar](1000) NULL,
	[returned_date] [datetime] NULL,
	[send_comment] [varchar](1000) NULL,
	[send_date] [datetime] NULL,
	[submission_date] [datetime] NULL,
	[kastam_reason_id] [numeric](19, 0) NULL,
	[vehicle_kastam_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_loose_item]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_loose_item](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[received] [bit] NULL,
	[received_date] [datetime] NULL,
	[received_user] [varchar](255) NULL,
	[loose_item_id] [numeric](19, 0) NOT NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_miti]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_miti](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[ap_collect_date] [datetime] NULL,
	[ap_num] [varchar](50) NULL,
	[ap_submit_date] [datetime] NULL,
	[mag_app_date] [datetime] NULL,
	[miti_app_date] [datetime] NULL,
	[smk_num] [varchar](50) NULL,
	[submission_date] [datetime] NULL,
	[submit_mag_date] [datetime] NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_miti_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_miti_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[result_date] [datetime] NULL,
	[vehicle_miti_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_owned]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_owned](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[chassis_no] [varchar](50) NULL,
	[customer_type] [varchar](50) NULL,
	[engine_no] [varchar](50) NULL,
	[ownership] [varchar](50) NULL,
	[registration_no] [varchar](50) NULL,
	[commercial_colour_id] [numeric](19, 0) NULL,
	[customer_id] [numeric](19, 0) NOT NULL,
	[lead_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NULL,
	[vehicle_brand_id] [numeric](19, 0) NULL,
	[vehicle_model_id] [numeric](19, 0) NULL,
	[vehicle_series_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_payment_term]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_payment_term](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[booking_fee] [bit] NULL,
	[booking_fee_alert] [int] NULL,
	[booking_fee_pay] [int] NULL,
	[deposit] [bit] NULL,
	[deposit_alert] [int] NULL,
	[deposit_pay] [int] NULL,
	[down_payment] [bit] NULL,
	[down_payment_alert] [int] NULL,
	[down_payment_pay] [int] NULL,
	[effective_from_date] [datetime] NULL,
	[effective_to_date] [datetime] NULL,
	[full_alert] [int] NULL,
	[full_amount] [bit] NULL,
	[full_pay] [int] NULL,
	[name] [varchar](50) NULL,
	[outstanding] [bit] NULL,
	[outstanding_alert] [int] NULL,
	[outstanding_pay] [int] NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_pdi_result]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_pdi_result](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[accessory_fit] [bit] NULL,
	[batch_no] [varchar](50) NULL,
	[comment] [varchar](1000) NULL,
	[inspection_date] [datetime] NULL,
	[priority] [int] NULL,
	[rework] [bit] NULL,
	[inspection_checklist_id] [numeric](19, 0) NOT NULL,
	[inspector_id] [numeric](19, 0) NULL,
	[service_manager_id] [numeric](19, 0) NULL,
	[vehicle_pdi_id] [numeric](19, 0) NOT NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_pdi_result_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_pdi_result_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[checked] [bit] NULL,
	[comment] [varchar](1000) NULL,
	[inspection_checklist_detail_id] [numeric](19, 0) NOT NULL,
	[vehicle_pdi_result_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_po]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_po](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[assembly_date] [datetime] NULL,
	[bl_date] [datetime] NULL,
	[bl_num] [varchar](50) NULL,
	[eta] [datetime] NULL,
	[invoice_date] [datetime] NULL,
	[invoice_no] [varchar](50) NULL,
	[packing_list_number] [varchar](50) NULL,
	[production_month] [int] NULL,
	[production_year] [int] NULL,
	[request_date] [datetime] NULL,
	[request_order_no] [varchar](50) NULL,
	[shipping_doc_receive_date] [datetime] NULL,
	[status] [bit] NULL,
	[company_id] [numeric](19, 0) NULL,
	[dest_country_id] [numeric](19, 0) NULL,
	[manufacturer_id] [numeric](19, 0) NULL,
	[workflow_status_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_po_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_po_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[quantity] [numeric](17, 2) NULL,
	[status] [bit] NULL,
	[commercial_colour_id] [numeric](19, 0) NULL,
	[trim_package_id] [numeric](19, 0) NULL,
	[variant_id] [numeric](19, 0) NULL,
	[vehicle_brand_id] [numeric](19, 0) NULL,
	[vehicle_model_id] [numeric](19, 0) NULL,
	[vehicle_po_id] [numeric](19, 0) NOT NULL,
	[vehicle_series_id] [numeric](19, 0) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_po_track]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_po_track](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[status] [bit] NULL,
	[vehicle_po_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_receive_order]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_receive_order](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[fulfill] [int] NULL,
	[status] [bit] NULL,
	[vehicle_po_detail_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_receive_order_detail]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_receive_order_detail](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[vehicle_id] [numeric](19, 0) NOT NULL,
	[vehicle_receive_order_id] [numeric](19, 0) NOT NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_status]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_status](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[description] [varchar](500) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehicle_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehicle_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_vehvar_type]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vehvar_type](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[name] [varchar](100) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_warranty_plan]    Script Date: 21/11/2016 6:02:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_warranty_plan](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ref_id] [numeric](19, 0) NULL,
	[cost_bearer_less_id] [int] NULL,
	[cost_bearer_less_name] [varchar](200) NULL,
	[cost_bearer_less_type] [varchar](50) NULL,
	[cost_bearer_more_id] [int] NULL,
	[cost_bearer_more_name] [varchar](200) NULL,
	[cost_bearer_more_type] [varchar](50) NULL,
	[effctive_end_date] [datetime] NULL,
	[effctive_start_date] [datetime] NULL,
	[mileage] [int] NULL,
	[period_end] [int] NULL,
	[period_start] [int] NULL,
	[plan_name] [varchar](200) NULL,
	[status] [bit] NULL,
	[warranty_type] [varchar](50) NULL,
	[update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tb_allocation]  WITH CHECK ADD  CONSTRAINT [FK3xrgmc8paafik64a5qmocb6e6] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_allocation] CHECK CONSTRAINT [FK3xrgmc8paafik64a5qmocb6e6]
GO
ALTER TABLE [dbo].[tb_allocation]  WITH CHECK ADD  CONSTRAINT [FK6sql8emsyh14m9bhr5f0qidkg] FOREIGN KEY([from_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_allocation] CHECK CONSTRAINT [FK6sql8emsyh14m9bhr5f0qidkg]
GO
ALTER TABLE [dbo].[tb_allocation]  WITH CHECK ADD  CONSTRAINT [FK9o58ftm3sqq43cj0t8mwyg5kw] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_allocation] CHECK CONSTRAINT [FK9o58ftm3sqq43cj0t8mwyg5kw]
GO
ALTER TABLE [dbo].[tb_allocation]  WITH CHECK ADD  CONSTRAINT [FKlu22rxsqcavxmmdgbpsc34mjv] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_allocation] CHECK CONSTRAINT [FKlu22rxsqcavxmmdgbpsc34mjv]
GO
ALTER TABLE [dbo].[tb_allocation_result]  WITH CHECK ADD  CONSTRAINT [FK35ur4a59p7tqxtjqbvye1eqf0] FOREIGN KEY([outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_allocation_result] CHECK CONSTRAINT [FK35ur4a59p7tqxtjqbvye1eqf0]
GO
ALTER TABLE [dbo].[tb_allocation_result]  WITH CHECK ADD  CONSTRAINT [FK3pt0lvxm54foisj0c5emk9a0b] FOREIGN KEY([allocation_id])
REFERENCES [dbo].[tb_allocation] ([id])
GO
ALTER TABLE [dbo].[tb_allocation_result] CHECK CONSTRAINT [FK3pt0lvxm54foisj0c5emk9a0b]
GO
ALTER TABLE [dbo].[tb_allocation_result]  WITH CHECK ADD  CONSTRAINT [FKh40v9m96d5syt80hafw3m2sm3] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_allocation_result] CHECK CONSTRAINT [FKh40v9m96d5syt80hafw3m2sm3]
GO
ALTER TABLE [dbo].[tb_allocation_result]  WITH CHECK ADD  CONSTRAINT [FKmrtv3aq4idov0mjhktl7o7pqf] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_allocation_result] CHECK CONSTRAINT [FKmrtv3aq4idov0mjhktl7o7pqf]
GO
ALTER TABLE [dbo].[tb_allocation_result]  WITH CHECK ADD  CONSTRAINT [FKos869jqmi87o9q8rv6bi196am] FOREIGN KEY([from_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_allocation_result] CHECK CONSTRAINT [FKos869jqmi87o9q8rv6bi196am]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK324wl5fy3lo27uf274ghwrv4f] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK324wl5fy3lo27uf274ghwrv4f]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK3mtkq0lw1e8p70nit993ckjy5] FOREIGN KEY([colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK3mtkq0lw1e8p70nit993ckjy5]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK412slcr5vbrbcc0ohhv36mlyt] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK412slcr5vbrbcc0ohhv36mlyt]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK44a402yqc288s9dqgtg7xcax] FOREIGN KEY([vehvar_type_id])
REFERENCES [dbo].[tb_vehvar_type] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK44a402yqc288s9dqgtg7xcax]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK6ysh4pdykpwpgg6fyqb1mjur8] FOREIGN KEY([trade_in_id])
REFERENCES [dbo].[tb_trade_in] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK6ysh4pdykpwpgg6fyqb1mjur8]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK7isdpi6eq5o5i91sb8kdfkibd] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK7isdpi6eq5o5i91sb8kdfkibd]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FK9p5ij9q7ytlfo58higvyao7jr] FOREIGN KEY([service_plan_id])
REFERENCES [dbo].[tb_service_plan] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FK9p5ij9q7ytlfo58higvyao7jr]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKb9qinw3pmjco9gh8ghm6ajjjb] FOREIGN KEY([insurance_company_id])
REFERENCES [dbo].[tb_insurance_company] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKb9qinw3pmjco9gh8ghm6ajjjb]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKcp3aigv7h1w1qkrb9u7yam0j6] FOREIGN KEY([service_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKcp3aigv7h1w1qkrb9u7yam0j6]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKec16qdgkiylcuf27u5icvr7ku] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKec16qdgkiylcuf27u5icvr7ku]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKfh392n0msq6or3gjsu38nf6qw] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKfh392n0msq6or3gjsu38nf6qw]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKg6f0lhtg6oogtsbfg2nsiw8ih] FOREIGN KEY([bank_in_bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKg6f0lhtg6oogtsbfg2nsiw8ih]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKg7fnid7s9l21ghxu6cv5ea31c] FOREIGN KEY([service_package_id])
REFERENCES [dbo].[tb_service_package] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKg7fnid7s9l21ghxu6cv5ea31c]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKhga1r6xkvquqw573ljfkmgjyc] FOREIGN KEY([issuing_bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKhga1r6xkvquqw573ljfkmgjyc]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKhukjrp6dbiwbqgldssts3rkvk] FOREIGN KEY([warranty_plan_id])
REFERENCES [dbo].[tb_warranty_plan] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKhukjrp6dbiwbqgldssts3rkvk]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKiasfk6px4vvmxgtrce04fuf69] FOREIGN KEY([cheque_type_id])
REFERENCES [dbo].[tb_cheque_type] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKiasfk6px4vvmxgtrce04fuf69]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKibtec8wlcxjh0hmq0kh1orqfb] FOREIGN KEY([first_opt_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKibtec8wlcxjh0hmq0kh1orqfb]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKknisaqc3aj3fnqhl0c6d19850] FOREIGN KEY([dealer_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKknisaqc3aj3fnqhl0c6d19850]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKltqbwa5siq4xl3oewa1wa4w3q] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKltqbwa5siq4xl3oewa1wa4w3q]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKnbm9s4xc6utgfceitj8843qfn] FOREIGN KEY([payment_mode_id])
REFERENCES [dbo].[tb_payment_mode] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKnbm9s4xc6utgfceitj8843qfn]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKnhcdkwe7q5k6roca178xv3pj3] FOREIGN KEY([quotation_id])
REFERENCES [dbo].[tb_quotation] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKnhcdkwe7q5k6roca178xv3pj3]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKntatkysd9wn30w5pearih7msc] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKntatkysd9wn30w5pearih7msc]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKo5yhdlp5r8oavtf15fiwtn1on] FOREIGN KEY([sales_executive_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKo5yhdlp5r8oavtf15fiwtn1on]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKp39bgkjfvvaarwg1t5knkdteu] FOREIGN KEY([usage_type_id])
REFERENCES [dbo].[tb_usage_type] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKp39bgkjfvvaarwg1t5knkdteu]
GO
ALTER TABLE [dbo].[tb_buyer_order]  WITH CHECK ADD  CONSTRAINT [FKpr4b5x8lugoctoh3ux2d76f20] FOREIGN KEY([second_opt_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_buyer_order] CHECK CONSTRAINT [FKpr4b5x8lugoctoh3ux2d76f20]
GO
ALTER TABLE [dbo].[tb_city]  WITH CHECK ADD  CONSTRAINT [FK1rn7oty4mwqviyw8vk67crapo] FOREIGN KEY([state_id])
REFERENCES [dbo].[tb_state] ([id])
GO
ALTER TABLE [dbo].[tb_city] CHECK CONSTRAINT [FK1rn7oty4mwqviyw8vk67crapo]
GO
ALTER TABLE [dbo].[tb_com_invoice]  WITH CHECK ADD  CONSTRAINT [FK44ggahqex9rr2bai8dy7aft0h] FOREIGN KEY([owner_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice] CHECK CONSTRAINT [FK44ggahqex9rr2bai8dy7aft0h]
GO
ALTER TABLE [dbo].[tb_com_invoice]  WITH CHECK ADD  CONSTRAINT [FK4qpaixp1iiubpaanr87pndhxf] FOREIGN KEY([invoice_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice] CHECK CONSTRAINT [FK4qpaixp1iiubpaanr87pndhxf]
GO
ALTER TABLE [dbo].[tb_com_invoice]  WITH CHECK ADD  CONSTRAINT [FKlbx62287waymwwkf1pmhh83o8] FOREIGN KEY([config_code_id])
REFERENCES [dbo].[tb_config_code] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice] CHECK CONSTRAINT [FKlbx62287waymwwkf1pmhh83o8]
GO
ALTER TABLE [dbo].[tb_com_invoice]  WITH CHECK ADD  CONSTRAINT [FKlkbvbr6timhwcqp1m36jft3nj] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice] CHECK CONSTRAINT [FKlkbvbr6timhwcqp1m36jft3nj]
GO
ALTER TABLE [dbo].[tb_com_invoice]  WITH CHECK ADD  CONSTRAINT [FKs5swhg1kje71m0la4vq7sulkk] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice] CHECK CONSTRAINT [FKs5swhg1kje71m0la4vq7sulkk]
GO
ALTER TABLE [dbo].[tb_com_invoice]  WITH CHECK ADD  CONSTRAINT [FKt0echc7ljaf1kra6y9wdv0y5n] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice] CHECK CONSTRAINT [FKt0echc7ljaf1kra6y9wdv0y5n]
GO
ALTER TABLE [dbo].[tb_com_invoice_info]  WITH CHECK ADD  CONSTRAINT [FK2qc1clrvt3pnfoksti5hca36b] FOREIGN KEY([group_dl_id])
REFERENCES [dbo].[tb_group_dl] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info] CHECK CONSTRAINT [FK2qc1clrvt3pnfoksti5hca36b]
GO
ALTER TABLE [dbo].[tb_com_invoice_info]  WITH CHECK ADD  CONSTRAINT [FK6psbplddbbahrgvl0k68xwh6d] FOREIGN KEY([delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info] CHECK CONSTRAINT [FK6psbplddbbahrgvl0k68xwh6d]
GO
ALTER TABLE [dbo].[tb_com_invoice_info]  WITH CHECK ADD  CONSTRAINT [FKh36v0ttmwfly17aepx69rajep] FOREIGN KEY([com_invoice_id])
REFERENCES [dbo].[tb_com_invoice] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info] CHECK CONSTRAINT [FKh36v0ttmwfly17aepx69rajep]
GO
ALTER TABLE [dbo].[tb_com_invoice_info]  WITH CHECK ADD  CONSTRAINT [FKly5yv128sp00mqx5fl7k1jklf] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info] CHECK CONSTRAINT [FKly5yv128sp00mqx5fl7k1jklf]
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail]  WITH CHECK ADD  CONSTRAINT [FK306lv7mmdiqq4ufaxq0khycqo] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail] CHECK CONSTRAINT [FK306lv7mmdiqq4ufaxq0khycqo]
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail]  WITH CHECK ADD  CONSTRAINT [FKdodj1awv50qv6h7vdy5plmrnv] FOREIGN KEY([config_code_id])
REFERENCES [dbo].[tb_config_code] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail] CHECK CONSTRAINT [FKdodj1awv50qv6h7vdy5plmrnv]
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail]  WITH CHECK ADD  CONSTRAINT [FKfhvm8qguipleccaskjw5btkya] FOREIGN KEY([com_invoice_info_id])
REFERENCES [dbo].[tb_com_invoice_info] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail] CHECK CONSTRAINT [FKfhvm8qguipleccaskjw5btkya]
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail]  WITH CHECK ADD  CONSTRAINT [FKjg6d09ni2ymax2ng2qanmc6es] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_com_invoice_info_detail] CHECK CONSTRAINT [FKjg6d09ni2ymax2ng2qanmc6es]
GO
ALTER TABLE [dbo].[tb_commercial_colour]  WITH CHECK ADD  CONSTRAINT [FKa5jqrmule56lek74paw0k2oky] FOREIGN KEY([manufacturer_colour_id])
REFERENCES [dbo].[tb_manufacturer_colour] ([id])
GO
ALTER TABLE [dbo].[tb_commercial_colour] CHECK CONSTRAINT [FKa5jqrmule56lek74paw0k2oky]
GO
ALTER TABLE [dbo].[tb_commercial_colour]  WITH CHECK ADD  CONSTRAINT [FKs6djk4lqemc6xfu4st6lwwxq8] FOREIGN KEY([government_colour_id])
REFERENCES [dbo].[tb_government_colour] ([id])
GO
ALTER TABLE [dbo].[tb_commercial_colour] CHECK CONSTRAINT [FKs6djk4lqemc6xfu4st6lwwxq8]
GO
ALTER TABLE [dbo].[tb_company_address]  WITH CHECK ADD  CONSTRAINT [FK130edah2a7padse9vqh1ecb6s] FOREIGN KEY([city_id])
REFERENCES [dbo].[tb_city] ([id])
GO
ALTER TABLE [dbo].[tb_company_address] CHECK CONSTRAINT [FK130edah2a7padse9vqh1ecb6s]
GO
ALTER TABLE [dbo].[tb_company_address]  WITH CHECK ADD  CONSTRAINT [FK4gifny6y5a6tiytv952tfg027] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_company_address] CHECK CONSTRAINT [FK4gifny6y5a6tiytv952tfg027]
GO
ALTER TABLE [dbo].[tb_company_address]  WITH CHECK ADD  CONSTRAINT [FK5888d74e4c3h7y50hx715fkrx] FOREIGN KEY([region_id])
REFERENCES [dbo].[tb_region] ([id])
GO
ALTER TABLE [dbo].[tb_company_address] CHECK CONSTRAINT [FK5888d74e4c3h7y50hx715fkrx]
GO
ALTER TABLE [dbo].[tb_company_address]  WITH CHECK ADD  CONSTRAINT [FK96inuwca6eoepdarhjel3wyt8] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_company_address] CHECK CONSTRAINT [FK96inuwca6eoepdarhjel3wyt8]
GO
ALTER TABLE [dbo].[tb_company_address]  WITH CHECK ADD  CONSTRAINT [FKc2ypb6dtjr0h8mhp4fu7y8som] FOREIGN KEY([state_id])
REFERENCES [dbo].[tb_state] ([id])
GO
ALTER TABLE [dbo].[tb_company_address] CHECK CONSTRAINT [FKc2ypb6dtjr0h8mhp4fu7y8som]
GO
ALTER TABLE [dbo].[tb_company_department]  WITH CHECK ADD  CONSTRAINT [FK7wtmh8s6ndumvrmlt2lqfyufk] FOREIGN KEY([department_id])
REFERENCES [dbo].[tb_department] ([id])
GO
ALTER TABLE [dbo].[tb_company_department] CHECK CONSTRAINT [FK7wtmh8s6ndumvrmlt2lqfyufk]
GO
ALTER TABLE [dbo].[tb_company_department]  WITH CHECK ADD  CONSTRAINT [FKhd6mc1snoweibjejwptw280k3] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_company_department] CHECK CONSTRAINT [FKhd6mc1snoweibjejwptw280k3]
GO
ALTER TABLE [dbo].[tb_company_type_ref]  WITH CHECK ADD  CONSTRAINT [FKg7ysi7y4i1gk1skdk8ibifrua] FOREIGN KEY([company_type_id])
REFERENCES [dbo].[tb_company_type] ([id])
GO
ALTER TABLE [dbo].[tb_company_type_ref] CHECK CONSTRAINT [FKg7ysi7y4i1gk1skdk8ibifrua]
GO
ALTER TABLE [dbo].[tb_company_type_ref]  WITH CHECK ADD  CONSTRAINT [FKslgvegkg8ink165jpmr4g6ig1] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_company_type_ref] CHECK CONSTRAINT [FKslgvegkg8ink165jpmr4g6ig1]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FK245o7kls027ysrbw5x088irch] FOREIGN KEY([outlet_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FK245o7kls027ysrbw5x088irch]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FK475plg1mv928t1vstrcrcqtjq] FOREIGN KEY([dealer_invoice_item_id])
REFERENCES [dbo].[tb_dealer_invoice_item] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FK475plg1mv928t1vstrcrcqtjq]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FK7kq4k8mr13qp7urcaybaa83lu] FOREIGN KEY([creator_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FK7kq4k8mr13qp7urcaybaa83lu]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FKh3frhkftcnvupcsvgywrnlssa] FOREIGN KEY([debit_note_id])
REFERENCES [dbo].[tb_debit_note] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FKh3frhkftcnvupcsvgywrnlssa]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FKoxt45hi7rf7h7i5jnl45ujnlq] FOREIGN KEY([promotion_id])
REFERENCES [dbo].[tb_promotion] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FKoxt45hi7rf7h7i5jnl45ujnlq]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FKqwmjxo3mar8g499j57ddcq7u5] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FKqwmjxo3mar8g499j57ddcq7u5]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FKs4yjp9h326qy0b4fv02h00608] FOREIGN KEY([credit_note_type_id])
REFERENCES [dbo].[tb_credit_note_type] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FKs4yjp9h326qy0b4fv02h00608]
GO
ALTER TABLE [dbo].[tb_credit_note]  WITH CHECK ADD  CONSTRAINT [FKs9ncr5crdgqqn00alb5vrsgu8] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_credit_note] CHECK CONSTRAINT [FKs9ncr5crdgqqn00alb5vrsgu8]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FK1u75tf4adrwj79tb736e439pi] FOREIGN KEY([receive_outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FK1u75tf4adrwj79tb736e439pi]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FK2rhb9l6vl0k6ikqvsd9cl7lil] FOREIGN KEY([lead_id])
REFERENCES [dbo].[tb_crm_lead] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FK2rhb9l6vl0k6ikqvsd9cl7lil]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FK4stj6axa54dmrjlo3elojdb3s] FOREIGN KEY([owner_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FK4stj6axa54dmrjlo3elojdb3s]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FKd7abvtj5hxw8s28fgsw1b5cww] FOREIGN KEY([outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FKd7abvtj5hxw8s28fgsw1b5cww]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FKpevm2t73pfrbhdhuwnuckgckv] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FKpevm2t73pfrbhdhuwnuckgckv]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FKq3kjfjjrddf46klx8yhbjvwge] FOREIGN KEY([reward_id])
REFERENCES [dbo].[tb_crm_reward] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FKq3kjfjjrddf46klx8yhbjvwge]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FKqg1svv7fhvws2ie7pmalhw15j] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FKqg1svv7fhvws2ie7pmalhw15j]
GO
ALTER TABLE [dbo].[tb_crm_activity]  WITH CHECK ADD  CONSTRAINT [FKrqt37e61ah0k1b3xjy0252sv4] FOREIGN KEY([campaign_id])
REFERENCES [dbo].[tb_crm_campaign] ([id])
GO
ALTER TABLE [dbo].[tb_crm_activity] CHECK CONSTRAINT [FKrqt37e61ah0k1b3xjy0252sv4]
GO
ALTER TABLE [dbo].[tb_crm_lead]  WITH CHECK ADD  CONSTRAINT [FK3pw8ystjw3kmths2hgf8pvgwo] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_crm_lead] CHECK CONSTRAINT [FK3pw8ystjw3kmths2hgf8pvgwo]
GO
ALTER TABLE [dbo].[tb_crm_lead]  WITH CHECK ADD  CONSTRAINT [FKp20y31oawsuthoktbblyx6ua8] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_crm_lead] CHECK CONSTRAINT [FKp20y31oawsuthoktbblyx6ua8]
GO
ALTER TABLE [dbo].[tb_crm_lead]  WITH CHECK ADD  CONSTRAINT [FKsr4mr1beic5n2feaqi3gi6nov] FOREIGN KEY([language_id])
REFERENCES [dbo].[tb_language] ([id])
GO
ALTER TABLE [dbo].[tb_crm_lead] CHECK CONSTRAINT [FKsr4mr1beic5n2feaqi3gi6nov]
GO
ALTER TABLE [dbo].[tb_crm_product_interest]  WITH CHECK ADD  CONSTRAINT [FKai65w4fth7a5jual0ftp7uls1] FOREIGN KEY([lead_id])
REFERENCES [dbo].[tb_crm_lead] ([id])
GO
ALTER TABLE [dbo].[tb_crm_product_interest] CHECK CONSTRAINT [FKai65w4fth7a5jual0ftp7uls1]
GO
ALTER TABLE [dbo].[tb_crm_product_interest]  WITH CHECK ADD  CONSTRAINT [FKbe4is48pn47sewtwfp0ow9ife] FOREIGN KEY([vehicle_model_id])
REFERENCES [dbo].[tb_vehicle_model] ([id])
GO
ALTER TABLE [dbo].[tb_crm_product_interest] CHECK CONSTRAINT [FKbe4is48pn47sewtwfp0ow9ife]
GO
ALTER TABLE [dbo].[tb_crm_product_interest]  WITH CHECK ADD  CONSTRAINT [FKdu4n3ti9x0kaiu8l83qx8u747] FOREIGN KEY([vehicle_brand_id])
REFERENCES [dbo].[tb_vehicle_brand] ([id])
GO
ALTER TABLE [dbo].[tb_crm_product_interest] CHECK CONSTRAINT [FKdu4n3ti9x0kaiu8l83qx8u747]
GO
ALTER TABLE [dbo].[tb_crm_product_interest]  WITH CHECK ADD  CONSTRAINT [FKl0loooencvekfy1v80yg9amr9] FOREIGN KEY([vehicle_series_id])
REFERENCES [dbo].[tb_vehicle_series] ([id])
GO
ALTER TABLE [dbo].[tb_crm_product_interest] CHECK CONSTRAINT [FKl0loooencvekfy1v80yg9amr9]
GO
ALTER TABLE [dbo].[tb_crm_product_interest]  WITH CHECK ADD  CONSTRAINT [FKlkjac744xng559nyrn0a4e5c5] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_crm_product_interest] CHECK CONSTRAINT [FKlkjac744xng559nyrn0a4e5c5]
GO
ALTER TABLE [dbo].[tb_crm_product_interest]  WITH CHECK ADD  CONSTRAINT [FKpt86g39p50nvc6f8w70aj37rq] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_crm_product_interest] CHECK CONSTRAINT [FKpt86g39p50nvc6f8w70aj37rq]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FK2g2mni66xkq4a3313npy09079] FOREIGN KEY([cust_credit_note_type_id])
REFERENCES [dbo].[tb_cust_credit_note_type] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FK2g2mni66xkq4a3313npy09079]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FK3g944v3uxst4xsxi1j0gn1esh] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FK3g944v3uxst4xsxi1j0gn1esh]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FK3tgr3yvurynt669ybnpkgpw0t] FOREIGN KEY([invoice_id])
REFERENCES [dbo].[tb_invoice] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FK3tgr3yvurynt669ybnpkgpw0t]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FK98hxgfemj0hxbdcoec03u92eo] FOREIGN KEY([promotion_id])
REFERENCES [dbo].[tb_promotion] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FK98hxgfemj0hxbdcoec03u92eo]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FKd67yb8p94iaqr9yjtvtknhh6m] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FKd67yb8p94iaqr9yjtvtknhh6m]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FKfd306ghin7tqhm77qbqefwy32] FOREIGN KEY([creator_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FKfd306ghin7tqhm77qbqefwy32]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FKnyqqy06jdhomw4u0d28wx5ryd] FOREIGN KEY([outlet_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FKnyqqy06jdhomw4u0d28wx5ryd]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FKp38hv66xs8dn5yh9l7fndl80f] FOREIGN KEY([buyer_order_id])
REFERENCES [dbo].[tb_buyer_order] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FKp38hv66xs8dn5yh9l7fndl80f]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FKqud1m0xlpde6y92qm0v5cdcdc] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FKqud1m0xlpde6y92qm0v5cdcdc]
GO
ALTER TABLE [dbo].[tb_cust_credit_note]  WITH CHECK ADD  CONSTRAINT [FKr4he1hf8djiw1k46fhhiksbs1] FOREIGN KEY([cust_debit_note_id])
REFERENCES [dbo].[tb_cust_debit_note] ([id])
GO
ALTER TABLE [dbo].[tb_cust_credit_note] CHECK CONSTRAINT [FKr4he1hf8djiw1k46fhhiksbs1]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FK4fk3ryqjwhe5icul2xwhrn77y] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FK4fk3ryqjwhe5icul2xwhrn77y]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FKfae32i66t6as8slejjlr9sy9w] FOREIGN KEY([cust_credit_note_id])
REFERENCES [dbo].[tb_cust_credit_note] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FKfae32i66t6as8slejjlr9sy9w]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FKgr9vd4nt2fei9cvembnwijuhu] FOREIGN KEY([invoice_id])
REFERENCES [dbo].[tb_invoice] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FKgr9vd4nt2fei9cvembnwijuhu]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FKlcifrnn0wb7xmqw5b77b4rs6x] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FKlcifrnn0wb7xmqw5b77b4rs6x]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FKobxbwid9lljgg44b2iea64rix] FOREIGN KEY([cust_debit_note_type_id])
REFERENCES [dbo].[tb_cust_debit_note_type] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FKobxbwid9lljgg44b2iea64rix]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FKq5qf1w43a7k8a2p1qbvhrlhns] FOREIGN KEY([outlet_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FKq5qf1w43a7k8a2p1qbvhrlhns]
GO
ALTER TABLE [dbo].[tb_cust_debit_note]  WITH CHECK ADD  CONSTRAINT [FKt6l5gv5atevolqi672suv07vd] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_cust_debit_note] CHECK CONSTRAINT [FKt6l5gv5atevolqi672suv07vd]
GO
ALTER TABLE [dbo].[tb_customer]  WITH CHECK ADD  CONSTRAINT [FKjgu9uucqrrio3xe5q5i8shvxv] FOREIGN KEY([customer_category_id])
REFERENCES [dbo].[tb_customer_category] ([id])
GO
ALTER TABLE [dbo].[tb_customer] CHECK CONSTRAINT [FKjgu9uucqrrio3xe5q5i8shvxv]
GO
ALTER TABLE [dbo].[tb_customer]  WITH CHECK ADD  CONSTRAINT [FKqhhbdap7ly83ov373bp2e2x6b] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_customer] CHECK CONSTRAINT [FKqhhbdap7ly83ov373bp2e2x6b]
GO
ALTER TABLE [dbo].[tb_customer]  WITH CHECK ADD  CONSTRAINT [FKsg3nst9sqdex2ob79h0foqia5] FOREIGN KEY([language_id])
REFERENCES [dbo].[tb_language] ([id])
GO
ALTER TABLE [dbo].[tb_customer] CHECK CONSTRAINT [FKsg3nst9sqdex2ob79h0foqia5]
GO
ALTER TABLE [dbo].[tb_customer_address]  WITH CHECK ADD  CONSTRAINT [FK367o4kdkp4v36gdun3jc4a8ej] FOREIGN KEY([region_id])
REFERENCES [dbo].[tb_region] ([id])
GO
ALTER TABLE [dbo].[tb_customer_address] CHECK CONSTRAINT [FK367o4kdkp4v36gdun3jc4a8ej]
GO
ALTER TABLE [dbo].[tb_customer_address]  WITH CHECK ADD  CONSTRAINT [FKkewbv5ahj182bx1f170d2wf4s] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_customer_address] CHECK CONSTRAINT [FKkewbv5ahj182bx1f170d2wf4s]
GO
ALTER TABLE [dbo].[tb_customer_address]  WITH CHECK ADD  CONSTRAINT [FKp9jkml62wn9g90jfsysw77v5] FOREIGN KEY([city_id])
REFERENCES [dbo].[tb_city] ([id])
GO
ALTER TABLE [dbo].[tb_customer_address] CHECK CONSTRAINT [FKp9jkml62wn9g90jfsysw77v5]
GO
ALTER TABLE [dbo].[tb_customer_address]  WITH CHECK ADD  CONSTRAINT [FKqartxbvygicbxqocav5akcy45] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_customer_address] CHECK CONSTRAINT [FKqartxbvygicbxqocav5akcy45]
GO
ALTER TABLE [dbo].[tb_customer_address]  WITH CHECK ADD  CONSTRAINT [FKqm9edwirwkmjgqklw53q1cfi2] FOREIGN KEY([state_id])
REFERENCES [dbo].[tb_state] ([id])
GO
ALTER TABLE [dbo].[tb_customer_address] CHECK CONSTRAINT [FKqm9edwirwkmjgqklw53q1cfi2]
GO
ALTER TABLE [dbo].[tb_customer_company]  WITH CHECK ADD  CONSTRAINT [FKbitpjv9ibc70l96wutrdmiyfc] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_customer_company] CHECK CONSTRAINT [FKbitpjv9ibc70l96wutrdmiyfc]
GO
ALTER TABLE [dbo].[tb_customer_company]  WITH CHECK ADD  CONSTRAINT [FKsmkmppnyj8nsc5eh0d4fd872a] FOREIGN KEY([ethnicity_id])
REFERENCES [dbo].[tb_ethnicity] ([id])
GO
ALTER TABLE [dbo].[tb_customer_company] CHECK CONSTRAINT [FKsmkmppnyj8nsc5eh0d4fd872a]
GO
ALTER TABLE [dbo].[tb_customer_email]  WITH CHECK ADD  CONSTRAINT [FK5t5ei6l42gmvf2ie275eq7wlv] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_customer_email] CHECK CONSTRAINT [FK5t5ei6l42gmvf2ie275eq7wlv]
GO
ALTER TABLE [dbo].[tb_customer_person]  WITH CHECK ADD  CONSTRAINT [FKdaf957opp4psxke7bsymnswys] FOREIGN KEY([ethnicity_id])
REFERENCES [dbo].[tb_ethnicity] ([id])
GO
ALTER TABLE [dbo].[tb_customer_person] CHECK CONSTRAINT [FKdaf957opp4psxke7bsymnswys]
GO
ALTER TABLE [dbo].[tb_customer_person]  WITH CHECK ADD  CONSTRAINT [FKl6j6sgckf49pakuymv4x0sq94] FOREIGN KEY([marriage_status_id])
REFERENCES [dbo].[tb_marriage_status] ([id])
GO
ALTER TABLE [dbo].[tb_customer_person] CHECK CONSTRAINT [FKl6j6sgckf49pakuymv4x0sq94]
GO
ALTER TABLE [dbo].[tb_customer_person]  WITH CHECK ADD  CONSTRAINT [FKmmax4wq5dd5i42yxys2eyfhn5] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_customer_person] CHECK CONSTRAINT [FKmmax4wq5dd5i42yxys2eyfhn5]
GO
ALTER TABLE [dbo].[tb_customer_person]  WITH CHECK ADD  CONSTRAINT [FKqxm6q7mv2qt07724i4fgt4faj] FOREIGN KEY([saluation_id])
REFERENCES [dbo].[tb_salutation] ([id])
GO
ALTER TABLE [dbo].[tb_customer_person] CHECK CONSTRAINT [FKqxm6q7mv2qt07724i4fgt4faj]
GO
ALTER TABLE [dbo].[tb_customer_phone]  WITH CHECK ADD  CONSTRAINT [FKefig1jugfkgo7juhetjj8ug3r] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_customer_phone] CHECK CONSTRAINT [FKefig1jugfkgo7juhetjj8ug3r]
GO
ALTER TABLE [dbo].[tb_customer_spouse]  WITH CHECK ADD  CONSTRAINT [FK49q9qwpca6slj27ul558t6caw] FOREIGN KEY([customer_person_id])
REFERENCES [dbo].[tb_customer_person] ([id])
GO
ALTER TABLE [dbo].[tb_customer_spouse] CHECK CONSTRAINT [FK49q9qwpca6slj27ul558t6caw]
GO
ALTER TABLE [dbo].[tb_customer_spouse]  WITH CHECK ADD  CONSTRAINT [FKjgeihes10629uchdwchdoatbd] FOREIGN KEY([ethnicity_id])
REFERENCES [dbo].[tb_ethnicity] ([id])
GO
ALTER TABLE [dbo].[tb_customer_spouse] CHECK CONSTRAINT [FKjgeihes10629uchdwchdoatbd]
GO
ALTER TABLE [dbo].[tb_customer_spouse]  WITH CHECK ADD  CONSTRAINT [FKpwr7sjbdww8wvtt37m3ffwftd] FOREIGN KEY([saluation_id])
REFERENCES [dbo].[tb_salutation] ([id])
GO
ALTER TABLE [dbo].[tb_customer_spouse] CHECK CONSTRAINT [FKpwr7sjbdww8wvtt37m3ffwftd]
GO
ALTER TABLE [dbo].[tb_dealer_invoice]  WITH CHECK ADD  CONSTRAINT [FK6x87lk5vkxcu9xe3uwhitq7da] FOREIGN KEY([dealer_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice] CHECK CONSTRAINT [FK6x87lk5vkxcu9xe3uwhitq7da]
GO
ALTER TABLE [dbo].[tb_dealer_invoice]  WITH CHECK ADD  CONSTRAINT [FKgv7fqdfm2lcf0li8gycvwvt09] FOREIGN KEY([price_plan_id])
REFERENCES [dbo].[tb_price_plan] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice] CHECK CONSTRAINT [FKgv7fqdfm2lcf0li8gycvwvt09]
GO
ALTER TABLE [dbo].[tb_dealer_invoice]  WITH CHECK ADD  CONSTRAINT [FKmd7b4jubfh9k76p53kcm3istw] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice] CHECK CONSTRAINT [FKmd7b4jubfh9k76p53kcm3istw]
GO
ALTER TABLE [dbo].[tb_dealer_invoice]  WITH CHECK ADD  CONSTRAINT [FKpmgepgy6bm399ek7dgpuktma7] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice] CHECK CONSTRAINT [FKpmgepgy6bm399ek7dgpuktma7]
GO
ALTER TABLE [dbo].[tb_dealer_invoice]  WITH CHECK ADD  CONSTRAINT [FKtf4bquemfpkwjaifuexksy1hp] FOREIGN KEY([dealer_request_order_id])
REFERENCES [dbo].[tb_dealer_request_order] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice] CHECK CONSTRAINT [FKtf4bquemfpkwjaifuexksy1hp]
GO
ALTER TABLE [dbo].[tb_dealer_invoice]  WITH CHECK ADD  CONSTRAINT [FKtq5tnkfut9vcdvp5tbar399ct] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice] CHECK CONSTRAINT [FKtq5tnkfut9vcdvp5tbar399ct]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FK1ar9qwij9w0r8ohwmbbpvux47] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FK1ar9qwij9w0r8ohwmbbpvux47]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FK6i92grqwm9qdl35ogot4bt38w] FOREIGN KEY([price_plan_id])
REFERENCES [dbo].[tb_price_plan] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FK6i92grqwm9qdl35ogot4bt38w]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FKawef24jciq1rsibd1r6obpfs3] FOREIGN KEY([dealer_invoice_item_id])
REFERENCES [dbo].[tb_dealer_invoice_item] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FKawef24jciq1rsibd1r6obpfs3]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FKnf9scr0ois9ts4sescugliy37] FOREIGN KEY([outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FKnf9scr0ois9ts4sescugliy37]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FKpwaylsk8uowxc44sm3pbr86js] FOREIGN KEY([commercial_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FKpwaylsk8uowxc44sm3pbr86js]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FKr0so221yygctfcjybpdju2jup] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FKr0so221yygctfcjybpdju2jup]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history]  WITH CHECK ADD  CONSTRAINT [FKr5qr7uyh92fx7gq405v2nk4af] FOREIGN KEY([dealer_invoice_id])
REFERENCES [dbo].[tb_dealer_invoice] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_history] CHECK CONSTRAINT [FKr5qr7uyh92fx7gq405v2nk4af]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_item]  WITH CHECK ADD  CONSTRAINT [FK9kjfjg8gsyk70ao85vak6t5d8] FOREIGN KEY([dealer_invoice_id])
REFERENCES [dbo].[tb_dealer_invoice] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_item] CHECK CONSTRAINT [FK9kjfjg8gsyk70ao85vak6t5d8]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_item]  WITH CHECK ADD  CONSTRAINT [FKqeyfk9rupp6o861lysy2lyopa] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_item] CHECK CONSTRAINT [FKqeyfk9rupp6o861lysy2lyopa]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_payment_history]  WITH CHECK ADD  CONSTRAINT [FK35qj67keyfc0l6plr8hi70r2c] FOREIGN KEY([payment_mode_id])
REFERENCES [dbo].[tb_payment_mode] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_payment_history] CHECK CONSTRAINT [FK35qj67keyfc0l6plr8hi70r2c]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_payment_history]  WITH CHECK ADD  CONSTRAINT [FKiyd9iob0kichkv5xub9ln92iu] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_payment_history] CHECK CONSTRAINT [FKiyd9iob0kichkv5xub9ln92iu]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_payment_history]  WITH CHECK ADD  CONSTRAINT [FKt4ug1hclf7cpluapaisuhs149] FOREIGN KEY([dealer_invoice_history_id])
REFERENCES [dbo].[tb_dealer_invoice_history] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_payment_history] CHECK CONSTRAINT [FKt4ug1hclf7cpluapaisuhs149]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_selected_item]  WITH CHECK ADD  CONSTRAINT [FKiq279ft9wexelbhmhtkn39yk] FOREIGN KEY([dealer_invoice_item_id])
REFERENCES [dbo].[tb_dealer_invoice_item] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_invoice_selected_item] CHECK CONSTRAINT [FKiq279ft9wexelbhmhtkn39yk]
GO
ALTER TABLE [dbo].[tb_dealer_promotion]  WITH CHECK ADD  CONSTRAINT [FK9qlg0wy6oae1k7eakf7ry4svq] FOREIGN KEY([outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_promotion] CHECK CONSTRAINT [FK9qlg0wy6oae1k7eakf7ry4svq]
GO
ALTER TABLE [dbo].[tb_dealer_promotion]  WITH CHECK ADD  CONSTRAINT [FKm9m3tek7i3vhxxsd300n2othl] FOREIGN KEY([hq_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_promotion] CHECK CONSTRAINT [FKm9m3tek7i3vhxxsd300n2othl]
GO
ALTER TABLE [dbo].[tb_dealer_promotion]  WITH CHECK ADD  CONSTRAINT [FKs7ovlkx8pa6xo9oh7a0x2wkwp] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_promotion] CHECK CONSTRAINT [FKs7ovlkx8pa6xo9oh7a0x2wkwp]
GO
ALTER TABLE [dbo].[tb_dealer_request_order]  WITH CHECK ADD  CONSTRAINT [FKhsaelxta6oddqgdkglpwxi8v] FOREIGN KEY([dealer_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_request_order] CHECK CONSTRAINT [FKhsaelxta6oddqgdkglpwxi8v]
GO
ALTER TABLE [dbo].[tb_dealer_request_order]  WITH CHECK ADD  CONSTRAINT [FKnkjfa09sf1byd09jadt5jnmev] FOREIGN KEY([requester_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_request_order] CHECK CONSTRAINT [FKnkjfa09sf1byd09jadt5jnmev]
GO
ALTER TABLE [dbo].[tb_dealer_request_order]  WITH CHECK ADD  CONSTRAINT [FKonx5h6x4vxak6dk3j2fhnvb59] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_request_order] CHECK CONSTRAINT [FKonx5h6x4vxak6dk3j2fhnvb59]
GO
ALTER TABLE [dbo].[tb_dealer_request_order]  WITH CHECK ADD  CONSTRAINT [FKt2m9ar0tan9iusu1x2mdxb5ty] FOREIGN KEY([disti_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_dealer_request_order] CHECK CONSTRAINT [FKt2m9ar0tan9iusu1x2mdxb5ty]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FK6sltj5qok2w134bnq2ag999mt] FOREIGN KEY([creator_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FK6sltj5qok2w134bnq2ag999mt]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FK83ambv2su8utcec9u7r2jfxfp] FOREIGN KEY([debit_note_type_id])
REFERENCES [dbo].[tb_debit_note_type] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FK83ambv2su8utcec9u7r2jfxfp]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FKb2r84bcdnh9knlkb14stv761f] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FKb2r84bcdnh9knlkb14stv761f]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FKbsg9nofr116uqexu8qkr4tkub] FOREIGN KEY([dealer_invoice_item_id])
REFERENCES [dbo].[tb_dealer_invoice_item] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FKbsg9nofr116uqexu8qkr4tkub]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FKck8rdsxl2rxqxj0lluht6g1cd] FOREIGN KEY([outlet_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FKck8rdsxl2rxqxj0lluht6g1cd]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FKcuwh8gw2yahixawkbiedl7km] FOREIGN KEY([credit_note_id])
REFERENCES [dbo].[tb_credit_note] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FKcuwh8gw2yahixawkbiedl7km]
GO
ALTER TABLE [dbo].[tb_debit_note]  WITH CHECK ADD  CONSTRAINT [FKi138ax5icvj6d49sb98qjaad] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_debit_note] CHECK CONSTRAINT [FKi138ax5icvj6d49sb98qjaad]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FK5n82utna4714h4b65gm9xleaq] FOREIGN KEY([dest_country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FK5n82utna4714h4b65gm9xleaq]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FK6jwh9qrhlicjn8osumnp2l4on] FOREIGN KEY([to_company_location_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FK6jwh9qrhlicjn8osumnp2l4on]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FK8pgg90mf20urm1gdpcwurh73b] FOREIGN KEY([from_location_type_id])
REFERENCES [dbo].[tb_location_type] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FK8pgg90mf20urm1gdpcwurh73b]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FK90llnafi2joegfxx5j8wfkqmt] FOREIGN KEY([delivery_type_id])
REFERENCES [dbo].[tb_delivery_type] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FK90llnafi2joegfxx5j8wfkqmt]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKa8k1g5kb77wktl9guqniwmlds] FOREIGN KEY([to_outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKa8k1g5kb77wktl9guqniwmlds]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKdfrsgxwdn1nn01cgr5g44yunr] FOREIGN KEY([from_outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKdfrsgxwdn1nn01cgr5g44yunr]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKfpu57vp7ef6ko1euf72xwcv8i] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKfpu57vp7ef6ko1euf72xwcv8i]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKgmh8vshvwh6nbe3l3dhwfr3y0] FOREIGN KEY([to_location_type_id])
REFERENCES [dbo].[tb_location_type] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKgmh8vshvwh6nbe3l3dhwfr3y0]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKot9pl56fhnir27kr6ql78iu0f] FOREIGN KEY([from_location_id])
REFERENCES [dbo].[tb_location] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKot9pl56fhnir27kr6ql78iu0f]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKs4q46m2mkuphmc2xlqsnuw01c] FOREIGN KEY([from_company_location_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKs4q46m2mkuphmc2xlqsnuw01c]
GO
ALTER TABLE [dbo].[tb_delivery]  WITH CHECK ADD  CONSTRAINT [FKsrjfjnys7nttw2twt634rbo98] FOREIGN KEY([to_location_id])
REFERENCES [dbo].[tb_location] ([id])
GO
ALTER TABLE [dbo].[tb_delivery] CHECK CONSTRAINT [FKsrjfjnys7nttw2twt634rbo98]
GO
ALTER TABLE [dbo].[tb_delivery_detail]  WITH CHECK ADD  CONSTRAINT [FKdw6q1fgq8086a3werrk18rkqb] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_delivery_detail] CHECK CONSTRAINT [FKdw6q1fgq8086a3werrk18rkqb]
GO
ALTER TABLE [dbo].[tb_delivery_detail]  WITH CHECK ADD  CONSTRAINT [FKsae535hsaehllv6hkavhpki47] FOREIGN KEY([delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_delivery_detail] CHECK CONSTRAINT [FKsae535hsaehllv6hkavhpki47]
GO
ALTER TABLE [dbo].[tb_delivery_detail]  WITH CHECK ADD  CONSTRAINT [FKsy8dtfnptg9nkbd8nvqtk63jl] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_delivery_detail] CHECK CONSTRAINT [FKsy8dtfnptg9nkbd8nvqtk63jl]
GO
ALTER TABLE [dbo].[tb_delivery_result]  WITH CHECK ADD  CONSTRAINT [FK5b4mbc4x05f98m7an9ft1qnnf] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_delivery_result] CHECK CONSTRAINT [FK5b4mbc4x05f98m7an9ft1qnnf]
GO
ALTER TABLE [dbo].[tb_delivery_result]  WITH CHECK ADD  CONSTRAINT [FK5bwnksr9h0cj54h05bgjsce2l] FOREIGN KEY([delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_delivery_result] CHECK CONSTRAINT [FK5bwnksr9h0cj54h05bgjsce2l]
GO
ALTER TABLE [dbo].[tb_department]  WITH CHECK ADD  CONSTRAINT [FK8qsca9vsdo24pruldkoyla688] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_department] CHECK CONSTRAINT [FK8qsca9vsdo24pruldkoyla688]
GO
ALTER TABLE [dbo].[tb_department]  WITH CHECK ADD  CONSTRAINT [FK955s6h2hmdqc7weuq8gfpwxbl] FOREIGN KEY([state_id])
REFERENCES [dbo].[tb_state] ([id])
GO
ALTER TABLE [dbo].[tb_department] CHECK CONSTRAINT [FK955s6h2hmdqc7weuq8gfpwxbl]
GO
ALTER TABLE [dbo].[tb_department]  WITH CHECK ADD  CONSTRAINT [FKo6og43gtnakoy583olnbt7elw] FOREIGN KEY([city_id])
REFERENCES [dbo].[tb_city] ([id])
GO
ALTER TABLE [dbo].[tb_department] CHECK CONSTRAINT [FKo6og43gtnakoy583olnbt7elw]
GO
ALTER TABLE [dbo].[tb_department]  WITH CHECK ADD  CONSTRAINT [FKqgm7qmlu0sbewumrniomlo99c] FOREIGN KEY([region_id])
REFERENCES [dbo].[tb_region] ([id])
GO
ALTER TABLE [dbo].[tb_department] CHECK CONSTRAINT [FKqgm7qmlu0sbewumrniomlo99c]
GO
ALTER TABLE [dbo].[tb_excise]  WITH CHECK ADD  CONSTRAINT [FK22s302l0sqqd9kavrfelpwbmn] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_excise] CHECK CONSTRAINT [FK22s302l0sqqd9kavrfelpwbmn]
GO
ALTER TABLE [dbo].[tb_group_dl]  WITH CHECK ADD  CONSTRAINT [FK6m7v1fi4lj9hi4dg8unw7vd6] FOREIGN KEY([delivery_type_id])
REFERENCES [dbo].[tb_delivery_type] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl] CHECK CONSTRAINT [FK6m7v1fi4lj9hi4dg8unw7vd6]
GO
ALTER TABLE [dbo].[tb_group_dl]  WITH CHECK ADD  CONSTRAINT [FKbq1uivhxmj163f040sixf0mf2] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl] CHECK CONSTRAINT [FKbq1uivhxmj163f040sixf0mf2]
GO
ALTER TABLE [dbo].[tb_group_dl]  WITH CHECK ADD  CONSTRAINT [FKe0n8ttpqjjwu36rlmr2iybuw4] FOREIGN KEY([from_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl] CHECK CONSTRAINT [FKe0n8ttpqjjwu36rlmr2iybuw4]
GO
ALTER TABLE [dbo].[tb_group_dl]  WITH CHECK ADD  CONSTRAINT [FKg295dlj71tbl7y387lmy758ab] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl] CHECK CONSTRAINT [FKg295dlj71tbl7y387lmy758ab]
GO
ALTER TABLE [dbo].[tb_group_dl]  WITH CHECK ADD  CONSTRAINT [FKh3h5p1sf8lgou119d7nhm064b] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl] CHECK CONSTRAINT [FKh3h5p1sf8lgou119d7nhm064b]
GO
ALTER TABLE [dbo].[tb_group_dl]  WITH CHECK ADD  CONSTRAINT [FKl59ebdibytgiofrvkd8ks5aoi] FOREIGN KEY([to_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl] CHECK CONSTRAINT [FKl59ebdibytgiofrvkd8ks5aoi]
GO
ALTER TABLE [dbo].[tb_group_dl_info]  WITH CHECK ADD  CONSTRAINT [FK30jb4s2ojnn02jss4t83fsv9f] FOREIGN KEY([group_dl_id])
REFERENCES [dbo].[tb_group_dl] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info] CHECK CONSTRAINT [FK30jb4s2ojnn02jss4t83fsv9f]
GO
ALTER TABLE [dbo].[tb_group_dl_info]  WITH CHECK ADD  CONSTRAINT [FK4geda9tw69e44ughrr8mgp3ir] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info] CHECK CONSTRAINT [FK4geda9tw69e44ughrr8mgp3ir]
GO
ALTER TABLE [dbo].[tb_group_dl_info]  WITH CHECK ADD  CONSTRAINT [FKdk2lgx2ou859a04ib9llmf1c6] FOREIGN KEY([delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info] CHECK CONSTRAINT [FKdk2lgx2ou859a04ib9llmf1c6]
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail]  WITH CHECK ADD  CONSTRAINT [FK2laxsl1s1bgqjdyrgv2pc9t2k] FOREIGN KEY([delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail] CHECK CONSTRAINT [FK2laxsl1s1bgqjdyrgv2pc9t2k]
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail]  WITH CHECK ADD  CONSTRAINT [FKag23bb6sqdgp61lghm09pv5db] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail] CHECK CONSTRAINT [FKag23bb6sqdgp61lghm09pv5db]
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail]  WITH CHECK ADD  CONSTRAINT [FKjrpxb05gcycsofhi3fw4a6gi9] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail] CHECK CONSTRAINT [FKjrpxb05gcycsofhi3fw4a6gi9]
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail]  WITH CHECK ADD  CONSTRAINT [FKr0ojqovue2hxfllmp6n6noqk6] FOREIGN KEY([group_dl_id])
REFERENCES [dbo].[tb_group_dl] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_info_detail] CHECK CONSTRAINT [FKr0ojqovue2hxfllmp6n6noqk6]
GO
ALTER TABLE [dbo].[tb_group_dl_vessel]  WITH CHECK ADD  CONSTRAINT [FK7dgeevcxn657svedu5qw9if50] FOREIGN KEY([group_dl_id])
REFERENCES [dbo].[tb_group_dl] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_vessel] CHECK CONSTRAINT [FK7dgeevcxn657svedu5qw9if50]
GO
ALTER TABLE [dbo].[tb_group_dl_vessel]  WITH CHECK ADD  CONSTRAINT [FKivn7oi3mxl55kyxxlv42pwemu] FOREIGN KEY([shipping_status_id])
REFERENCES [dbo].[tb_shipping_status] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_vessel] CHECK CONSTRAINT [FKivn7oi3mxl55kyxxlv42pwemu]
GO
ALTER TABLE [dbo].[tb_group_dl_vessel]  WITH CHECK ADD  CONSTRAINT [FKnrh2t4lagwnag71rjhup3xisg] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_group_dl_vessel] CHECK CONSTRAINT [FKnrh2t4lagwnag71rjhup3xisg]
GO
ALTER TABLE [dbo].[tb_incentive_detail]  WITH CHECK ADD  CONSTRAINT [FK3mv786y3s3u56vsif3rj6km5k] FOREIGN KEY([model_id])
REFERENCES [dbo].[tb_vehicle_model] ([id])
GO
ALTER TABLE [dbo].[tb_incentive_detail] CHECK CONSTRAINT [FK3mv786y3s3u56vsif3rj6km5k]
GO
ALTER TABLE [dbo].[tb_incentive_detail]  WITH CHECK ADD  CONSTRAINT [FKjgu28xkrtcbc5hqy6t8oxmslx] FOREIGN KEY([range_id])
REFERENCES [dbo].[tb_range] ([id])
GO
ALTER TABLE [dbo].[tb_incentive_detail] CHECK CONSTRAINT [FKjgu28xkrtcbc5hqy6t8oxmslx]
GO
ALTER TABLE [dbo].[tb_incentive_detail]  WITH CHECK ADD  CONSTRAINT [FKoofrqk14151t7l8s1schc1h5e] FOREIGN KEY([incentive_id])
REFERENCES [dbo].[tb_cost] ([id])
GO
ALTER TABLE [dbo].[tb_incentive_detail] CHECK CONSTRAINT [FKoofrqk14151t7l8s1schc1h5e]
GO
ALTER TABLE [dbo].[tb_incentive_detail]  WITH CHECK ADD  CONSTRAINT [FKqonfgtkksl5jruyqth4vcjee6] FOREIGN KEY([incentive_type_id])
REFERENCES [dbo].[tb_incentive_type] ([id])
GO
ALTER TABLE [dbo].[tb_incentive_detail] CHECK CONSTRAINT [FKqonfgtkksl5jruyqth4vcjee6]
GO
ALTER TABLE [dbo].[tb_incentive_detail]  WITH CHECK ADD  CONSTRAINT [FKs589pm5r4h9pcxopewphb268o] FOREIGN KEY([incentive_id])
REFERENCES [dbo].[tb_incentive] ([id])
GO
ALTER TABLE [dbo].[tb_incentive_detail] CHECK CONSTRAINT [FKs589pm5r4h9pcxopewphb268o]
GO
ALTER TABLE [dbo].[tb_incentive_type]  WITH CHECK ADD  CONSTRAINT [FK4ia73rye95t7a6g9ha2bb29na] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_incentive_type] CHECK CONSTRAINT [FK4ia73rye95t7a6g9ha2bb29na]
GO
ALTER TABLE [dbo].[tb_inspection]  WITH CHECK ADD  CONSTRAINT [FKmvdai3wujhw9rwp6cxq042fll] FOREIGN KEY([inspection_type_id])
REFERENCES [dbo].[tb_inspection_type] ([id])
GO
ALTER TABLE [dbo].[tb_inspection] CHECK CONSTRAINT [FKmvdai3wujhw9rwp6cxq042fll]
GO
ALTER TABLE [dbo].[tb_inspection_checklist]  WITH CHECK ADD  CONSTRAINT [FK1xvry6vuqt1t31x258l72a1st] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_inspection_checklist] CHECK CONSTRAINT [FK1xvry6vuqt1t31x258l72a1st]
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail]  WITH CHECK ADD  CONSTRAINT [FK2ldsejh6vvale5ws0hfqbitxt] FOREIGN KEY([inspection_level_id])
REFERENCES [dbo].[tb_inspection_level] ([id])
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail] CHECK CONSTRAINT [FK2ldsejh6vvale5ws0hfqbitxt]
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail]  WITH CHECK ADD  CONSTRAINT [FKgg4g705va0ft3ovkbpwv2wxt1] FOREIGN KEY([inspection_id])
REFERENCES [dbo].[tb_inspection] ([id])
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail] CHECK CONSTRAINT [FKgg4g705va0ft3ovkbpwv2wxt1]
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail]  WITH CHECK ADD  CONSTRAINT [FKrwxdb2g4b3qc23qpq2r8q4yt4] FOREIGN KEY([inspection_checklist_id])
REFERENCES [dbo].[tb_inspection_checklist] ([id])
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail] CHECK CONSTRAINT [FKrwxdb2g4b3qc23qpq2r8q4yt4]
GO
ALTER TABLE [dbo].[tb_invoice]  WITH CHECK ADD  CONSTRAINT [FK598ml90te8h5kkioyrqxr952g] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_invoice] CHECK CONSTRAINT [FK598ml90te8h5kkioyrqxr952g]
GO
ALTER TABLE [dbo].[tb_invoice]  WITH CHECK ADD  CONSTRAINT [FK9sbqhmwy75sv2hbehjq9ei90q] FOREIGN KEY([sales_id])
REFERENCES [dbo].[tb_sales] ([id])
GO
ALTER TABLE [dbo].[tb_invoice] CHECK CONSTRAINT [FK9sbqhmwy75sv2hbehjq9ei90q]
GO
ALTER TABLE [dbo].[tb_invoice]  WITH CHECK ADD  CONSTRAINT [FKjngd8j9806y78hp01a8idnvwi] FOREIGN KEY([service_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_invoice] CHECK CONSTRAINT [FKjngd8j9806y78hp01a8idnvwi]
GO
ALTER TABLE [dbo].[tb_invoice]  WITH CHECK ADD  CONSTRAINT [FKnq73pwn6xeu30xs01goq8rssm] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_invoice] CHECK CONSTRAINT [FKnq73pwn6xeu30xs01goq8rssm]
GO
ALTER TABLE [dbo].[tb_location]  WITH CHECK ADD  CONSTRAINT [FK1ng2o55c7ia0232wtxq7442mo] FOREIGN KEY([location_type_id])
REFERENCES [dbo].[tb_location_type] ([id])
GO
ALTER TABLE [dbo].[tb_location] CHECK CONSTRAINT [FK1ng2o55c7ia0232wtxq7442mo]
GO
ALTER TABLE [dbo].[tb_location]  WITH CHECK ADD  CONSTRAINT [FK7ac2o9ey6mw5fhapcwipgay6m] FOREIGN KEY([region_id])
REFERENCES [dbo].[tb_region] ([id])
GO
ALTER TABLE [dbo].[tb_location] CHECK CONSTRAINT [FK7ac2o9ey6mw5fhapcwipgay6m]
GO
ALTER TABLE [dbo].[tb_location]  WITH CHECK ADD  CONSTRAINT [FK7q78iypopivs92sdgmk8w1e5b] FOREIGN KEY([city_id])
REFERENCES [dbo].[tb_city] ([id])
GO
ALTER TABLE [dbo].[tb_location] CHECK CONSTRAINT [FK7q78iypopivs92sdgmk8w1e5b]
GO
ALTER TABLE [dbo].[tb_location]  WITH CHECK ADD  CONSTRAINT [FK9fbqtkq46slo1u4me55n5ub9n] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_location] CHECK CONSTRAINT [FK9fbqtkq46slo1u4me55n5ub9n]
GO
ALTER TABLE [dbo].[tb_location]  WITH CHECK ADD  CONSTRAINT [FKliubc1niiwxyj45sxnv1fyfqy] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_location] CHECK CONSTRAINT [FKliubc1niiwxyj45sxnv1fyfqy]
GO
ALTER TABLE [dbo].[tb_location]  WITH CHECK ADD  CONSTRAINT [FKpfvl97cxef87uv02j38y5wroa] FOREIGN KEY([state_id])
REFERENCES [dbo].[tb_state] ([id])
GO
ALTER TABLE [dbo].[tb_location] CHECK CONSTRAINT [FKpfvl97cxef87uv02j38y5wroa]
GO
ALTER TABLE [dbo].[tb_outlet]  WITH CHECK ADD  CONSTRAINT [FKcwdxlhjgsrx6wn1heasqr8ibx] FOREIGN KEY([outlet_type_id])
REFERENCES [dbo].[tb_outlet_type] ([id])
GO
ALTER TABLE [dbo].[tb_outlet] CHECK CONSTRAINT [FKcwdxlhjgsrx6wn1heasqr8ibx]
GO
ALTER TABLE [dbo].[tb_outlet]  WITH CHECK ADD  CONSTRAINT [FKhl1jtxxf24druqavwnnd5s2tf] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_outlet] CHECK CONSTRAINT [FKhl1jtxxf24druqavwnnd5s2tf]
GO
ALTER TABLE [dbo].[tb_pay_voucher_vehicle]  WITH CHECK ADD  CONSTRAINT [FKaipmgorcf620suu6srhkybu6h] FOREIGN KEY([payment_voucher_id])
REFERENCES [dbo].[tb_payment_voucher] ([id])
GO
ALTER TABLE [dbo].[tb_pay_voucher_vehicle] CHECK CONSTRAINT [FKaipmgorcf620suu6srhkybu6h]
GO
ALTER TABLE [dbo].[tb_pay_voucher_vehicle]  WITH CHECK ADD  CONSTRAINT [FKrpp6moej3ahujxkq1un837a65] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_pay_voucher_vehicle] CHECK CONSTRAINT [FKrpp6moej3ahujxkq1un837a65]
GO
ALTER TABLE [dbo].[tb_payment]  WITH CHECK ADD  CONSTRAINT [FK36m9di3jpfx3x836a721r7mb9] FOREIGN KEY([payment_user_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_payment] CHECK CONSTRAINT [FK36m9di3jpfx3x836a721r7mb9]
GO
ALTER TABLE [dbo].[tb_payment]  WITH CHECK ADD  CONSTRAINT [FKdglrwbkq8i1isp5s55qt1l07t] FOREIGN KEY([payment_account_id])
REFERENCES [dbo].[tb_payment_account] ([id])
GO
ALTER TABLE [dbo].[tb_payment] CHECK CONSTRAINT [FKdglrwbkq8i1isp5s55qt1l07t]
GO
ALTER TABLE [dbo].[tb_payment_account]  WITH CHECK ADD  CONSTRAINT [FKb71gdnl5sjqjxur7kfsggf8cc] FOREIGN KEY([excise_id])
REFERENCES [dbo].[tb_excise] ([id])
GO
ALTER TABLE [dbo].[tb_payment_account] CHECK CONSTRAINT [FKb71gdnl5sjqjxur7kfsggf8cc]
GO
ALTER TABLE [dbo].[tb_payment_detail]  WITH CHECK ADD  CONSTRAINT [FK83hqb6f2n2x30rbnhdeh28wi3] FOREIGN KEY([payment_id])
REFERENCES [dbo].[tb_payment] ([id])
GO
ALTER TABLE [dbo].[tb_payment_detail] CHECK CONSTRAINT [FK83hqb6f2n2x30rbnhdeh28wi3]
GO
ALTER TABLE [dbo].[tb_payment_history]  WITH CHECK ADD  CONSTRAINT [FK5nalfu0g3n8eu2o0oaapr9tiw] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_payment_history] CHECK CONSTRAINT [FK5nalfu0g3n8eu2o0oaapr9tiw]
GO
ALTER TABLE [dbo].[tb_payment_history]  WITH CHECK ADD  CONSTRAINT [FK6wvdl02nnccnmsyw3wo9j4ixa] FOREIGN KEY([cheque_type_id])
REFERENCES [dbo].[tb_cheque_type] ([id])
GO
ALTER TABLE [dbo].[tb_payment_history] CHECK CONSTRAINT [FK6wvdl02nnccnmsyw3wo9j4ixa]
GO
ALTER TABLE [dbo].[tb_payment_history]  WITH CHECK ADD  CONSTRAINT [FKdr19rv0f0ui0ohq1ty8uxwc1m] FOREIGN KEY([sales_id])
REFERENCES [dbo].[tb_sales] ([id])
GO
ALTER TABLE [dbo].[tb_payment_history] CHECK CONSTRAINT [FKdr19rv0f0ui0ohq1ty8uxwc1m]
GO
ALTER TABLE [dbo].[tb_payment_history]  WITH CHECK ADD  CONSTRAINT [FKh2n43r9fmrhgt7p26qif1xia1] FOREIGN KEY([payment_mode_id])
REFERENCES [dbo].[tb_payment_mode] ([id])
GO
ALTER TABLE [dbo].[tb_payment_history] CHECK CONSTRAINT [FKh2n43r9fmrhgt7p26qif1xia1]
GO
ALTER TABLE [dbo].[tb_payment_history]  WITH CHECK ADD  CONSTRAINT [FKjcsy9u98wuwpjx449fo4tf3k] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_payment_history] CHECK CONSTRAINT [FKjcsy9u98wuwpjx449fo4tf3k]
GO
ALTER TABLE [dbo].[tb_payment_history]  WITH CHECK ADD  CONSTRAINT [FKsgikpl30553xr8uf7hg40rusw] FOREIGN KEY([bank_in_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_payment_history] CHECK CONSTRAINT [FKsgikpl30553xr8uf7hg40rusw]
GO
ALTER TABLE [dbo].[tb_payment_request]  WITH CHECK ADD  CONSTRAINT [FK78831ml6c51e0j8qdbnfofwij] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_payment_request] CHECK CONSTRAINT [FK78831ml6c51e0j8qdbnfofwij]
GO
ALTER TABLE [dbo].[tb_payment_request]  WITH CHECK ADD  CONSTRAINT [FKb9kp3fnce3rxcho3e3g5n1nf7] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_payment_request] CHECK CONSTRAINT [FKb9kp3fnce3rxcho3e3g5n1nf7]
GO
ALTER TABLE [dbo].[tb_payment_request]  WITH CHECK ADD  CONSTRAINT [FKbr3myh6hhbs014ngv98srjmgx] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_payment_request] CHECK CONSTRAINT [FKbr3myh6hhbs014ngv98srjmgx]
GO
ALTER TABLE [dbo].[tb_payment_request]  WITH CHECK ADD  CONSTRAINT [FKlr8ejxcn3bksby222y4vr5amb] FOREIGN KEY([dealer_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_payment_request] CHECK CONSTRAINT [FKlr8ejxcn3bksby222y4vr5amb]
GO
ALTER TABLE [dbo].[tb_payment_terms_variant]  WITH CHECK ADD  CONSTRAINT [FKc3hxeknve3agri14949ld4yrl] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_payment_terms_variant] CHECK CONSTRAINT [FKc3hxeknve3agri14949ld4yrl]
GO
ALTER TABLE [dbo].[tb_payment_terms_variant]  WITH CHECK ADD  CONSTRAINT [FKqu64pa8cy2y9mntdat2r0j00g] FOREIGN KEY([vehicle_payment_term_id])
REFERENCES [dbo].[tb_vehicle_payment_term] ([id])
GO
ALTER TABLE [dbo].[tb_payment_terms_variant] CHECK CONSTRAINT [FKqu64pa8cy2y9mntdat2r0j00g]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FK5ho7il18bd2palrl29wk5wko7] FOREIGN KEY([cust_credit_note_id])
REFERENCES [dbo].[tb_cust_credit_note] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FK5ho7il18bd2palrl29wk5wko7]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FK6pbgmyg2ok0lni1dlufnbycf4] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FK6pbgmyg2ok0lni1dlufnbycf4]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FK89mksmq2xuyphvv6pkk0s9nya] FOREIGN KEY([business_process_id])
REFERENCES [dbo].[tb_business_process] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FK89mksmq2xuyphvv6pkk0s9nya]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKd3ph1chumq3tk5oo9gww78a5w] FOREIGN KEY([cheque_type_id])
REFERENCES [dbo].[tb_cheque_type] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKd3ph1chumq3tk5oo9gww78a5w]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKjt8djlsmnne99s0e0ae11ls4g] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKjt8djlsmnne99s0e0ae11ls4g]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKk2ndyaguw4u2iwys6s9b6jobj] FOREIGN KEY([approver_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKk2ndyaguw4u2iwys6s9b6jobj]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKlre0s8x3xfwpvlwqd0obvhr1g] FOREIGN KEY([credit_note_id])
REFERENCES [dbo].[tb_credit_note] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKlre0s8x3xfwpvlwqd0obvhr1g]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKp2m7op08n5k9lgugxyno4pgny] FOREIGN KEY([outlet_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKp2m7op08n5k9lgugxyno4pgny]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKp93x5iq6q2ruqinmcqe1el9xp] FOREIGN KEY([payment_mode_id])
REFERENCES [dbo].[tb_payment_mode] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKp93x5iq6q2ruqinmcqe1el9xp]
GO
ALTER TABLE [dbo].[tb_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKq35dt6nqkck7y22gxw6m66764] FOREIGN KEY([sales_id])
REFERENCES [dbo].[tb_sales] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher] CHECK CONSTRAINT [FKq35dt6nqkck7y22gxw6m66764]
GO
ALTER TABLE [dbo].[tb_payment_voucher_result]  WITH CHECK ADD  CONSTRAINT [FK7bsfyiekdih56b7pfcasycff8] FOREIGN KEY([payment_voucher_id])
REFERENCES [dbo].[tb_payment_voucher] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher_result] CHECK CONSTRAINT [FK7bsfyiekdih56b7pfcasycff8]
GO
ALTER TABLE [dbo].[tb_payment_voucher_result]  WITH CHECK ADD  CONSTRAINT [FKa79wbrd3oemj58r1rmxaejvg1] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_payment_voucher_result] CHECK CONSTRAINT [FKa79wbrd3oemj58r1rmxaejvg1]
GO
ALTER TABLE [dbo].[tb_price_plan]  WITH CHECK ADD  CONSTRAINT [FK1edhvoivoy89s3e93lstpnfdq] FOREIGN KEY([usage_type_id])
REFERENCES [dbo].[tb_usage_type] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan] CHECK CONSTRAINT [FK1edhvoivoy89s3e93lstpnfdq]
GO
ALTER TABLE [dbo].[tb_price_plan]  WITH CHECK ADD  CONSTRAINT [FK3irxs6tldu526xwr7urv1sjkr] FOREIGN KEY([customer_type_id])
REFERENCES [dbo].[tb_customer_type] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan] CHECK CONSTRAINT [FK3irxs6tldu526xwr7urv1sjkr]
GO
ALTER TABLE [dbo].[tb_price_plan]  WITH CHECK ADD  CONSTRAINT [FK5vab8ltha0vap7r96ysbb3apm] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan] CHECK CONSTRAINT [FK5vab8ltha0vap7r96ysbb3apm]
GO
ALTER TABLE [dbo].[tb_price_plan]  WITH CHECK ADD  CONSTRAINT [FKhwoak51g61kqrup8bb8cges8c] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan] CHECK CONSTRAINT [FKhwoak51g61kqrup8bb8cges8c]
GO
ALTER TABLE [dbo].[tb_price_plan]  WITH CHECK ADD  CONSTRAINT [FKk7fkv2a5492m73q5t1d787tvq] FOREIGN KEY([currency_id])
REFERENCES [dbo].[tb_currency] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan] CHECK CONSTRAINT [FKk7fkv2a5492m73q5t1d787tvq]
GO
ALTER TABLE [dbo].[tb_price_plan_variant]  WITH CHECK ADD  CONSTRAINT [FK8upnqtq4dqhsj2k4ml60rudnw] FOREIGN KEY([price_plan_id])
REFERENCES [dbo].[tb_price_plan] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan_variant] CHECK CONSTRAINT [FK8upnqtq4dqhsj2k4ml60rudnw]
GO
ALTER TABLE [dbo].[tb_price_plan_variant]  WITH CHECK ADD  CONSTRAINT [FKfjramdbl0p5i9gsbr0nkyxet2] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan_variant] CHECK CONSTRAINT [FKfjramdbl0p5i9gsbr0nkyxet2]
GO
ALTER TABLE [dbo].[tb_price_plan_variant_detail]  WITH CHECK ADD  CONSTRAINT [FK18l7754tjsk8q40u3qpy045fh] FOREIGN KEY([config_field_id])
REFERENCES [dbo].[tb_config_field] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan_variant_detail] CHECK CONSTRAINT [FK18l7754tjsk8q40u3qpy045fh]
GO
ALTER TABLE [dbo].[tb_price_plan_variant_detail]  WITH CHECK ADD  CONSTRAINT [FK2nuacsc2br5eydw4nl2098bl9] FOREIGN KEY([price_plan_variant_id])
REFERENCES [dbo].[tb_price_plan_variant] ([id])
GO
ALTER TABLE [dbo].[tb_price_plan_variant_detail] CHECK CONSTRAINT [FK2nuacsc2br5eydw4nl2098bl9]
GO
ALTER TABLE [dbo].[tb_promotion]  WITH CHECK ADD  CONSTRAINT [FKga38606wl3gtlmbc9gpjwch68] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_promotion] CHECK CONSTRAINT [FKga38606wl3gtlmbc9gpjwch68]
GO
ALTER TABLE [dbo].[tb_promotion]  WITH CHECK ADD  CONSTRAINT [FKknub2dq2a4p57l5rapcwr5lll] FOREIGN KEY([promotion_type_id])
REFERENCES [dbo].[tb_promotion_type] ([id])
GO
ALTER TABLE [dbo].[tb_promotion] CHECK CONSTRAINT [FKknub2dq2a4p57l5rapcwr5lll]
GO
ALTER TABLE [dbo].[tb_promotion]  WITH CHECK ADD  CONSTRAINT [FKs95b38k51iq18ijnbyfnoycrw] FOREIGN KEY([service_plan_id])
REFERENCES [dbo].[tb_service_plan] ([id])
GO
ALTER TABLE [dbo].[tb_promotion] CHECK CONSTRAINT [FKs95b38k51iq18ijnbyfnoycrw]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FK1kcm10nc20hm3d4x314oa5wf4] FOREIGN KEY([warranty_plan_id])
REFERENCES [dbo].[tb_warranty_plan] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FK1kcm10nc20hm3d4x314oa5wf4]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FK7k7qnug6fy3emjaghgtgeywuc] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FK7k7qnug6fy3emjaghgtgeywuc]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FK7s5phg2d38vs3an95cvob2rm1] FOREIGN KEY([usage_type_id])
REFERENCES [dbo].[tb_usage_type] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FK7s5phg2d38vs3an95cvob2rm1]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FKgilch6hcddiwbo0qna4qtm8ty] FOREIGN KEY([service_plan_id])
REFERENCES [dbo].[tb_service_plan] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FKgilch6hcddiwbo0qna4qtm8ty]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FKo4yn7uv4c7mu734dh2lehgfgb] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FKo4yn7uv4c7mu734dh2lehgfgb]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FKo77d7myeo7n2g9x4yttx0d6gg] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FKo77d7myeo7n2g9x4yttx0d6gg]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FKoqj8ida2dg3r73ypci1od4ij8] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FKoqj8ida2dg3r73ypci1od4ij8]
GO
ALTER TABLE [dbo].[tb_quotation]  WITH CHECK ADD  CONSTRAINT [FKqoipr16wf34tq47p1y7u3flbr] FOREIGN KEY([colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_quotation] CHECK CONSTRAINT [FKqoipr16wf34tq47p1y7u3flbr]
GO
ALTER TABLE [dbo].[tb_quotation_accessory]  WITH CHECK ADD  CONSTRAINT [FK3343njtxecd8rbd2yjweo7qc4] FOREIGN KEY([sta_accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_quotation_accessory] CHECK CONSTRAINT [FK3343njtxecd8rbd2yjweo7qc4]
GO
ALTER TABLE [dbo].[tb_quotation_accessory]  WITH CHECK ADD  CONSTRAINT [FKa1192wq73891sj0uq4ccpej71] FOREIGN KEY([accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_quotation_accessory] CHECK CONSTRAINT [FKa1192wq73891sj0uq4ccpej71]
GO
ALTER TABLE [dbo].[tb_quotation_accessory]  WITH CHECK ADD  CONSTRAINT [FKhode9y9w1frhhp8ajvm2198d1] FOREIGN KEY([quotation_id])
REFERENCES [dbo].[tb_quotation] ([id])
GO
ALTER TABLE [dbo].[tb_quotation_accessory] CHECK CONSTRAINT [FKhode9y9w1frhhp8ajvm2198d1]
GO
ALTER TABLE [dbo].[tb_quotation_price_detail]  WITH CHECK ADD  CONSTRAINT [FKkq0blaxoxveaceux7petx2x1a] FOREIGN KEY([config_field_id])
REFERENCES [dbo].[tb_config_field] ([id])
GO
ALTER TABLE [dbo].[tb_quotation_price_detail] CHECK CONSTRAINT [FKkq0blaxoxveaceux7petx2x1a]
GO
ALTER TABLE [dbo].[tb_quotation_price_detail]  WITH CHECK ADD  CONSTRAINT [FKpvucs4qyry1478sx57od9kej4] FOREIGN KEY([quotation_id])
REFERENCES [dbo].[tb_quotation] ([id])
GO
ALTER TABLE [dbo].[tb_quotation_price_detail] CHECK CONSTRAINT [FKpvucs4qyry1478sx57od9kej4]
GO
ALTER TABLE [dbo].[tb_quotation_track]  WITH CHECK ADD  CONSTRAINT [FKsx2vj2imeiwcu37adkp0up0af] FOREIGN KEY([quotation_id])
REFERENCES [dbo].[tb_quotation] ([id])
GO
ALTER TABLE [dbo].[tb_quotation_track] CHECK CONSTRAINT [FKsx2vj2imeiwcu37adkp0up0af]
GO
ALTER TABLE [dbo].[tb_region]  WITH CHECK ADD  CONSTRAINT [FKax2orrd96ep90vrd6c1soxd83] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_region] CHECK CONSTRAINT [FKax2orrd96ep90vrd6c1soxd83]
GO
ALTER TABLE [dbo].[tb_register_out_pay]  WITH CHECK ADD  CONSTRAINT [FKlhsi1w88xcnbgw3yrsnil9443] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_register_out_pay] CHECK CONSTRAINT [FKlhsi1w88xcnbgw3yrsnil9443]
GO
ALTER TABLE [dbo].[tb_register_out_pay]  WITH CHECK ADD  CONSTRAINT [FKrowtilkqdjxjg4o96lvisqtgn] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_register_out_pay] CHECK CONSTRAINT [FKrowtilkqdjxjg4o96lvisqtgn]
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail]  WITH CHECK ADD  CONSTRAINT [FKifyxsl3vl24teodad76sa9q62] FOREIGN KEY([sales_id])
REFERENCES [dbo].[tb_sales] ([id])
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail] CHECK CONSTRAINT [FKifyxsl3vl24teodad76sa9q62]
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail]  WITH CHECK ADD  CONSTRAINT [FKrpv66307mc6ytui9ggufskfkj] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail] CHECK CONSTRAINT [FKrpv66307mc6ytui9ggufskfkj]
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail]  WITH CHECK ADD  CONSTRAINT [FKtc9jqifl96jhmks3pys1dlj4k] FOREIGN KEY([register_out_pay_id])
REFERENCES [dbo].[tb_register_out_pay] ([id])
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail] CHECK CONSTRAINT [FKtc9jqifl96jhmks3pys1dlj4k]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FK15seopu2f62yvjiacek51pbar] FOREIGN KEY([dealer_request_order_id])
REFERENCES [dbo].[tb_dealer_request_order] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FK15seopu2f62yvjiacek51pbar]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FK1djbs1v9m5tbciwhs4wn4b613] FOREIGN KEY([buyer_order_id])
REFERENCES [dbo].[tb_buyer_order] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FK1djbs1v9m5tbciwhs4wn4b613]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FK5vl7fgph362ex0qt7jic4bucu] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FK5vl7fgph362ex0qt7jic4bucu]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FK6ikqcqhq216j84hxpkfhtnj1d] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FK6ikqcqhq216j84hxpkfhtnj1d]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FK8dre7ymlbdo5kob0ficbs2wt3] FOREIGN KEY([second_opt_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FK8dre7ymlbdo5kob0ficbs2wt3]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FK9x49qgswa4eq6hedgoht2eqsy] FOREIGN KEY([first_opt_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FK9x49qgswa4eq6hedgoht2eqsy]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FKbenbmewxyy71jt7k85rer1lsu] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FKbenbmewxyy71jt7k85rer1lsu]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FKe2br24kl2d3wf5ju9c1p34k1h] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FKe2br24kl2d3wf5ju9c1p34k1h]
GO
ALTER TABLE [dbo].[tb_request_detail]  WITH CHECK ADD  CONSTRAINT [FKff332upd3niorxcy0mic1xesp] FOREIGN KEY([colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_request_detail] CHECK CONSTRAINT [FKff332upd3niorxcy0mic1xesp]
GO
ALTER TABLE [dbo].[tb_request_track]  WITH CHECK ADD  CONSTRAINT [FK8ndbfn3lbfrqa623dy0h4ntkx] FOREIGN KEY([request_order_id])
REFERENCES [dbo].[tb_dealer_request_order] ([id])
GO
ALTER TABLE [dbo].[tb_request_track] CHECK CONSTRAINT [FK8ndbfn3lbfrqa623dy0h4ntkx]
GO
ALTER TABLE [dbo].[tb_role]  WITH CHECK ADD  CONSTRAINT [FK4ba5xyitcoyw84rhap0e253iv] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_role] CHECK CONSTRAINT [FK4ba5xyitcoyw84rhap0e253iv]
GO
ALTER TABLE [dbo].[tb_role_function]  WITH CHECK ADD  CONSTRAINT [FKosnkj24ed9vyuhwp48jf18ek2] FOREIGN KEY([business_function_id])
REFERENCES [dbo].[tb_business_function] ([id])
GO
ALTER TABLE [dbo].[tb_role_function] CHECK CONSTRAINT [FKosnkj24ed9vyuhwp48jf18ek2]
GO
ALTER TABLE [dbo].[tb_role_function]  WITH CHECK ADD  CONSTRAINT [FKqmh2ewr5sc7lj2ssbvjknnk0l] FOREIGN KEY([role_id])
REFERENCES [dbo].[tb_role] ([id])
GO
ALTER TABLE [dbo].[tb_role_function] CHECK CONSTRAINT [FKqmh2ewr5sc7lj2ssbvjknnk0l]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FK3m8njcmq8rx3eidg5i6fqgxkj] FOREIGN KEY([quotation_id])
REFERENCES [dbo].[tb_quotation] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FK3m8njcmq8rx3eidg5i6fqgxkj]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FK4wnae50p589susr5hbbacjhe2] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FK4wnae50p589susr5hbbacjhe2]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FK5mt95bd2i1if55kvkh4s0ymd2] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FK5mt95bd2i1if55kvkh4s0ymd2]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FK6r68tgr8kqmt9webqykj5urwm] FOREIGN KEY([sales_executive_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FK6r68tgr8kqmt9webqykj5urwm]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FK8vrmw2e8n4h8bv71s67p6xjba] FOREIGN KEY([buyer_order_id])
REFERENCES [dbo].[tb_buyer_order] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FK8vrmw2e8n4h8bv71s67p6xjba]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FK8xmdvjo9ebvxqsqk3qaw8mhk8] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FK8xmdvjo9ebvxqsqk3qaw8mhk8]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FKjid9bgqtnk3kqllvmvrnnavib] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FKjid9bgqtnk3kqllvmvrnnavib]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FKklsub2ltoyr7knehueusegumi] FOREIGN KEY([dealer_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FKklsub2ltoyr7knehueusegumi]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FKnr4w7l4swtbxcb1dqoigapsdi] FOREIGN KEY([invoice_id])
REFERENCES [dbo].[tb_invoice] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FKnr4w7l4swtbxcb1dqoigapsdi]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FKrbnu8qfv3hfk3kv7xyetnht3c] FOREIGN KEY([colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FKrbnu8qfv3hfk3kv7xyetnht3c]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FKrv0vi1hnwqcyhg5u6vma1q3nk] FOREIGN KEY([sales_order_id])
REFERENCES [dbo].[tb_sales_order] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FKrv0vi1hnwqcyhg5u6vma1q3nk]
GO
ALTER TABLE [dbo].[tb_sales]  WITH CHECK ADD  CONSTRAINT [FKsutj5a4375y2lbt0jxnj4154f] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_sales] CHECK CONSTRAINT [FKsutj5a4375y2lbt0jxnj4154f]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FK529igmrrlnn4bgln7qjrbo7gk] FOREIGN KEY([sales_executive_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FK529igmrrlnn4bgln7qjrbo7gk]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FKeqg5nk8du3v6dcxgm8ehg20pc] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FKeqg5nk8du3v6dcxgm8ehg20pc]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FKhbfb0qvff2lg3ldsw0b6qnq52] FOREIGN KEY([dealer_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FKhbfb0qvff2lg3ldsw0b6qnq52]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FKhj74wg262d5q2a3a42pnv0krq] FOREIGN KEY([buyer_order_id])
REFERENCES [dbo].[tb_buyer_order] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FKhj74wg262d5q2a3a42pnv0krq]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FKhx3tkj459vs4c2trih1m4mq3] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FKhx3tkj459vs4c2trih1m4mq3]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FKimybfqfars3qla60wjurilnbl] FOREIGN KEY([insurance_company_id])
REFERENCES [dbo].[tb_insurance_company] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FKimybfqfars3qla60wjurilnbl]
GO
ALTER TABLE [dbo].[tb_sales_order]  WITH CHECK ADD  CONSTRAINT [FKtr8tks7jhhaf62qnmcc0h0lj7] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order] CHECK CONSTRAINT [FKtr8tks7jhhaf62qnmcc0h0lj7]
GO
ALTER TABLE [dbo].[tb_sales_order_accessory]  WITH CHECK ADD  CONSTRAINT [FK11l4xcwuveya10qpm2yw46pyj] FOREIGN KEY([sales_order_id])
REFERENCES [dbo].[tb_sales_order] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order_accessory] CHECK CONSTRAINT [FK11l4xcwuveya10qpm2yw46pyj]
GO
ALTER TABLE [dbo].[tb_sales_order_accessory]  WITH CHECK ADD  CONSTRAINT [FK14qp8qxbsdkds2ebvnist6t93] FOREIGN KEY([sta_accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order_accessory] CHECK CONSTRAINT [FK14qp8qxbsdkds2ebvnist6t93]
GO
ALTER TABLE [dbo].[tb_sales_order_accessory]  WITH CHECK ADD  CONSTRAINT [FK7xogayygix5mnp8tspkxmciab] FOREIGN KEY([accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order_accessory] CHECK CONSTRAINT [FK7xogayygix5mnp8tspkxmciab]
GO
ALTER TABLE [dbo].[tb_sales_order_price_detail]  WITH CHECK ADD  CONSTRAINT [FKpmchmbukvpfjbi90q7d45f15e] FOREIGN KEY([config_field_id])
REFERENCES [dbo].[tb_config_field] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order_price_detail] CHECK CONSTRAINT [FKpmchmbukvpfjbi90q7d45f15e]
GO
ALTER TABLE [dbo].[tb_sales_order_price_detail]  WITH CHECK ADD  CONSTRAINT [FKpp8cka9m2a2j7d4a98tgijr66] FOREIGN KEY([sales_order_id])
REFERENCES [dbo].[tb_sales_order] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order_price_detail] CHECK CONSTRAINT [FKpp8cka9m2a2j7d4a98tgijr66]
GO
ALTER TABLE [dbo].[tb_sales_order_track]  WITH CHECK ADD  CONSTRAINT [FKr870sksg2h4928b3q9gl7d1g2] FOREIGN KEY([sales_order_id])
REFERENCES [dbo].[tb_sales_order] ([id])
GO
ALTER TABLE [dbo].[tb_sales_order_track] CHECK CONSTRAINT [FKr870sksg2h4928b3q9gl7d1g2]
GO
ALTER TABLE [dbo].[tb_service]  WITH CHECK ADD  CONSTRAINT [FKg55gi409aqlakt3oxclc044v6] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_service] CHECK CONSTRAINT [FKg55gi409aqlakt3oxclc044v6]
GO
ALTER TABLE [dbo].[tb_service]  WITH CHECK ADD  CONSTRAINT [FKse4fmjipj69r18kxmvxhr2igr] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_service] CHECK CONSTRAINT [FKse4fmjipj69r18kxmvxhr2igr]
GO
ALTER TABLE [dbo].[tb_service_record]  WITH CHECK ADD  CONSTRAINT [FKs3fwdh432dqu7x7ubn5rxmtlv] FOREIGN KEY([service_id])
REFERENCES [dbo].[tb_service] ([id])
GO
ALTER TABLE [dbo].[tb_service_record] CHECK CONSTRAINT [FKs3fwdh432dqu7x7ubn5rxmtlv]
GO
ALTER TABLE [dbo].[tb_state]  WITH CHECK ADD  CONSTRAINT [FKjln74j2yroa9uytbulswttfi9] FOREIGN KEY([region_id])
REFERENCES [dbo].[tb_region] ([id])
GO
ALTER TABLE [dbo].[tb_state] CHECK CONSTRAINT [FKjln74j2yroa9uytbulswttfi9]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_country]  WITH CHECK ADD  CONSTRAINT [FKibrnyfqelnm4nyxwwlp4swxo7] FOREIGN KEY([sublet_contractor_id])
REFERENCES [dbo].[tb_sublet_contractor] ([id])
GO
ALTER TABLE [dbo].[tb_sublet_contractor_country] CHECK CONSTRAINT [FKibrnyfqelnm4nyxwwlp4swxo7]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_country]  WITH CHECK ADD  CONSTRAINT [FKt4x9eaqqdsevauuvisdn8b00m] FOREIGN KEY([country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_sublet_contractor_country] CHECK CONSTRAINT [FKt4x9eaqqdsevauuvisdn8b00m]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_outlet]  WITH CHECK ADD  CONSTRAINT [FKbisb3v2ndr6rd8c1fome790i] FOREIGN KEY([sublet_contractor_id])
REFERENCES [dbo].[tb_sublet_contractor] ([id])
GO
ALTER TABLE [dbo].[tb_sublet_contractor_outlet] CHECK CONSTRAINT [FKbisb3v2ndr6rd8c1fome790i]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_region]  WITH CHECK ADD  CONSTRAINT [FKbwvrm90x2cs4b1kw38iqaeigi] FOREIGN KEY([sublet_contractor_id])
REFERENCES [dbo].[tb_sublet_contractor] ([id])
GO
ALTER TABLE [dbo].[tb_sublet_contractor_region] CHECK CONSTRAINT [FKbwvrm90x2cs4b1kw38iqaeigi]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_region]  WITH CHECK ADD  CONSTRAINT [FKdl1ub4o5wo5pvtc4dw1hyyqri] FOREIGN KEY([region_id])
REFERENCES [dbo].[tb_region] ([id])
GO
ALTER TABLE [dbo].[tb_sublet_contractor_region] CHECK CONSTRAINT [FKdl1ub4o5wo5pvtc4dw1hyyqri]
GO
ALTER TABLE [dbo].[tb_trade_in]  WITH CHECK ADD  CONSTRAINT [FKlkynpmofnberjvn8qkp1ixb1e] FOREIGN KEY([insurance_company_id])
REFERENCES [dbo].[tb_insurance_company] ([id])
GO
ALTER TABLE [dbo].[tb_trade_in] CHECK CONSTRAINT [FKlkynpmofnberjvn8qkp1ixb1e]
GO
ALTER TABLE [dbo].[tb_trade_in]  WITH CHECK ADD  CONSTRAINT [FKod8i1bcfketsp7y3didt3kiop] FOREIGN KEY([trade_in_brand_id])
REFERENCES [dbo].[tb_trade_in_brand] ([id])
GO
ALTER TABLE [dbo].[tb_trade_in] CHECK CONSTRAINT [FKod8i1bcfketsp7y3didt3kiop]
GO
ALTER TABLE [dbo].[tb_trim_package_detail]  WITH CHECK ADD  CONSTRAINT [FKd22oqr77k6sa3hxt0syi1yj1i] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_trim_package_detail] CHECK CONSTRAINT [FKd22oqr77k6sa3hxt0syi1yj1i]
GO
ALTER TABLE [dbo].[tb_trim_package_detail]  WITH CHECK ADD  CONSTRAINT [FKnv3osn03m69onku64u2hoopql] FOREIGN KEY([trim_package_category_id])
REFERENCES [dbo].[tb_trim_package_category] ([id])
GO
ALTER TABLE [dbo].[tb_trim_package_detail] CHECK CONSTRAINT [FKnv3osn03m69onku64u2hoopql]
GO
ALTER TABLE [dbo].[tb_user_role]  WITH CHECK ADD  CONSTRAINT [FK7vn3h53d0tqdimm8cp45gc0kl] FOREIGN KEY([user_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_user_role] CHECK CONSTRAINT [FK7vn3h53d0tqdimm8cp45gc0kl]
GO
ALTER TABLE [dbo].[tb_user_role]  WITH CHECK ADD  CONSTRAINT [FKea2ootw6b6bb0xt3ptl28bymv] FOREIGN KEY([role_id])
REFERENCES [dbo].[tb_role] ([id])
GO
ALTER TABLE [dbo].[tb_user_role] CHECK CONSTRAINT [FKea2ootw6b6bb0xt3ptl28bymv]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FK2r9y6xda2ynr0yv2nocvojf97] FOREIGN KEY([vehicle_use_id])
REFERENCES [dbo].[tb_vehicle_use] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FK2r9y6xda2ynr0yv2nocvojf97]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FK36gbr5vwotfno8luma0c38ms6] FOREIGN KEY([variant_type_id])
REFERENCES [dbo].[tb_variant_type] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FK36gbr5vwotfno8luma0c38ms6]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FK3u4u90tu4vq5ihy2gqn9l8s75] FOREIGN KEY([manufacturer_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FK3u4u90tu4vq5ihy2gqn9l8s75]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FK5ao7ngy130qk95huymh491nxc] FOREIGN KEY([fuel_type_id])
REFERENCES [dbo].[tb_fuel_type] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FK5ao7ngy130qk95huymh491nxc]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FK61osmk240s4puc9bgl8g39vfe] FOREIGN KEY([gst_tax_id])
REFERENCES [dbo].[tb_gst_tax] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FK61osmk240s4puc9bgl8g39vfe]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FKchasoo3mjl2agr2mv0xf10ogj] FOREIGN KEY([body_style_id])
REFERENCES [dbo].[tb_vehicle_body_style] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FKchasoo3mjl2agr2mv0xf10ogj]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FKe7twp918ogusxld53oo0a817s] FOREIGN KEY([vehicle_model_id])
REFERENCES [dbo].[tb_vehicle_model] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FKe7twp918ogusxld53oo0a817s]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FKhrgemqqmtmgoxmal49tjuig2l] FOREIGN KEY([original_status_id])
REFERENCES [dbo].[tb_original_status] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FKhrgemqqmtmgoxmal49tjuig2l]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FKj6qi29qufloqprhe76t0u2a9v] FOREIGN KEY([currency_id])
REFERENCES [dbo].[tb_currency] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FKj6qi29qufloqprhe76t0u2a9v]
GO
ALTER TABLE [dbo].[tb_variant]  WITH CHECK ADD  CONSTRAINT [FKt3mjil1extaw9rfyvcxti765q] FOREIGN KEY([body_type_id])
REFERENCES [dbo].[tb_vehicle_body_type] ([id])
GO
ALTER TABLE [dbo].[tb_variant] CHECK CONSTRAINT [FKt3mjil1extaw9rfyvcxti765q]
GO
ALTER TABLE [dbo].[tb_variant_accessory]  WITH CHECK ADD  CONSTRAINT [FKhge0ysx96re7flt5rykvd710y] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_variant_accessory] CHECK CONSTRAINT [FKhge0ysx96re7flt5rykvd710y]
GO
ALTER TABLE [dbo].[tb_variant_accessory]  WITH CHECK ADD  CONSTRAINT [FKjr0ex00raa5fina9367grsddk] FOREIGN KEY([accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_variant_accessory] CHECK CONSTRAINT [FKjr0ex00raa5fina9367grsddk]
GO
ALTER TABLE [dbo].[tb_variant_colour]  WITH CHECK ADD  CONSTRAINT [FK3sl91h7ijd1i27ge812sy5krd] FOREIGN KEY([commercial_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_variant_colour] CHECK CONSTRAINT [FK3sl91h7ijd1i27ge812sy5krd]
GO
ALTER TABLE [dbo].[tb_variant_colour]  WITH CHECK ADD  CONSTRAINT [FKsdjafa3yqti7s60li3kg2lnur] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_variant_colour] CHECK CONSTRAINT [FKsdjafa3yqti7s60li3kg2lnur]
GO
ALTER TABLE [dbo].[tb_variant_loose_item]  WITH CHECK ADD  CONSTRAINT [FKaoeopeww95elheqm3nkj4n3mr] FOREIGN KEY([loose_item_id])
REFERENCES [dbo].[tb_loose_item] ([id])
GO
ALTER TABLE [dbo].[tb_variant_loose_item] CHECK CONSTRAINT [FKaoeopeww95elheqm3nkj4n3mr]
GO
ALTER TABLE [dbo].[tb_variant_loose_item]  WITH CHECK ADD  CONSTRAINT [FKgjj9qo1fjimc9ovueoexxbo1m] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_variant_loose_item] CHECK CONSTRAINT [FKgjj9qo1fjimc9ovueoexxbo1m]
GO
ALTER TABLE [dbo].[tb_variant_trim_package]  WITH CHECK ADD  CONSTRAINT [FK8rlj6uenaus8a4dwhs183ovub] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_variant_trim_package] CHECK CONSTRAINT [FK8rlj6uenaus8a4dwhs183ovub]
GO
ALTER TABLE [dbo].[tb_variant_trim_package]  WITH CHECK ADD  CONSTRAINT [FKdjm27e5bxnur8hsh9sg49gexx] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_variant_trim_package] CHECK CONSTRAINT [FKdjm27e5bxnur8hsh9sg49gexx]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK1g82b50do5nm9k3gq9wqicwld] FOREIGN KEY([tagged_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK1g82b50do5nm9k3gq9wqicwld]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK1gyqjggcjo2ouorbl1r6wjp3m] FOREIGN KEY([sublet_po_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK1gyqjggcjo2ouorbl1r6wjp3m]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK3c240gyk0jox1r31vgmscjk2e] FOREIGN KEY([location_id])
REFERENCES [dbo].[tb_location] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK3c240gyk0jox1r31vgmscjk2e]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK46et323awotjmwjrjp1wp59kx] FOREIGN KEY([location_type_id])
REFERENCES [dbo].[tb_location_type] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK46et323awotjmwjrjp1wp59kx]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK4u8mrr3vvbdc94d8voyiw1glb] FOREIGN KEY([company_location_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK4u8mrr3vvbdc94d8voyiw1glb]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK5qayntey8vg8ol30fj867xold] FOREIGN KEY([commercial_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK5qayntey8vg8ol30fj867xold]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK6j134cjbwclfhqtfr5376m8m0] FOREIGN KEY([pay_voucher_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK6j134cjbwclfhqtfr5376m8m0]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK6ky05pki1rn169hcyg2eq2i4r] FOREIGN KEY([doc_release_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK6ky05pki1rn169hcyg2eq2i4r]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK6rxe4q1rbibiv17k4tyvm7p38] FOREIGN KEY([model_id])
REFERENCES [dbo].[tb_vehicle_model] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK6rxe4q1rbibiv17k4tyvm7p38]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK9c0qfy088s5x88jc7h1jit6cb] FOREIGN KEY([kdrm_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK9c0qfy088s5x88jc7h1jit6cb]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FK9helpdtmtfkmbcjnhl65m24n3] FOREIGN KEY([new_delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FK9helpdtmtfkmbcjnhl65m24n3]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKayf6xmjjynb20i5ydp69ek6gr] FOREIGN KEY([kastam_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKayf6xmjjynb20i5ydp69ek6gr]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKcwtvge253ob1hiv3asrnnncbl] FOREIGN KEY([department_id])
REFERENCES [dbo].[tb_department] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKcwtvge253ob1hiv3asrnnncbl]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKdhqq3da7lbjd2u97dguspo10y] FOREIGN KEY([vehicle_type_id])
REFERENCES [dbo].[tb_vehicle_type] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKdhqq3da7lbjd2u97dguspo10y]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKfgu681irsn56oebtwwxkek66e] FOREIGN KEY([body_type_id])
REFERENCES [dbo].[tb_vehicle_body_type] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKfgu681irsn56oebtwwxkek66e]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKioiux0ndtw54ecm2b91o8wu39] FOREIGN KEY([old_delivery_id])
REFERENCES [dbo].[tb_delivery] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKioiux0ndtw54ecm2b91o8wu39]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKire4q2j44td5l2dltb9wivmlq] FOREIGN KEY([dest_country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKire4q2j44td5l2dltb9wivmlq]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKixccg14xfax6s8tp8u34mnki] FOREIGN KEY([body_style_id])
REFERENCES [dbo].[tb_vehicle_body_style] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKixccg14xfax6s8tp8u34mnki]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKj2t8atm9yf8xc2a58gnouuw3i] FOREIGN KEY([pdi_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKj2t8atm9yf8xc2a58gnouuw3i]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKjcjrerqnj390gsq1oletm87qj] FOREIGN KEY([allocation_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKjcjrerqnj390gsq1oletm87qj]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKjmb0w3rrcrx4hg3f4kx3l3aat] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKjmb0w3rrcrx4hg3f4kx3l3aat]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKkew4r1m266colqb495qwabat] FOREIGN KEY([delivery_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKkew4r1m266colqb495qwabat]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKlc0v09tyh62v92jjfsk13h0xh] FOREIGN KEY([endorsement_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKlc0v09tyh62v92jjfsk13h0xh]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKleqpq5iuxaowj4gwgnoironsx] FOREIGN KEY([company_own_type_id])
REFERENCES [dbo].[tb_company_own_type] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKleqpq5iuxaowj4gwgnoironsx]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKmghjj5ft4cigt7tabojc4fdbp] FOREIGN KEY([miti_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKmghjj5ft4cigt7tabojc4fdbp]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKn5ugu24o4vvhmi7v8geumcpyv] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKn5ugu24o4vvhmi7v8geumcpyv]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKpvm0bl7t2j0fyycx07hf5wi05] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKpvm0bl7t2j0fyycx07hf5wi05]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKraqe96mts7p5snlvqit4jfl0t] FOREIGN KEY([vehicle_status_id])
REFERENCES [dbo].[tb_vehicle_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKraqe96mts7p5snlvqit4jfl0t]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKrr9d07m6vpepiuk85ecrgsae3] FOREIGN KEY([distributor_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKrr9d07m6vpepiuk85ecrgsae3]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKs52eru8lirx5p7djisqlg17qt] FOREIGN KEY([origin_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKs52eru8lirx5p7djisqlg17qt]
GO
ALTER TABLE [dbo].[tb_vehicle]  WITH CHECK ADD  CONSTRAINT [FKtqw0riglhc056k53ar2wjghkt] FOREIGN KEY([reimbursement_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle] CHECK CONSTRAINT [FKtqw0riglhc056k53ar2wjghkt]
GO
ALTER TABLE [dbo].[tb_vehicle_accessory]  WITH CHECK ADD  CONSTRAINT [FKg2bvj5fho4ni7bysudvoylwyg] FOREIGN KEY([accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_accessory] CHECK CONSTRAINT [FKg2bvj5fho4ni7bysudvoylwyg]
GO
ALTER TABLE [dbo].[tb_vehicle_accessory]  WITH CHECK ADD  CONSTRAINT [FKol4ethy5h46u46ig1b9mm1ni8] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_accessory] CHECK CONSTRAINT [FKol4ethy5h46u46ig1b9mm1ni8]
GO
ALTER TABLE [dbo].[tb_vehicle_detail]  WITH CHECK ADD  CONSTRAINT [FKopktgw8nausv7fihxr3hffu3g] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_detail] CHECK CONSTRAINT [FKopktgw8nausv7fihxr3hffu3g]
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar]  WITH CHECK ADD  CONSTRAINT [FKaxrgdriwkpfmj910l49xpxk30] FOREIGN KEY([sales_order_id])
REFERENCES [dbo].[tb_sales_order] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar] CHECK CONSTRAINT [FKaxrgdriwkpfmj910l49xpxk30]
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar]  WITH CHECK ADD  CONSTRAINT [FKg4rd70mkq50qpbuqnyx53ef2u] FOREIGN KEY([status_id])
REFERENCES [dbo].[tb_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar] CHECK CONSTRAINT [FKg4rd70mkq50qpbuqnyx53ef2u]
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar]  WITH CHECK ADD  CONSTRAINT [FKnj3ehway0rxmdkvxlq94p60ms] FOREIGN KEY([user_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar] CHECK CONSTRAINT [FKnj3ehway0rxmdkvxlq94p60ms]
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar]  WITH CHECK ADD  CONSTRAINT [FKsns6vm8pj46daxbxy5sk4uwtq] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar] CHECK CONSTRAINT [FKsns6vm8pj46daxbxy5sk4uwtq]
GO
ALTER TABLE [dbo].[tb_vehicle_excise]  WITH CHECK ADD  CONSTRAINT [FKkt2u0yb3dioj5q6bir4ims2ph] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_excise] CHECK CONSTRAINT [FKkt2u0yb3dioj5q6bir4ims2ph]
GO
ALTER TABLE [dbo].[tb_vehicle_excise_result]  WITH CHECK ADD  CONSTRAINT [FKadk5n10vo145ig5cgd08de701] FOREIGN KEY([approver_company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_excise_result] CHECK CONSTRAINT [FKadk5n10vo145ig5cgd08de701]
GO
ALTER TABLE [dbo].[tb_vehicle_excise_result]  WITH CHECK ADD  CONSTRAINT [FKhluw48gtw1kqkx1beb3chun6c] FOREIGN KEY([approver_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_excise_result] CHECK CONSTRAINT [FKhluw48gtw1kqkx1beb3chun6c]
GO
ALTER TABLE [dbo].[tb_vehicle_fit_accessory]  WITH CHECK ADD  CONSTRAINT [FKjnpuolipsedt6a7ls1sqnyro2] FOREIGN KEY([accessory_id])
REFERENCES [dbo].[tb_accessory] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_fit_accessory] CHECK CONSTRAINT [FKjnpuolipsedt6a7ls1sqnyro2]
GO
ALTER TABLE [dbo].[tb_vehicle_fit_accessory]  WITH CHECK ADD  CONSTRAINT [FKs1w2hxu4tbgg7jyh7saw93q95] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_fit_accessory] CHECK CONSTRAINT [FKs1w2hxu4tbgg7jyh7saw93q95]
GO
ALTER TABLE [dbo].[tb_vehicle_hold]  WITH CHECK ADD  CONSTRAINT [FK4pf2g7f1ef43kw55eb43nj12i] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold] CHECK CONSTRAINT [FK4pf2g7f1ef43kw55eb43nj12i]
GO
ALTER TABLE [dbo].[tb_vehicle_hold]  WITH CHECK ADD  CONSTRAINT [FKa3rw9sw2kflsx3tgqd5ps67ke] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold] CHECK CONSTRAINT [FKa3rw9sw2kflsx3tgqd5ps67ke]
GO
ALTER TABLE [dbo].[tb_vehicle_hold]  WITH CHECK ADD  CONSTRAINT [FKdc7s1l2si18l0b5jsg77rf656] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold] CHECK CONSTRAINT [FKdc7s1l2si18l0b5jsg77rf656]
GO
ALTER TABLE [dbo].[tb_vehicle_hold]  WITH CHECK ADD  CONSTRAINT [FKoqwbbxidj5iutlp1ae4m0tq28] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold] CHECK CONSTRAINT [FKoqwbbxidj5iutlp1ae4m0tq28]
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result]  WITH CHECK ADD  CONSTRAINT [FK4qj525ebtlw54x5c8dia3e3wi] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result] CHECK CONSTRAINT [FK4qj525ebtlw54x5c8dia3e3wi]
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result]  WITH CHECK ADD  CONSTRAINT [FKak3isvd5b0et5pjh3i600tmf7] FOREIGN KEY([bank_id])
REFERENCES [dbo].[tb_bank] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result] CHECK CONSTRAINT [FKak3isvd5b0et5pjh3i600tmf7]
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result]  WITH CHECK ADD  CONSTRAINT [FKqxofrygo9m3ef6dx1jjdrj79k] FOREIGN KEY([vehicle_hold_id])
REFERENCES [dbo].[tb_vehicle_hold] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result] CHECK CONSTRAINT [FKqxofrygo9m3ef6dx1jjdrj79k]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam]  WITH CHECK ADD  CONSTRAINT [FKeh6pn5t71nj54et0jgi52rg7o] FOREIGN KEY([kastam_reason_id])
REFERENCES [dbo].[tb_kastam_reason] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_kastam] CHECK CONSTRAINT [FKeh6pn5t71nj54et0jgi52rg7o]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam]  WITH CHECK ADD  CONSTRAINT [FKiryv0exqa6uslcd842k4tupll] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_kastam] CHECK CONSTRAINT [FKiryv0exqa6uslcd842k4tupll]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam]  WITH CHECK ADD  CONSTRAINT [FKljko4jh8cicb8txh2w2cohx64] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_kastam] CHECK CONSTRAINT [FKljko4jh8cicb8txh2w2cohx64]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result]  WITH CHECK ADD  CONSTRAINT [FK6yilj0uy6dh26x70u5mtqnhe8] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result] CHECK CONSTRAINT [FK6yilj0uy6dh26x70u5mtqnhe8]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result]  WITH CHECK ADD  CONSTRAINT [FKafbs5pcxos7ddny2bu8pl3c9x] FOREIGN KEY([vehicle_kastam_id])
REFERENCES [dbo].[tb_vehicle_kastam] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result] CHECK CONSTRAINT [FKafbs5pcxos7ddny2bu8pl3c9x]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result]  WITH CHECK ADD  CONSTRAINT [FKgorulbmst6tvwx525j1k2rsa9] FOREIGN KEY([kastam_reason_id])
REFERENCES [dbo].[tb_kastam_reason] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result] CHECK CONSTRAINT [FKgorulbmst6tvwx525j1k2rsa9]
GO
ALTER TABLE [dbo].[tb_vehicle_loose_item]  WITH CHECK ADD  CONSTRAINT [FKjc2qckv4xrnm7a6kdfhtg0j35] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_loose_item] CHECK CONSTRAINT [FKjc2qckv4xrnm7a6kdfhtg0j35]
GO
ALTER TABLE [dbo].[tb_vehicle_loose_item]  WITH CHECK ADD  CONSTRAINT [FKlu6pkfi88ftalsmlt7pm36bgt] FOREIGN KEY([loose_item_id])
REFERENCES [dbo].[tb_loose_item] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_loose_item] CHECK CONSTRAINT [FKlu6pkfi88ftalsmlt7pm36bgt]
GO
ALTER TABLE [dbo].[tb_vehicle_miti]  WITH CHECK ADD  CONSTRAINT [FKj0xw32pj5bw2bhv7aewpgq954] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_miti] CHECK CONSTRAINT [FKj0xw32pj5bw2bhv7aewpgq954]
GO
ALTER TABLE [dbo].[tb_vehicle_miti]  WITH CHECK ADD  CONSTRAINT [FKscar8464ehwc5l0r5ty5e9kld] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_miti] CHECK CONSTRAINT [FKscar8464ehwc5l0r5ty5e9kld]
GO
ALTER TABLE [dbo].[tb_vehicle_miti_result]  WITH CHECK ADD  CONSTRAINT [FKcs3jgcmtwscxsrl678q8fhn9u] FOREIGN KEY([vehicle_miti_id])
REFERENCES [dbo].[tb_vehicle_miti] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_miti_result] CHECK CONSTRAINT [FKcs3jgcmtwscxsrl678q8fhn9u]
GO
ALTER TABLE [dbo].[tb_vehicle_miti_result]  WITH CHECK ADD  CONSTRAINT [FKj6rgol364y1n13lxo7hfiqh78] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_miti_result] CHECK CONSTRAINT [FKj6rgol364y1n13lxo7hfiqh78]
GO
ALTER TABLE [dbo].[tb_vehicle_model]  WITH CHECK ADD  CONSTRAINT [FK8f9t3kg3oq1nxu333olltsvnj] FOREIGN KEY([vehicle_series_id])
REFERENCES [dbo].[tb_vehicle_series] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_model] CHECK CONSTRAINT [FK8f9t3kg3oq1nxu333olltsvnj]
GO
ALTER TABLE [dbo].[tb_vehicle_model]  WITH CHECK ADD  CONSTRAINT [FKi83c2pj0oab633icm0wdv261p] FOREIGN KEY([vehicle_category_id])
REFERENCES [dbo].[tb_vehicle_category] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_model] CHECK CONSTRAINT [FKi83c2pj0oab633icm0wdv261p]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKa1y4gn91fwhw6usgmpwlura0k] FOREIGN KEY([customer_id])
REFERENCES [dbo].[tb_customer] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKa1y4gn91fwhw6usgmpwlura0k]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKatokl4nmlaxwfb6ukbxogu9ql] FOREIGN KEY([lead_id])
REFERENCES [dbo].[tb_crm_lead] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKatokl4nmlaxwfb6ukbxogu9ql]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKavm8r1hvii4a6qwdq25x7tmc7] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKavm8r1hvii4a6qwdq25x7tmc7]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKduoai0xm4v45cvvhokuxc1yf2] FOREIGN KEY([vehicle_brand_id])
REFERENCES [dbo].[tb_vehicle_brand] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKduoai0xm4v45cvvhokuxc1yf2]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKic9lck3nrqeq07884tk5mp4ln] FOREIGN KEY([vehicle_model_id])
REFERENCES [dbo].[tb_vehicle_model] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKic9lck3nrqeq07884tk5mp4ln]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKifk8kemo0s1x4iepocav2cu44] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKifk8kemo0s1x4iepocav2cu44]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKjmu3tsq9sol4485bvj6lbwhr8] FOREIGN KEY([commercial_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKjmu3tsq9sol4485bvj6lbwhr8]
GO
ALTER TABLE [dbo].[tb_vehicle_owned]  WITH CHECK ADD  CONSTRAINT [FKqebmc4gi2wqxnye6rtb2ksnd0] FOREIGN KEY([vehicle_series_id])
REFERENCES [dbo].[tb_vehicle_series] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_owned] CHECK CONSTRAINT [FKqebmc4gi2wqxnye6rtb2ksnd0]
GO
ALTER TABLE [dbo].[tb_vehicle_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FK7qk7sudwhmkmqol4px3bq7pgo] FOREIGN KEY([payment_voucher_id])
REFERENCES [dbo].[tb_payment_voucher] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_payment_voucher] CHECK CONSTRAINT [FK7qk7sudwhmkmqol4px3bq7pgo]
GO
ALTER TABLE [dbo].[tb_vehicle_payment_voucher]  WITH CHECK ADD  CONSTRAINT [FKqh56e9qdwpagikxgkr07p46g9] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_payment_voucher] CHECK CONSTRAINT [FKqh56e9qdwpagikxgkr07p46g9]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi]  WITH CHECK ADD  CONSTRAINT [FK35i3rjnvw6p39sqsbam5o1svi] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi] CHECK CONSTRAINT [FK35i3rjnvw6p39sqsbam5o1svi]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi]  WITH CHECK ADD  CONSTRAINT [FK4ltdswxwqly5wwx2ugfb3oj8i] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi] CHECK CONSTRAINT [FK4ltdswxwqly5wwx2ugfb3oj8i]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi]  WITH CHECK ADD  CONSTRAINT [FKgedeb3mdodltgiqb06o3nylxh] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi] CHECK CONSTRAINT [FKgedeb3mdodltgiqb06o3nylxh]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result]  WITH CHECK ADD  CONSTRAINT [FK7e2kg878orbfu3ckh1mrcr8cy] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] CHECK CONSTRAINT [FK7e2kg878orbfu3ckh1mrcr8cy]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result]  WITH CHECK ADD  CONSTRAINT [FK7sk7fjpsbhhsgwriodrbqb777] FOREIGN KEY([vehicle_pdi_id])
REFERENCES [dbo].[tb_vehicle_pdi] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] CHECK CONSTRAINT [FK7sk7fjpsbhhsgwriodrbqb777]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result]  WITH CHECK ADD  CONSTRAINT [FKe7do91jswan310d13v3vcsk7c] FOREIGN KEY([service_manager_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] CHECK CONSTRAINT [FKe7do91jswan310d13v3vcsk7c]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result]  WITH CHECK ADD  CONSTRAINT [FKisaw815ij9rt296i2wd8xgdji] FOREIGN KEY([inspector_id])
REFERENCES [dbo].[tb_user] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] CHECK CONSTRAINT [FKisaw815ij9rt296i2wd8xgdji]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result]  WITH CHECK ADD  CONSTRAINT [FKmwpvlaxuoe88ote0kck839mee] FOREIGN KEY([inspection_checklist_id])
REFERENCES [dbo].[tb_inspection_checklist] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] CHECK CONSTRAINT [FKmwpvlaxuoe88ote0kck839mee]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result_detail]  WITH CHECK ADD  CONSTRAINT [FK746vudapglyoq93an4wccwi1g] FOREIGN KEY([inspection_checklist_detail_id])
REFERENCES [dbo].[tb_inspection_checklist_detail] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result_detail] CHECK CONSTRAINT [FK746vudapglyoq93an4wccwi1g]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result_detail]  WITH CHECK ADD  CONSTRAINT [FKlohhd4tkkkxv1gqeuahdbcth8] FOREIGN KEY([vehicle_pdi_result_id])
REFERENCES [dbo].[tb_vehicle_pdi_result] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result_detail] CHECK CONSTRAINT [FKlohhd4tkkkxv1gqeuahdbcth8]
GO
ALTER TABLE [dbo].[tb_vehicle_po]  WITH CHECK ADD  CONSTRAINT [FK37si5wvtxrhp9t5a92lfepamo] FOREIGN KEY([company_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po] CHECK CONSTRAINT [FK37si5wvtxrhp9t5a92lfepamo]
GO
ALTER TABLE [dbo].[tb_vehicle_po]  WITH CHECK ADD  CONSTRAINT [FKh5eu5korspk89sm9efxo6ddco] FOREIGN KEY([dest_country_id])
REFERENCES [dbo].[tb_country] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po] CHECK CONSTRAINT [FKh5eu5korspk89sm9efxo6ddco]
GO
ALTER TABLE [dbo].[tb_vehicle_po]  WITH CHECK ADD  CONSTRAINT [FKiho2ayh2njnpyrkfk4r5jues] FOREIGN KEY([workflow_status_id])
REFERENCES [dbo].[tb_workflow_status] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po] CHECK CONSTRAINT [FKiho2ayh2njnpyrkfk4r5jues]
GO
ALTER TABLE [dbo].[tb_vehicle_po]  WITH CHECK ADD  CONSTRAINT [FKod5w4fkrinc6yawqjfgbekvuw] FOREIGN KEY([manufacturer_id])
REFERENCES [dbo].[tb_company] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po] CHECK CONSTRAINT [FKod5w4fkrinc6yawqjfgbekvuw]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FK1jhrfmrqeufqf3fk2khbx9ues] FOREIGN KEY([vehicle_brand_id])
REFERENCES [dbo].[tb_vehicle_brand] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FK1jhrfmrqeufqf3fk2khbx9ues]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FK262eo0vmimbitwq702k1rcqj8] FOREIGN KEY([vehicle_po_id])
REFERENCES [dbo].[tb_vehicle_po] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FK262eo0vmimbitwq702k1rcqj8]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FK3awrubs3usdi7ya9cxsx511hx] FOREIGN KEY([trim_package_id])
REFERENCES [dbo].[tb_trim_package] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FK3awrubs3usdi7ya9cxsx511hx]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FK9eq175lfsn7scgvn8p399pouw] FOREIGN KEY([variant_id])
REFERENCES [dbo].[tb_variant] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FK9eq175lfsn7scgvn8p399pouw]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FKcaky2s49oety76kv8xkfhx1vo] FOREIGN KEY([commercial_colour_id])
REFERENCES [dbo].[tb_commercial_colour] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FKcaky2s49oety76kv8xkfhx1vo]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FKr941hs458p854eaucx71xqbsy] FOREIGN KEY([vehicle_series_id])
REFERENCES [dbo].[tb_vehicle_series] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FKr941hs458p854eaucx71xqbsy]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail]  WITH CHECK ADD  CONSTRAINT [FKtaon8m3pbwa80duskkcif2doj] FOREIGN KEY([vehicle_model_id])
REFERENCES [dbo].[tb_vehicle_model] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] CHECK CONSTRAINT [FKtaon8m3pbwa80duskkcif2doj]
GO
ALTER TABLE [dbo].[tb_vehicle_po_track]  WITH CHECK ADD  CONSTRAINT [FKtip55wbwt40ahrsl0loyrmjjl] FOREIGN KEY([vehicle_po_id])
REFERENCES [dbo].[tb_vehicle_po] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_po_track] CHECK CONSTRAINT [FKtip55wbwt40ahrsl0loyrmjjl]
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order]  WITH CHECK ADD  CONSTRAINT [FKiex5di26itta6tskl0bxpuiko] FOREIGN KEY([vehicle_po_detail_id])
REFERENCES [dbo].[tb_vehicle_po_detail] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order] CHECK CONSTRAINT [FKiex5di26itta6tskl0bxpuiko]
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order_detail]  WITH CHECK ADD  CONSTRAINT [FK5wi7r555ssta9ubo7mklxovly] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order_detail] CHECK CONSTRAINT [FK5wi7r555ssta9ubo7mklxovly]
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order_detail]  WITH CHECK ADD  CONSTRAINT [FKnnqd2s0u1ebe7ho5ycigfiog3] FOREIGN KEY([vehicle_receive_order_id])
REFERENCES [dbo].[tb_vehicle_receive_order] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order_detail] CHECK CONSTRAINT [FKnnqd2s0u1ebe7ho5ycigfiog3]
GO
ALTER TABLE [dbo].[tb_vehicle_registration]  WITH CHECK ADD  CONSTRAINT [FKj4owf9o67ypvb6xdyart0s9tk] FOREIGN KEY([sales_order_id])
REFERENCES [dbo].[tb_sales_order] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_registration] CHECK CONSTRAINT [FKj4owf9o67ypvb6xdyart0s9tk]
GO
ALTER TABLE [dbo].[tb_vehicle_registration]  WITH CHECK ADD  CONSTRAINT [FKmw1ey801dokbfeljl60ko7hli] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[tb_vehicle] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_registration] CHECK CONSTRAINT [FKmw1ey801dokbfeljl60ko7hli]
GO
ALTER TABLE [dbo].[tb_vehicle_series]  WITH CHECK ADD  CONSTRAINT [FK60mi4c69apm0u4dwqulimj6j4] FOREIGN KEY([vehicle_brand_id])
REFERENCES [dbo].[tb_vehicle_brand] ([id])
GO
ALTER TABLE [dbo].[tb_vehicle_series] CHECK CONSTRAINT [FK60mi4c69apm0u4dwqulimj6j4]
GO
