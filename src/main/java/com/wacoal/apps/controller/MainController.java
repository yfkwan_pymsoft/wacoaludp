package com.wacoal.apps.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @RequestMapping("/")
    public String home() {
    	logger.debug("Main Page");
        return "Welcome to UDP - CRM!";
        
    }

}
