package com.wacoal.apps.controller;

//import com.auto.apps.adaptor.Pymsoft.MigrationAdaptor;
import com.wacoal.apps.adaptor.etl.SalesAdaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/etl/wacoal")
public class WacoalEtlController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SalesAdaptor salesAdaptor;

	//@Autowired
	//private MigrationAdaptor migrationAdaptor;
	
	@RequestMapping("/syncdata")
	public String syncAllData() {
		//http://localhost:8080/etl/wacoal/syncdata
    	logger.info("Sync all data...");
    	
    	try {
			salesAdaptor.syncData();
    		//autoAdaptor.syncData();
    		//bigDataAdaptor.syncData();
  	
    		logger.info("Finish sync all data");
    	} catch (Exception e) {
    		logger.error("Failed to sync all data", e);
    		return "Failed";
    	}
   	
    	return "Done";
	    	
	}

	@RequestMapping("/migrateData")
	public String migrateData() {
		//http://localhost:8080/etl/wacoal/migrateData
		logger.info("Migrate all data...");

		try {
			salesAdaptor.migrateData();
			//autoAdaptor.syncData();
			//bigDataAdaptor.syncData();

			logger.info("Finish sync all data");
		} catch (Exception e) {
			logger.error("Failed to sync all data", e);
			return "Failed";
		}

		return "Done";

	}

	@RequestMapping("/fullMigrateData")
	public String fullMigrateData() {
		//http://localhost:8080/etl/wacoal/fullMigrateData
		logger.info("Full migrate all data...");

		try {
			salesAdaptor.fullMigrateData();
			//autoAdaptor.syncData();
			//bigDataAdaptor.syncData();

			logger.info("Finish sync all data");
		} catch (Exception e) {
			logger.error("Failed to sync all data", e);
			return "Failed";
		}

		return "Done";

	}
}
