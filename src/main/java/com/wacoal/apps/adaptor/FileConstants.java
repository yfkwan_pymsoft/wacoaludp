package com.wacoal.apps.adaptor;

public class FileConstants {
    public static final String AUDIT_LOG_DIR 		= "AUDIT_LOG_DIR";
    public static final String ERROR_LOG_DIR        = "ERROR_LOG_DIR";

    public static final String INPUT_FILE_DIR       = "INPUT_FILE_DIR";
}
