package com.wacoal.apps.adaptor;

public class DatabaseConstants {

	public static final String TARGET_DB 			= "TARGET_DB";
	public static final String TARGET_HOST 			= "TARGET_HOST";
	public static final String TARGET_PORT 			= "TARGET_PORT";
	public static final String TARGET_USER 			= "TARGET_USER";
	public static final String TARGET_PASSWORD 		= "TARGET_PASSWORD";
	
	public static final String SOURCE_DB 			= "SOURCE_DB";
	public static final String SOURCE_HOST 			= "SOURCE_HOST";
	public static final String SOURCE_PORT 			= "SOURCE_PORT";
	public static final String SOURCE_USER	 		= "SOURCE_USER";
	public static final String SOURCE_PASSWORD 		= "SOURCE_PASSWORD";

	public static final String SOURCE_DB2 			= "SOURCE_DB2";
	public static final String SOURCE_HOST2 		= "SOURCE_HOST2";
	public static final String SOURCE_PORT2 		= "SOURCE_PORT2";
	public static final String SOURCE_USER2	 		= "SOURCE_USER2";
	public static final String SOURCE_PASSWORD2 	= "SOURCE_PASSWORD2";
}
