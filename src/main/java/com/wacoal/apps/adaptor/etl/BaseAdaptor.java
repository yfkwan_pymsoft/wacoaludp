package com.wacoal.apps.adaptor.etl;

import java.util.HashMap;
import java.util.Map;

import com.wacoal.apps.adaptor.DatabaseConstants;
import com.wacoal.apps.adaptor.FileConstants;
import org.springframework.beans.factory.annotation.Value;

public abstract class BaseAdaptor {
	
	@Value("${target.db.name}")
	private String targetDb;
	
	@Value("${target.db.host}")
	private String targetHost;
	
	@Value("${target.db.port}")
	private String targetPort;
	
	@Value("${target.db.user}")
	private String targetUser;
	
	@Value("${target.db.password}")
	private String targetPassword;
	
	@Value("${source.db.name}")
	private String sourceDb;
	
	@Value("${source.db.host}")
	private String sourceHost;
	
	@Value("${source.db.port}")
	private String sourcePort;
	
	@Value("${source.db.user}")
	private String sourceUser;
	
	@Value("${source.db.password}")
	private String sourcePassword;

	@Value("${source.db.name2}")
	private String sourceDb2;

	@Value("${source.db.host2}")
	private String sourceHost2;

	@Value("${source.db.port2}")
	private String sourcePort2;

	@Value("${source.db.user2}")
	private String sourceUser2;

	@Value("${source.db.password2}")
	private String sourcePassword2;

	@Value("${error_log_directory}")
	private String errorLogDirectory;

	@Value("${audit_log_directory}")
	private String auditLogDirectory;

	@Value("${input_file_directory}")
	private String inputFileDir;

	//@Value("${staging.db.name}")
	//private String stagingDb;

	//@Value("${staging.db.host}")
	//private String stagingHost;

	//@Value("${staging.db.port}")
	//private String stagingPort;

	//@Value("${staging.db.user}")
	//private String stagingUser;

	//@Value("${staging.db.password}")
	//private String stagingPassword;

	
	public abstract void migrateData();
	public abstract void syncData();

	public abstract void fullMigrateData();
	
	protected Map<String, String> getNamedParameters() {
		Map<String, String> namedParameters = new HashMap<String, String>();
		
		if(sourceDb != null && !sourceDb.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_DB, sourceDb);
		}
		
		if(sourceHost != null && !sourceHost.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_HOST, sourceHost);
		}
		
		if(sourcePort != null && !sourcePort.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_PORT, sourcePort);
		}
		
		if(sourceUser != null && !sourceUser.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_USER, sourceUser);
		}
		
		if(sourcePassword != null && !sourcePassword.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_PASSWORD, sourcePassword);
		}
		else {
			namedParameters.put(DatabaseConstants.SOURCE_PASSWORD,"");
		}

		if(sourceDb2 != null && !sourceDb2.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_DB2, sourceDb2);
		}

		if(sourceHost2 != null && !sourceHost2.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_HOST2, sourceHost2);
		}

		if(sourcePort2 != null && !sourcePort2.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_PORT2, sourcePort2);
		}

		if(sourceUser2 != null && !sourceUser2.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_USER2, sourceUser2);
		}

		if(sourcePassword2 != null && !sourcePassword2.isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_PASSWORD2, sourcePassword2);
		}
		else {
			namedParameters.put(DatabaseConstants.SOURCE_PASSWORD2,"");
		}
		
		if(targetDb != null && !targetDb.isEmpty()) {
			namedParameters.put(DatabaseConstants.TARGET_DB, targetDb);
		}
		
		if(targetHost != null && !targetHost.isEmpty()) {
			namedParameters.put(DatabaseConstants.TARGET_HOST, targetHost);
		}
		
		if(targetPort != null && !targetPort.isEmpty()) {
			namedParameters.put(DatabaseConstants.TARGET_PORT, targetPort);
		}
		
		if(targetUser != null && !targetUser.isEmpty()) {
			namedParameters.put(DatabaseConstants.TARGET_USER, targetUser);
		}
		
		if(targetPassword != null && !targetPassword.isEmpty()) {
			namedParameters.put(DatabaseConstants.TARGET_PASSWORD, targetPassword);
		}

		if(errorLogDirectory != null && !errorLogDirectory.isEmpty()) {
			namedParameters.put(FileConstants.ERROR_LOG_DIR, errorLogDirectory);
		}

		if(auditLogDirectory != null && !auditLogDirectory.isEmpty()) {
			namedParameters.put(FileConstants.AUDIT_LOG_DIR, auditLogDirectory);
		}

		if(inputFileDir != null && !inputFileDir.isEmpty()) {
			namedParameters.put(FileConstants.INPUT_FILE_DIR, inputFileDir);
		}

		/*if(stagingDb != null && !stagingDb.isEmpty()) {
			namedParameters.put(DatabaseConstants.STAGING_DB, getStagingDb());
		}

		if(getStagingHost() != null && !getStagingHost().isEmpty()) {
			namedParameters.put(DatabaseConstants.SOURCE_HOST, getStagingHost());
		}

		if(getStagingPort() != null && !getStagingPort().isEmpty()) {
			namedParameters.put(DatabaseConstants.STAGING_PORT, getStagingPort());
		}

		if(getStagingUser() != null && !getStagingUser().isEmpty()) {
			namedParameters.put(DatabaseConstants.STAGING_USER, getStagingUser());
		}

		if(getStagingPassword() != null && !getStagingPassword().isEmpty()) {
			namedParameters.put(DatabaseConstants.STAGING_PASSWORD, getStagingPassword());
		}*/

		return namedParameters;
	}

	public String getTargetHost() {
		return targetHost;
	}

	public void setTargetHost(String targetHost) {
		this.targetHost = targetHost;
	}

	public String getTargetPort() {
		return targetPort;
	}

	public void setTargetPort(String targetPort) {
		this.targetPort = targetPort;
	}

	public String getTargetPassword() {
		return targetPassword;
	}

	public void setTargetPassword(String targetPassword) {
		this.targetPassword = targetPassword;
	}

	public String getTargetDb() {
		return targetDb;
	}

	public void setTargetDb(String targetDb) {
		this.targetDb = targetDb;
	}

	public String getSourceDb() {
		return sourceDb;
	}

	public void setSourceDb(String sourceDb) {
		this.sourceDb = sourceDb;
	}

	public String getSourceHost() {
		return sourceHost;
	}

	public void setSourceHost(String sourceHost) {
		this.sourceHost = sourceHost;
	}

	public String getSourcePort() {
		return sourcePort;
	}

	public void setSourcePort(String sourcePort) {
		this.sourcePort = sourcePort;
	}

	public String getSourcePassword() {
		return sourcePassword;
	}

	public void setSourcePassword(String sourcePassword) {
		this.sourcePassword = sourcePassword;
	}

	public String getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	public String getSourceUser() {
		return sourceUser;
	}

	public void setSourceUser(String sourceUser) {
		this.sourceUser = sourceUser;
	}


	/*public String getStagingDb() {
		return stagingDb;
	}

	public void setStagingDb(String stagingDb) {
		this.stagingDb = stagingDb;
	}

	public String getStagingHost() {
		return stagingHost;
	}

	public void setStagingHost(String stagingHost) {
		this.stagingHost = stagingHost;
	}

	public String getStagingPort() {
		return stagingPort;
	}

	public void setStagingPort(String stagingPort) {
		this.stagingPort = stagingPort;
	}

	public String getStagingUser() {
		return stagingUser;
	}

	public void setStagingUser(String stagingUser) {
		this.stagingUser = stagingUser;
	}

	public String getStagingPassword() {
		return stagingPassword;
	}

	public void setStagingPassword(String stagingPassword) {
		this.stagingPassword = stagingPassword;
	}*/
}
