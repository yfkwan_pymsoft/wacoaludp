package com.wacoal.apps.adaptor.etl;

import com.wacoal.apps.adaptor.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SalesAdaptor extends BaseAdaptor {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static boolean isRunning = false;

    private final String [] fullMappingFiles = {
            "etl/mapping/full/gx_master_branch_etl.ktr", //daily - full
            //"etl/mapping/full/gx_master_branch_jde_etl.ktr", //daily - full
            //"etl/mapping/full/gx_master_branch_exclusionList_etl.ktr", //daily - full
            //"etl/mapping/full/gx_master_sku_exclusionList_etl.ktr",
            /*"etl/mapping/full/gx_master_branch_target_etl.ktr",//daily - full
            "etl/mapping/full/gx_master_ubasePrice_etl.ktr",// daily - full*/

            //sales
            "etl/mapping/full/gx_summary_sales.ktr",
            "etl/mapping/full/gx_sales_analaysis_etl.ktr", // daily - inc (3months) monthly - full pym_analysis_sales
            "etl/mapping/full/gx_summary_sales_target.ktr",
            /*"etl/mapping/full/gx_summary_salesStock.ktr",
            "etl/mapping/full/gx_sales_stock_analaysis_etl_new_1_3.ktr",
            "etl/mapping/full/gx_sales_stock_analaysis_etl_new_7_9.ktr",*/
            "etl/mapping/full/gx_sales_stock_analaysis_sold_etl.ktr", // monthly - full - pym_analysis_sales_stock
            "etl/mapping/full/gx_sales_analaysis_current_month_etl.ktr",// daily - full - pym_analysis_sales_stock
            "etl/mapping/full/gx_sales_analysis_rsp.ktr",

            //jde
            /*"etl/mapping/full/gx_summary_stock.ktr",
            "etl/mapping/full/gx_stock_jde_main_new_1_3.ktr",
            "etl/mapping/full/gx_stock_jde_main_new_7_9.ktr",*/
            "etl/mapping/full/gx_stock_jde_ts_sold.ktr", // monthly - full
            "etl/mapping/full/gx_stock_jde_to_other.ktr", // monthly - full
            "etl/mapping/full/gx_stock_jde_pr_return.ktr", // monthly - full
            "etl/mapping/full/gx_stock_jde_pr_import.ktr",// monthly - full
            "etl/mapping/full/gx_stock_jde_ti_other.ktr",// monthly - full
            "etl/mapping/full/gx_stock_jde_op_om.ktr", //monthly - full

    };

    private final String [] mappingFiles = {
            "etl/mapping/gx_master_branch_etl.ktr", //daily - full
            //"etl/mapping/gx_master_branch_jde_etl.ktr", //daily - full
            //"etl/mapping/gx_master_branch_exclusionList_etl.ktr", //daily - full
            //"etl/mapping/full/gx_master_sku_exclusionList_etl.ktr",
            "etl/mapping/full/gx_master_branch_target_etl.ktr",//daily - full
            "etl/mapping/full/gx_master_ubasePrice_etl.ktr",// daily - full
            //sales
            "etl/mapping/full/gx_summary_sales.ktr",
            "etl/mapping/full/gx_sales_analaysis_etl.ktr", // daily - inc (3months) monthly - full pym_analysis_sales
            "etl/mapping/full/gx_summary_sales_target.ktr",
            "etl/mapping/full/gx_summary_salesStock.ktr",
            "etl/mapping/sh_sales_stock_analaysis_etl_new_1_6.ktr",
            "etl/mapping/sh_sales_stock_analaysis_etl_new_7_12.ktr",

            "etl/mapping/full/gx_sales_stock_analaysis_sold_etl.ktr", // monthly - full - pym_analysis_sales_stock
            "etl/mapping/full/gx_sales_analaysis_current_month_etl.ktr",// daily - full - pym_analysis_sales_stock
            "etl/mapping/full/gx_sales_analysis_rsp.ktr",
            //jde
            "etl/mapping/full/gx_summary_stock.ktr",
            "etl/mapping/sh_stock_jde_main_new_1_6.ktr",
            "etl/mapping/sh_stock_jde_main_new_7_12.ktr",
            "etl/mapping/sh_stock_jde_ts_sold.ktr", // monthly - full
            "etl/mapping/sh_stock_jde_to_other.ktr", // monthly - full
            "etl/mapping/sh_stock_jde_pr_return.ktr", // monthly - full
            "etl/mapping/sh_stock_jde_pr_import.ktr",// monthly - full
            "etl/mapping/sh_stock_jde_ti_other.ktr",// monthly - full
            "etl/mapping/sh_stock_jde_op_om.ktr", //monthly - full

    };

    private final String [] syncMappingFiles = {
            "etl/mapping/gx_master_branch_etl.ktr", //daily - full
            "etl/mapping/full/gx_master_branch_target_etl.ktr",//daily - full
            "etl/mapping/full/gx_master_ubasePrice_etl.ktr",// daily - full

            //sales
            "etl/mapping/full/gx_summary_sales.ktr",
            "etl/mapping/full/gx_sales_analaysis_etl.ktr", // daily - inc (3months) monthly - full pym_analysis_sales
            "etl/mapping/full/gx_summary_sales_target.ktr",

            //"etl/mapping/sh_sales_stock_analaysis_sold_etl.ktr", // daily - full - pym_analysis_sales_stock
            "etl/mapping/full/gx_sales_stock_analaysis_sold_etl.ktr",
            "etl/mapping/full/gx_sales_analaysis_current_month_etl.ktr",// daily - full - pym_analysis_sales_stock
            "etl/mapping/full/gx_summary_stock.ktr",
            //"etl/mapping/gx_sales_analysis_rsp.ktr",

    };

    @Override
    public void fullMigrateData(){
        isRunning = true;

        Map<String, String> namedParameters = getNamedParameters();

        for(String mappingFile: fullMappingFiles) {
            try {
                boolean status = DataUtil.runTransformationFile(mappingFile, namedParameters);
            } catch(Exception e) {
                logger.error("ETL Failed for " + mappingFile, e);
            }
        }

        isRunning = false;
    }

    @Override
    public void migrateData() {
        // run a transformation from the file system

        isRunning = true;

        Map<String, String> namedParameters = getNamedParameters();

        for(String mappingFile: mappingFiles) {
            try {
                boolean status = DataUtil.runTransformationFile(mappingFile, namedParameters);
            } catch(Exception e) {
                logger.error("ETL Failed for " + mappingFile, e);
            }
        }

        isRunning = false;

    }

    @Override
    public void syncData() {
        // run a transformation from the file system
        Map<String, String> namedParameters = getNamedParameters();

        if(!isRunning) {
            for (String mappingFile : syncMappingFiles) {
                try {
                    boolean status = DataUtil.runTransformationFile(mappingFile, namedParameters);
                } catch (Exception e) {
                    logger.error("ETL Failed for " + mappingFile, e);
                }
            }
        }
    }
}
