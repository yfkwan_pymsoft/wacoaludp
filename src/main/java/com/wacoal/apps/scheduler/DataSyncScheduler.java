package com.wacoal.apps.scheduler;

import com.wacoal.apps.adaptor.etl.SalesAdaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class DataSyncScheduler {

	@Value("${isDevelopment}")
	private String isDevelopment;

	@Autowired
	private SalesAdaptor salesAdaptor;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());



	
	@Scheduled(cron="0 0 20 * * SUN-SAT")
	//@Scheduled(cron="0 * * * * SUN-SAT")
	//@Scheduled(cron="0 0 1 * * SAT")
	public void syncAllData() {

		logger.info("Sync latest data...");
    	try {
			logger.info("isDevelopment = ["+isDevelopment+"]");
			if(isDevelopment.toUpperCase().trim().compareTo("TRUE") != 0) {
				logger.info("Starting sync");
				salesAdaptor.syncData();
			}
    		logger.info("Finish Updating all data");
    	} catch (Exception e) {
    		logger.error("Failed to Update all data", e);
    	}
   	
	}
	
	@Scheduled(cron="0 00 20 5 * SUN-SAT")
	public void updateAllData() {
		logger.info("Full data restore...");

    	try {
    		logger.debug("isDevelopment = ["+isDevelopment+"]");
			if(isDevelopment.toUpperCase().trim().compareTo("TRUE") != 0) {
				logger.info("Starting migration");
				salesAdaptor.migrateData();
			}
    	} catch (Exception e) {
    		logger.error("Failed to sync all data", e);
    	}
		
	}
	
	
}
