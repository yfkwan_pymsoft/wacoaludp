-- 1 CKD Buy Off - sp_as_CKDDailyReport
DROP VIEW CKD_BUY_OFF;

CREATE VIEW CKD_BUY_OFF AS
SELECT tv.chassis_no,tv2.name AS modelName,tv.production_date,
 	tv.mcaicbu,tv.mcaickd,--add by Jack for QF0151 ON 2014-7-15
  	isnull(Q.destCountry,'') AS destCountry, 
 	tv.engine_no,tv.key_no,tcc.name AS color,tv.lot_no AS process,tm.name AS model,
 	tv.assembly_date AS completedDt,B.delivery_date as buyOffDate,tve.submission_date,
	tve.successful_Date,tcm.name, isnull(tl.name,'')locationName,O.delivery_date AS dateOut,
	isnull(V.vessel_name,'')AS vesselName, isnull(v.vessel_no,'') AS voyageNo,V.loadingportInDate,
	V.vessDepETDDate,V.vessDepActualDate,V.vessAvalETDDate, V.vessAvalActualDate,
	isnull(F.invoice_no,'') AS fininvNo,F.invoice_date AS salesDt,isnull(F.price,0) AS salesPrice,
 	isnull(F.code,'')AS salesPriceCurrency
 	FROM  tb_vehicle  tv
 	LEFT JOIN tb_variant tv2 ON tv2.id=tv.variant_id
 	LEFT JOIN tb_commercial_colour tcc ON tcc.id=tv.commercial_colour_id
 	LEFT JOIN tb_vehicle_model tm ON tm.id=tv.model_id
 	LEFT JOIN tb_location tl ON tl.id=tv.location_id
 	LEFT JOIN tb_company tcm ON tcm.id=tv.company_id
 	LEFT JOIN tb_vehicle_excise tve ON tve.vehicle_id=tv.id
 	LEFT JOIN (
 		  SELECT  tgdld.vehicle_id,P.loadingportInDate ,tgd.departure_date AS vessDepETDDate
 		  ,D.vessDepActualDate,tgd.arrival_date AS vessAvalETDDate,R.vessAvalActualDate,
 		  tgd.vessel_no,tgd.vessel_name,tgd.update_date
 		  FROM  tb_group_dl tgd 
 		  LEFT JOIN 
 			--Arrived at Local Port
 			(	
 				SELECT tgds.group_dl_id AS PLId,tgds.status_date AS loadingportInDate
 				FROM  tb_group_dl_vessel tgds
 				LEFT JOIN tb_shipping_status tcm ON tcm.id=tgds.shipping_status_id
 				WHERE  tcm.code='LOCALPORT'
 			)P	ON P.PLId = tgd.id
 			LEFT JOIN 
 			--vessel departure date 
 			(
 				SELECT tgds.group_dl_id AS PLId,tgds.status_date AS vessDepActualDate
 				FROM  tb_group_dl_vessel tgds
 				LEFT JOIN tb_shipping_status tcm ON tcm.id=tgds.shipping_status_id
 				WHERE  tcm.code='DIP'
 			)D ON D.PLId=tgd.id
 			LEFT JOIN 
 			--for Received at Destination Port
 			(
 				SELECT tgds.group_dl_id AS PLId,tgds.status_date  AS vessAvalActualDate
 				FROM  tb_group_dl_vessel tgds
 				LEFT JOIN tb_shipping_status tcm ON tcm.id=tgds.shipping_status_id
 				WHERE tcm.code='RADP'
 			)R ON R.PLId=tgd.id
 			--LEFT JOIN tb_groupdlinfo tgi ON tgi.groupDLId=tgd.id
 			LEFT JOIN tb_group_dl_info_detail tgdld ON tgdld.delivery_id=tgd.id
 
 	)V ON v.vehicle_id=tv.id
 	
 	--financial Information
 	LEFT JOIN(
 		SELECT tci.invoice_no,tci.invoice_date,tciid.vehicle_id,tciid.target_price AS price,
 		(case when taccn.code='YEN' THEN 'JPY' ELSE taccn.code end) AS code,tci.update_date
 		FROM tb_com_invoice tci
 		LEFT join tb_com_invoice_info tcii ON tcii.com_invoice_id=tci.id
 		LEFT JOIN tb_com_invoice_info_detail tciid ON tciid.com_invoice_info_id=tcii.id
 		LEFT JOIN tb_config_code taccn ON taccn.id=tciid.config_code_id
 		LEFT JOIN tb_workflow_status tws ON tws.id=tci.workflow_status_id
 		LEFT JOIN tb_company tcOwn ON tcOwn.id = tci.owner_company_id
 		LEFT JOIN tb_company tcToCom on tcToCom.id = tci.invoice_company_id
 		WHERE tws.module_name='Company Invoice' AND tws.[status]='Generated'	
 		AND tcOwn.code='MMSB' AND tcToCom.code='MMC'
 	)F ON F.vehicle_id=tv.id
 	
 	--dest country
 	LEFT JOIN(
 		SELECT tdd.vehicle_id,isnull(tc.name,'')AS destCountry,tc.id AS destCountryId
 		  FROM tb_delivery td
 		LEFT JOIN tb_delivery_detail tdd ON tdd.delivery_id=td.id
 		LEFT JOIN tb_workflow_status tws ON tws.id=td.workflow_status_id
 		LEFT JOIN tb_country tc ON tc.id=td.dest_country_id
 		WHERE tws.module_name='vehicleDelivery' AND 
 		(tws.[status]='Delivery Approved' OR tws.[status]='Delivery In Progress')
 		AND tdd.status = 1	
 	)Q ON Q.vehicle_id=tv.id
 	
 	--buy off day
 	LEFT JOIN (
 		SELECT tdd.vehicle_id,td.delivery_date FROM tb_delivery td
 		LEFT JOIN tb_delivery_detail tdd ON tdd.delivery_id=td.id
 		LEFT JOIN tb_workflow_status tws ON tws.id=td.workflow_status_id
  		LEFT JOIN tb_delivery_type tdt ON tdt.id=td.delivery_type_id
 		WHERE tws.module_name='vehicleDelivery' AND (tws.[status]='Received')
 		AND tdd.status = 1
 		AND td.delivery_type_id =(
			SELECT 
				tdt.id 
			FROM 
				tb_delivery_type tdt 
 		    where 
				tdt.name='Assembler to Manufacturer'
		)	
 	)B ON B.vehicle_id=tv.id
 	
 	--date out
 	LEFT JOIN (
 		SELECT 
			tdd.vehicle_id,td.delivery_date 
		FROM 
			tb_delivery td
 			LEFT JOIN tb_delivery_detail tdd ON tdd.delivery_id=td.id
 			LEFT JOIN tb_workflow_status tws ON tws.id=td.workflow_status_id
 		WHERE 
			tws.module_name='vehicleDelivery' AND (tws.status='Received')
 			AND tdd.status = 1
 			AND td.delivery_type_id=(
				SELECT id FROM tb_delivery_type WHERE name='Manufacturer to Distributor')
 			AND td.to_outlet_id =(
				SELECT tc.id FROM tb_company tc WHERE tc.code='MMC')
 			AND td.to_location_type_id=(
				SELECT tlt.id FROM tb_location_type tlt WHERE tlt.name='Port')
 	)O ON O.vehicle_id = tv.id
 	WHERE tv2.origin_type='CKD' 










CREATE VIEW CKD_BUY_OFF AS
select a.chassis_no, a.production_date, c.name as country, a.mcaicbu, a.mcaickd, a.engine_no, 
		a.key_no, d.name as colour, a.lot_no, f.name as model, a.remarks, e.delivery_date,
		g.name as location
from tb_vehicle a
	left join tb_variant b on a.variant_id = b.id
	left join tb_country c on  a.dest_country_id = c.id
	left join tb_commercial_colour d on a.commercial_colour_id = d.id
	left join tb_delivery_detail e on a.id = e.vehicle_id
	left join tb_vehicle_model f on a.model_id = f.id
	left join tb_location g on a.location_id = g.id;

-- 2 Dealer Free Stock: sp_rpt_ManagementDetailDealerFreeStockReport
DROP VIEW DEALER_FREE_STOCK;
CREATE VIEW DEALER_FREE_STOCK AS
select
	distinct vehicle.chassis_no, company.name as outlet, 
	variant.code as variant, vehicle.engine_no,
	colour.name as colour, 
	vehicle.production_date, vehicle.made_year,
	DATEDIFF(DD,dlrinv.create_date, GETDATE()) as aging
from 
	tb_vehicle vehicle
	left join tb_dealer_invoice dlrinv on dlrinv.vehicle_id = vehicle.id
		and dlrinv.id in (select MAX(a.id) from tb_dealer_invoice a group by a.vehicle_id)
	--left join tb_dealer_invoice_item dlrinvitem on dlrinvitem.dealer_invoice_id = dlrinv.id
	left join tb_variant variant on  variant.id = vehicle.variant_id 
	left join tb_company company on company.id = vehicle.company_id
	left join tb_commercial_colour colour on colour.id = vehicle.commercial_colour_id
	--left join tb_vehicle_model mo on mo.id=variant.vehicle_model_id
	--left join tb_vehicle_series se on se.id=mo.vehicle_series_id
	--left join tb_vehicle_brand br on br.id=mo.vehicle_brand_id
	--left join tb_vehicle_edaftar vehdaf on vehdaf.vehicle_id = vehicle.id 
where
	vehicle.location_type_id = 5
	and ( vehicle.registration_status <> 2007 or vehicle.registration_status is null )
	--and vehicle.company_own <> 1
	--and variant.publish_to_order = 'Yes'
	--and vehdaf.id is null
	and exists (
		select 
			di.vehicle_id 
		from 
			tb_dealer_invoice di
			left join tb_dealer_invoice_item difi on difi.dealer_invoice_id=di.id
		where 
			di.workflow_status_id = 117 and di.vehicle_id= vehicle.id
			and not exists (
				select 
					cn.id 
				from 
					tb_credit_note cn 
				where 
					cn.dealer_invoice_item_id = difi.id 
					and cn.workflow_status_id = 40
					and cn.credit_note_type_id = 1
			) 
	)
	and not exists(
      select 1 from tb_vehicle_edaftar vehdaf where vehdaf.vehicle_id = vehicle.id 
    )
	and exists (
		select 1 from tb_company com where dlrinv.dealer_company_id = com.id 
		and exists (
			select 1 from tb_outlet oot where oot.company_id=com.id
		)
	)

--	order by variant, chassis_no

-- 3 E-Daftar submitted : sp_rpt_vehicle_registration 
DROP VIEW EDAFTAR_SUBMIT
CREATE VIEW EDAFTAR_SUBMIT AS
select
	case when  
			veh.registration_no = '' 
		 then 
			''
		 else 
			veh.registration_no
	end as registration_no,
	
	cbu.application_date as submission_date,
	veh.registration_date as registration_date,
	co.name as company_name,
	va.name + ' (' + va.code + ')' as variant,
	cbu.chassis_no as chassis_no,
	cbu.owner_name1 as owner_name,
	cbu.owner_address1 as owner_address1,
	cbu.owner_address2 as owner_address2,
	reg.finance_company as finance_company,
	ca.postcode as postcode,
	st.name as state_name,
	ic.name as insurance_company,
	veh.id,
	u.username as sales_executive_name,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.ic_no
		else cc.roc_no
	  end
	) as customer_ic,
	cusCate.description,
	(select top 1 cp2.phone_no 
		from tb_customer_phone cp2 
		where cp2.customer_id = cu.id
		and cp2.preferred = 1) as customer_phone,
	vuse.description as usage_type,
	cemail.email as email_addr,
	cbu.owner_address3  as owner_address3,
	city.name  as cityName,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.gender
		else cc.gender
	  end
	) as genderName,
	(case 
		when (cperson.birth_date is not null) 
		then cast(DATEDIFF(MONTH,cast(cperson.birth_date as datetime),GETDATE())/12 as varchar(5))
		else '0' 
	end) age,
	(case 
		when race.id = 5 
		then isnull(cu.other_ethnicity,'')
		else isnull(race.name,'') 
	end )as race_name
	from tb_vehicle veh 
	left outer join tb_vehicle_edaftar cbu on veh.id = cbu.vehicle_id
	left outer join tb_sales si on si.vehicle_id = veh.id 
		and si.id in (select MAX(s1.id) from tb_sales s1 group by s1.vehicle_id)
	left join tb_workflow_status wfs on wfs.id=si.workflow_status_id
	left join tb_variant va on va.id=veh.variant_id
	left join tb_vehicle_model mo on mo.id=va.vehicle_model_id
	left join tb_vehicle_series se on se.id=mo.vehicle_series_id
	left join tb_vehicle_brand br on br.id = se.vehicle_brand_id  																		   
	left join tb_company co on co.id = si.dealer_id
	left join tb_buyer_order bo on bo.id=si.buyer_order_id 
	left join tb_insurance_company ic on ic.id=bo.insurance_company_id
	left join tb_customer cu on cu.id=bo.customer_id
	left join tb_customer_person cperson on cperson.id = cu.id and cu.customer_type = 'Individual'
	left join tb_customer_company cc on cc.id = cu.id and cu.customer_type = 'Company'
	left join tb_ethnicity race on race.id = cperson.ethnicity_id
	left join tb_customer_address ca on ca.customer_id=cu.id and ca.id = (select top 1 ca1.id from tb_customer_address ca1 where ca1.customer_id = cu.id order by ca1.preferred_billing desc)
	left join tb_state st on st.id = ca.state_id 
	left join tb_customer_email  cemail on cemail.customer_id=cu.id and cemail.id = (select top 1 ce1.id from tb_customer_email ce1 where ce1.customer_id = cu.id order by ce1.preferred desc)
	left join tb_city city on city.id=ca.city_id
	left join tb_vehicle_registration reg on reg.vehicle_id= cbu.vehicle_id  
		and reg.id in ( select MAX(id) from tb_vehicle_registration vr group by vr.vehicle_id ) 
	left join tb_user u on u.id = si.sales_executive_id
	left join tb_customer_category cusCate on cusCate.id = cu.customer_category_id
	left join tb_customer_phone cp on cp.customer_id=cu.id and cp.id = (select top 1 cp1.id from tb_customer_phone cp1 where cp1.customer_id = cu.id order by cp1.preferred desc)
	left join tb_vehicle_use vuse on vuse.code= cbu.usage	 
	where cbu.id in ( 
				select MAX(ve1.id) 
				from tb_vehicle_edaftar ve1 
				group by ve1.vehicle_id ) 
		and ((cbu.status_id = 192 and cbu.edaftar_flag = 0) or veh.registration_status=2007) 							
		and veh.status=1
union all
select
	case when  
			veh.registration_no = '' 
		 then 
			''
		 else 
			veh.registration_no
	end as registration_no,
	
	cbu.application_date as submission_date,
	veh.registration_date as registration_date,
	co.name as company_name,
	va.name + ' (' + va.code + ')' as variant,
	cbu.chassis_no as chassis_no,
	cbu.owner_name1 as owner_name,
	cbu.owner_address1 as owner_address1,
	cbu.owner_address2 as owner_address2,
	reg.finance_company as finance_company,
	ca.postcode as postcode,
	st.name as state_name,
	ic.name as insurance_company,
	veh.id,
	u.username as sales_executive_name,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.ic_no
		else cc.roc_no
	  end
	) as customer_ic,
	cusCate.description,
	(select top 1 cp2.phone_no 
		from tb_customer_phone cp2 
		where cp2.customer_id = cu.id
		and cp2.preferred = 1) as customer_phone,
	vuse.description as usage_type,
	cemail.email as email_addr,
	cbu.owner_address3  as owner_address3,
	city.name  as cityName,
	(case 
		when (cu.customer_type = 'Individual')
		then cperson.gender
		else cc.gender
	  end
	) as genderName,
	(case 
		when (cperson.birth_date is not null) 
		then cast(DATEDIFF(MONTH,cast(cperson.birth_date as datetime),GETDATE())/12 as varchar(5))
		else '0' 
	end) age,
	(case 
		when race.id = 5 
		then isnull(cu.other_ethnicity,'')
		else isnull(race.name,'') 
	end )as race_name
	from tb_vehicle veh 
	left outer join tb_vehicle_edaftar cbu on veh.id = cbu.vehicle_id
	left outer join tb_sales si on si.vehicle_id = veh.id 
		and si.id in (select MAX(s1.id) from tb_sales s1 group by s1.vehicle_id)
	left join tb_workflow_status wfs on wfs.id=si.workflow_status_id
	left join tb_variant va on va.id=veh.variant_id
	left join tb_vehicle_model mo on mo.id=va.vehicle_model_id
	left join tb_vehicle_series se on se.id=mo.vehicle_series_id
	left join tb_vehicle_brand br on br.id = se.vehicle_brand_id  																		   
	left join tb_company co on co.id = si.dealer_id
	left join tb_buyer_order bo on bo.id=si.buyer_order_id 
	left join tb_insurance_company ic on ic.id=bo.insurance_company_id
	left join tb_customer cu on cu.id=bo.customer_id
	left join tb_customer_person cperson on cperson.id = cu.id and cu.customer_type = 'Individual'
	left join tb_customer_company cc on cc.id = cu.id and cu.customer_type = 'Company'
	left join tb_ethnicity race on race.id = cperson.ethnicity_id
	left join tb_customer_address ca on ca.customer_id=cu.id and ca.id = (select top 1 ca1.id from tb_customer_address ca1 where ca1.customer_id = cu.id order by ca1.preferred_billing desc)
	left join tb_state st on st.id = ca.state_id 
	left join tb_customer_email  cemail on cemail.customer_id=cu.id and cemail.id = (select top 1 ce1.id from tb_customer_email ce1 where ce1.customer_id = cu.id order by ce1.preferred desc)
	left join tb_city city on city.id=ca.city_id
	left join tb_vehicle_registration reg on reg.vehicle_id= cbu.vehicle_id  
		and reg.id in ( select MAX(id) from tb_vehicle_registration vr group by vr.vehicle_id ) 
	left join tb_user u on u.id = si.sales_executive_id
	left join tb_customer_category cusCate on cusCate.id = cu.customer_category_id
	left join tb_customer_phone cp on cp.customer_id=cu.id and cp.id = (select top 1 cp1.id from tb_customer_phone cp1 where cp1.customer_id = cu.id order by cp1.preferred desc)
	left join tb_vehicle_use vuse on vuse.code= cbu.usage	 
	where veh.registration_status = 2007		
		and veh.status=1
		and not exists (select cbu.id from tb_vehicle_edaftar cbu where cbu.vehicle_id = veh.id)



-- 4 Invoice Sales : sp_rpt_ManagementDetailUnitInvoicedReport
DROP VIEW INVOICE_SALES
CREATE VIEW INVOICE_SALES AS
select 
	distinct variant.name as model, vehicle.chassis_no, vehicle.engine_no, colour.name as colour, 
	vehicle.production_date, dealer.name as "allocated to", item.invoice_no, item.invoice_amount,
	item.invoice_date, 
	--creditnote.credit_note_no, creditnote.credit_note_date,
	previous_invoice_no = COALESCE((
		select 
			top 1 difi2.invoice_no
		from 
			tb_dealer_invoice_item as difi2
			left outer join tb_dealer_invoice as di2 on (difi2.dealer_invoice_id = di2.id)
		where 
			di2.vehicle_id = invoice.vehicle_id
			and difi2.id < item.id 
		order by 
			difi2.id desc 
	),''),
	previous_invoice_date = COALESCE((
		select 
			top 1 CONVERT(VARCHAR(10), difi2.invoice_date, 120) 
		from 
			tb_dealer_invoice_item as difi2
			left outer join tb_dealer_invoice as di2 on (difi2.dealer_invoice_id = di2.id)
		where 
			di2.vehicle_id = invoice.vehicle_id
			and difi2.id < item.id 
		order by 
			difi2.id desc 
	),''), 
	credit_note_no = COALESCE((
		select 
			top 1 cn.credit_note_no
		from 
			tb_credit_note cn
			left join tb_dealer_invoice_item cndi on cndi.id = cn.dealer_invoice_item_id
		where 
			cndi.id = item.id 
			and cn.workflow_status_id = 40
		order by 
			cn.id desc 
	),''),
	credit_note_date = COALESCE((
		select 
			top 1 CONVERT(VARCHAR(10), cn.credit_note_date, 120) 
		from 
			tb_credit_note cn
			left join tb_dealer_invoice_item cndi on cndi.id = cn.dealer_invoice_item_id
		where 
			cndi.id = item.id 
			and cn.workflow_status_id = 40
		order by 
			cn.id desc 
		),''),
		(case 
			when variant.origin_type = 'CKD' 
			-- for CKD type
				then (
					case 
						when vehicle.status = 1 and vehicle.ckd_verified <> 1 
						then 'Total Buy Off'
							
						--when exists(
						--	select id 
						--	from tb_company_type_ref comref
						--	where comref.company_type_id=3 
						--		and comref.company_id = vehicle.company_id)
						--		and vehicle.status = 1 and vehicle.ckd_verified = 1 
								--and not exists(
								--	select endVehicleID from tb_Endorse e where e.endVehicleID=b.id
								--)
						--then 'Not Endorsed, at Assembler'
							
						--when exists(
						--	select 1 
						--	from tb_nik_records ni 
						--	where ni.nikStatus=570004 and ni.nikVehicleId=b.id) and b.statusId=802901 
						--		and exists(
						--			select id 
						--			from tb_comCompanyType 
						--			where companyTypeID=3 and companyID=b.ownComOutID) 
						--		and exists(
						--			select 1 
						--			from tb_Endorse ed 
						--			where ed.endVehicleID=b.id and ed.endStatus = 18)
						--then 'Endorsed & NIK, at Assembler'
							
						when vehicle.status = 1 and vehicle.location_type_id=1 
							and exists(
									select 1 
									from 
										tb_vehicle_pdi pdi
										left join tb_workflow_status pdiws on pdiws.id = pdi.workflow_status_id
									where 
										pdi.vehicle_id = vehicle.id 
										and pdiws.status<>'Approved' 
										and pdiws.status<>'Cancelled' 
										and pdi.workflow_status_id is not null)
						then 'WIP'
							
						--when vehicle.status = 1 and vehicle.location_type_id=1  
						--	and not exists(
						--		select pdi.id 
						--		from tb_vehicle_pdi pdi 
						--		where 
						--			pdi.vehicle_id=vehicle.id 
						--			and pdi.workflow_status_id is not null) 
									--and exists(
									--	select 1 from tb_nik_records ni 
									--	where ni.nikStatus=570004 and ni.nikVehicleId=b.id) 
									--		and exists(
									--			select 1 
									--			from tb_Endorse ed 
									--			where ed.endVehicleID=b.id and ed.endStatus = 18)
						--then 'Endorsed & NIK, at PDI'
							
						when vehicle.accel_fitted=1 and vehicle.location_type_id=1 
							and exists(
								select vpdi.id 
								from tb_vehicle_pdi vpdi  
									left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
								where vpdi_wfs.status = 'Approved' 
								and vpdi.vehicle_id = vehicle.id
							) 
						then 'Ready, at PDI'
							
						when vehicle.accel_fitted=1  and vehicle.location_type_id <> 1 
							and  exists(
								select vpdi.id 
								from tb_vehicle_pdi vpdi  
									left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
								where vpdi_wfs.status = 'Approved' 
								and vpdi.vehicle_id = vehicle.id
							)
							  
						then 'Ready, at Outlet'

						else ''
				end 
			)else(    -- for CBU type   
				case 
				when vehicle.status = 1 and (vehicle.location_type_id<>2 or vehicle.location_type_id is null) 
					and not exists(
						select 1 
						from tb_delivery_detail dd 
						where dd.vehicle_id=vehicle.id
						)
				then 'Shipped'
							
				when vehicle.status = 1 and vehicle.location_type_id=2
				then 'Arrived at Port'
							
				when vehicle.status = 1 and vehicle.location_type_id=7 
					and not exists ( 
						select 1 
						from tb_vehicle_payment_voucher pvv 
							left join tb_payment_voucher pv on pv.id=pvv.payment_voucher_id
							left join tb_workflow_status pvws on pvws.id=pv.workflow_status_id
						where 
							pvv.vehicle_id= vehicle.id 
							--and pv.processid=1 
							and pvws.status='Payment Received' 
							and pvws.status is not null)
				then 'Bonded'
							
				when vehicle.status = 1 and vehicle.location_type_id=1 
					and exists(
						select 1 
						from tb_vehicle_pdi pdi
							left join tb_workflow_status pdiws on pdiws.id=pdi.workflow_status_id
						where 
							pdi.vehicle_id=vehicle.id 
							and pdiws.status<>'Approved' 
							and pdiws.status<>'Cancelled' 
							and pdi.workflow_status_id is not null)
				then 'WIP'
							
				when vehicle.location_type_id=1 and vehicle.status = 1
					and not exists(
						select pdi.id 
						from tb_vehicle_pdi pdi 
						where 
							pdi.vehicle_id=vehicle.id 
							and pdi.workflow_status_id is not null)
				then 'Receive at PDI'
							
				when vehicle.accel_fitted=1 and vehicle.location_type_id = 1 
					and exists(
						select vpdi.id 
						from tb_vehicle_pdi vpdi  
							left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
						where 
							vpdi_wfs.status = 'Approved' 
							and vpdi.vehicle_id = vehicle.id
					) 
							   
				then 'Ready, at PDI'
							
				when vehicle.accel_fitted=1 and vehicle.location_type_id <> 1 
					and  exists(
						select vpdi.id 
						from tb_vehicle_pdi vpdi  
							left join tb_workflow_status vpdi_wfs on vpdi.workflow_status_id=vpdi_wfs.id
						where vpdi_wfs.status = 'Approved' and vpdi.vehicle_id = vehicle.id
					)
							       
				then 'Ready, at Outlet' 
							
				else ''
				end
			)
			end) as pdistatus,
		isnull(vehicle.ckd_verify_remarks,'') as  ckd_verify_remarks
from 
	tb_vehicle vehicle
	--tb_dealer_invoice_item item 
    --left join tb_dealer_invoice invoice on item.dealer_invoice_id=invoice.id
    left join tb_variant variant on variant.id = vehicle.variant_id
    left join tb_dealer_invoice invoice on invoice.vehicle_id = vehicle.id
 	left join tb_dealer_invoice_item item on item.dealer_invoice_id = invoice.id
	left join tb_company dealer on invoice.dealer_company_id=dealer.id
    --left join tb_vehicle vehicle on invoice.vehicle_id=vehicle.id
    left join tb_workflow_status workstatus on vehicle.pdi_status_id=workstatus.id
    left join tb_buyer_order buyer on vehicle.id=buyer.vehicle_id
    left join tb_commercial_colour colour on vehicle.commercial_colour_id=colour.id
	left join tb_credit_note creditnote on creditnote.dealer_invoice_item_id = creditnote.id
where 
	invoice.workflow_status_id = 117



-- 5 Outstanding Booking : sp_rpt_ManagementDetailTotalOutstandingBookingReport
DROP VIEW OUTSTANDING_BOOKING
CREATE VIEW OUTSTANDING_BOOKING AS
select
	distinct DATEDIFF(DD,reqorder.submission_date, GETDATE()) as aging,
	variant.name as variant, colour.name as colour, vehicle.made_year, bo.buyer_order_no, 
	bo.order_date as "buy_order_date", reqorder.request_order_no as "stock_request_no", 
	company.name as outlet, customer.name as "Customer Name", cusphone.phone_no as "customer_phone", 
	cusemail.email as "customer_email", salesperson.username as "sales_exec_name", 
	salesperson.phone as "sales_exec_phone", cusemail.preferred
from 
	tb_buyer_order bo
	join tb_request_detail reqdetail on reqdetail.buyer_order_id = bo.id
	join tb_dealer_request_order reqorder on reqorder.id = reqdetail.dealer_request_order_id
	left join tb_variant variant on variant.id = reqdetail.variant_id
	left join tb_customer customer on customer.id = bo.customer_id
	left join tb_vehicle vehicle on vehicle.id = bo.vehicle_id
	left join tb_commercial_colour colour on colour.id = reqdetail.colour_id
	left join tb_company company on company.id = bo.dealer_company_id
	left join tb_user salesperson on salesperson.id = bo.sales_executive_id
	left join tb_customer_email cusemail on cusemail.customer_id = customer.id and cusemail.preferred = 1
	left join tb_customer_phone cusphone on cusphone.customer_id = customer.id and cusphone.preferred = 1
where 
	--bo.buyer_order_no = 'ADK-BO000616'
	reqorder.workflow_status_id in (163,168)
	and reqorder.disti_company_id = 1
	and reqdetail.loan_request = 0
	--and reqorder.request_date>=CONVERT(varchar(10),DATEADD(dd,-120,GETDATE()),120)
	--and reqorder.request_date<=GETDATE()	
	and exists (
		select bo1.id from tb_buyer_order bo1 
		where
			bo1.id = reqdetail.buyer_order_id  
			and bo1.workflow_status_id not in (89, 91, 127) 
	)
	and not exists (
		select 1 from tb_allocation al
		where 
			al.workflow_status_id = 46 
			and al.vehicle_id = reqdetail.vehicle_id
	)
	and not exists (
		select di.vehicle_id from tb_dealer_invoice di
			left join tb_dealer_invoice_item difi on difi.dealer_invoice_id=di.id
		where 
			di.workflow_status_id = 117 
			and di.vehicle_id = reqdetail.vehicle_id
			and not exists (
				select cn.id from tb_credit_note cn 
				where 
					cn.dealer_invoice_item_id = difi.id 
					and cn.workflow_status_id = 40
					and cn.credit_note_type_id = 1 
			)
	)
	and exists (
		select 1 from tb_company com where reqorder.dealer_company_id = com.id 
			and exists (
			select 1 from tb_outlet oot where oot.company_id=com.id
		)
	)

	order by aging desc, variant
select top 10 * from tb_vehicle_pdi

-- 6 PDI ready, invoiced, not collected : sp_rpt_VehicleReadyNotCollectedReport (sp_rpt_ManagementDetailReportNoCollected)
CREATE VIEW PDI_READY AS
select 
	distinct variant.name variant,co.name as dealer,
	--convert(VARCHAR(10),pdi.lastUpdateDate,120) as PDIApprovedDate,
	--convert(VARCHAR(10),difi.invoice_date,120) as invoiceDate,ve.chassis_no as chassis, 
	--(select datediff(d,difi.invoiceDate,getDate()) ) as agingDays,  
	--pdi_approved_date = (select max(res.inspection_date) 
	--	from tb_vehicle_pdi_result res 
	--	where res.vehicle_pdi_id = pdi.id),
	pdi.approve_date,
	difi.invoice_date, ve.chassis_no as chassis,
	DATEDIFF(DD,difi.invoice_date, GETDATE()) as aging,
	case
	when ve.location_type_id = 3 or ve.location_type_id = 5 
		then (select comLo.name from tb_company comLo where comLo.id = ve.company_location_id)
		else loc.name
		end as location,  
	'edaftarStatus' = case
		when ve.registration_status = 2007 then 'Registered'
		else ''
		end 	
	from tb_vehicle ve
	left join tb_variant variant on variant.id=ve.variant_id
	left join tb_allocation allocation on allocation.vehicle_id=ve.id
	left join tb_dealer_invoice di on di.vehicle_id=ve.id
	--left join tb_workflow_status diws on di.workflow_status_id=diws.id 
	left join tb_company co on co.id = di.dealer_company_id
	left join tb_dealer_invoice_item difi on difi.dealer_invoice_id=di.id
	left join tb_vehicle_pdi pdi on pdi.vehicle_id=ve.id
	--left join tb_vehicle_pdi_result pdiresult on pdiresult.vehicle_pdi_id=pdi.id
	--left join tb_Company comLo on comLo.id = ve.currentLocation 
	--left join tb_location_type loctype on loctype.id = ve.location_type_id 
	left join tb_location loc on loc.id = ve.location_id
	left join tb_commercial_colour g on ve.commercial_colour_id=g.id
	left join tb_vehicle_model mo on mo.id = variant.vehicle_model_id
	left join tb_vehicle_series se on se.id = mo.vehicle_series_id
	left join tb_vehicle_brand br on br.id = se.vehicle_brand_id
	where
	 ve.accel_fitted = 1
	and ve.location_type_id = 1 
	-- Approved 
	and pdi.workflow_status_id = 16
	-- Generated
	and (di.workflow_status_id is null or di.workflow_status_id = 117)
	and invoice_date is not null

	and not exists (
		select cn.id 
		from 
			tb_credit_note cn 
		where 
			cn.dealer_invoice_item_id=difi.id 
			-- Approved
			and cn.workflow_status_id = 40
			and cn.credit_note_type_id=1 
	) 					 


-- 7 Company Vehicle Repport : sp_rpt_ManagementDetailBondedReport
DROP VIEW COMPANY_OWN_VEHICLES 
CREATE VIEW COMPANY_OWN_VEHICLES AS
select distinct 
		com.name as outlet,
		variant.name + ' ( '+variant.code+' )' as variant,
		c.chassis_no as chassis_no, 
		c.engine_no as engine_no, 
		co.name as colour,
		COALESCE(CONVERT(VARCHAR(10), c.upload_date, 120),'') as upload_date,
		c.made_year, st.name as company_type,
		isnull(d.name,'') as custodian, c.owner_name, c.ready_sale,
		bo.buyer_order_no, wstatus.status as bo_status
		from 
			tb_vehicle c
			left join tb_company com on com.id = c.company_id
			left join tb_variant variant on variant.id = c.variant_id
			left join tb_commercial_colour co on co.id = c.commercial_colour_id
			left join tb_vehicle_model mo on mo.id = variant.vehicle_model_id
			left join tb_vehicle_series se on se.id = mo.vehicle_series_id
			left join tb_vehicle_brand br on br.id = se.vehicle_brand_id
			left join tb_company_own_type st on st.id = c.company_own_type_id
			left join tb_department d on d.id = c.department_id
			left join tb_buyer_order bo on bo.vehicle_id = c.id
			left join tb_workflow_status wstatus on wstatus.id = bo.workflow_status_id
		where c.company_own = 1
		and variant.publish_to_order = 'yes'


-- DEBUGGING

select top 10 * from tb_vehicle
select * from tb_company_type


select * from tb_vehicle_registration
select * from tb_buyer_order
select * from tb_workflow_status where id in (15, 16, 11, 25, 23)
select * from tb_workflow_status where module_name like '%Invoice%'
select count(*), workflow_status_id from tb_vehicle_pdi group by workflow_status_id
select * from tb_delivery
select * from tb_delivery_detail
select * from tb_dealer_request_order
select * from tb_request_detail


select * from tb_workflow_status where module_name = 'bookingOrder'

select top 10 * from tb_customer_phone where preferred = 0

select * from tb_variant

select * from tb_outlet where outlet_type_id is null

select com.id, com.name, outlettype.name from tb_company com
	left join tb_outlet outlet on outlet.company_id = com.id
	left join tb_outlet_type outlettype on outlettype.id = outlet.outlet_type_id

select * 
from 
	tb_buyer_order bo
	left join tb_request_detail reqdetail on reqdetail.buyer_order_id = bo.id
where buyer_order_no  = 'C3S-BO003502'

select count(*), customer_id from tb_customer_email group by customer_id

select * from tb_customer_email where customer_id = 17681

update tb_customer_email set sync_delete = 1

select * from tb_customer_email where sync_delete = 0

delete tb_customer_email where sync_delete = 0

--select count(*) from tb_vehicle veh
--	left outer join tb_vehicle_edaftar cbu on veh.id = cbu.vehicle_id
--	left outer join tb_sales si on si.vehicle_id = veh.id 
--	left join tb_buyer_order bo on bo.id=si.buyer_order_id 
--	left join tb_customer cu on cu.id=bo.customer_id
--	left join tb_customer_person cp on cp.id = cu.id and cu.customer_type = 'Individual'
--	left join tb_customer_company cc on cc.id = cu.id and cu.customer_type = 'Company'
--where cbu.vehicle_type = 'CBU'

--select top 10 * from tb_vehicle_edaftar cbu where cbu.vehicle_type = 'CBU'
--select * from tb_status where ref_id = 802901

-- Customer Type - Person

--select 
--	top 10 *
--from 
--	tb_vehicle veh 
--	left outer join tb_vehicle_edaftar cbu on veh.id = cbu.vehicle_id
--	left outer join tb_sales si on si.vehicle_id = veh.id 
--		and si.id in (select MAX(s1.id) from tb_sales s1 group by s1.vehicle_id)
 
 select top 10 * from tb_vehicle_edaftar
select location_id from tb_vehicle a where a.location_type_id != 5
and location_id is not null

select * from tb_delivery

select * from tb_location_type
select * from tb_location

select * from tb_customer_person
select * from tb_customer_company

-- Query for debugging
select 
	--distinct variant.name as model, vehicle.chassis_no, vehicle.engine_no, colour.name as colour, 
	--vehicle.production_date, dealer.name as "allocated to", item.invoice_no, item.invoice_amount,
	--item.invoice_date, creditnote.credit_note_no, creditnote.credit_note_date,

	--workstatus.status as "pdi status"
	*
from 
	tb_vehicle vehicle
	--tb_dealer_invoice_item item 
    --left join tb_dealer_invoice invoice on item.dealer_invoice_id=invoice.id
    left join tb_dealer_invoice invoice on invoice.vehicle_id = vehicle.id
 	left join tb_dealer_invoice_item item on item.dealer_invoice_id = invoice.id
	left join tb_company dealer on invoice.dealer_company_id=dealer.id
    --left join tb_vehicle vehicle on invoice.vehicle_id=vehicle.id
    left join tb_workflow_status workstatus on vehicle.pdi_status_id=workstatus.id
    left join tb_buyer_order buyer on vehicle.id=buyer.vehicle_id
    left join tb_variant variant on vehicle.variant_id=variant.id
    left join tb_commercial_colour colour on vehicle.commercial_colour_id=colour.id
	left join tb_credit_note creditnote on creditnote.dealer_invoice_item_id = creditnote.id
where 
	invoice.workflow_status_id = 117
	and vehicle.engine_no = 'PE30545879'

select a.id, a.accel_fitted, a.location_type_id, a.variant_id, status, ckd_verified 
	from tb_vehicle a where a.engine_no = 'PE30587880' and accel_fitted = 1
select origin_type from tb_variant a where origin_type='CKD'
select * from tb_vehicle_pdi where vehicle_id = 20971

select a.accel_fitted, a.status from tb_vehicle a where a.accel_fitted is null or a.status is null

select count(*), workflow_status_id  from tb_credit_note group by workflow_status_id
select * from tb_workflow_status where id in (16);


select * from tb_workflow_status where id in (59, 60, 61,62,63,64,65); 
	
select count(*), workflow_status_id from  tb_delivery_detail group by workflow_status_id

select top 10 * from tb_delivery
select top 10 * from tb_delivery_detail
select top 10 * from tb_delivery_type


select * from tb_location_type
select top 10 * from tb_credit_note
select top 10 * from tb_dealer_invoice
select * from tb_credit_note_type

select location_type_id, location_id, company_location_id from tb_vehicle where location_type_id = 3

--- Old
select 
	distinct company.name as company, variant.name as variant, vehicle.chassis_no, vehicle.engine_no, 
	pdiresult.inspection_date as "pdi_ready_date", dlrinvoiceitem.invoice_date,
	DATEDIFF(DD,pdiresult.inspection_date, GETDATE()) as aging, location.name as location
from 
	tb_vehicle vehicle 
	join tb_vehicle_pdi pdi on pdi.vehicle_id = vehicle.id
	join tb_vehicle_pdi_result pdiresult on pdiresult.vehicle_pdi_id = pdi.id
	left join tb_dealer_invoice dlrinvoice on dlrinvoice.vehicle_id = vehicle.id
	left join tb_dealer_invoice_item dlrinvoiceitem on dlrinvoiceitem.dealer_invoice_id = dlrinvoice.id
	left join tb_delivery_detail deliverydetail on deliverydetail.vehicle_id = vehicle.id
	left join tb_location location on vehicle.location_id = location.id
	left join tb_variant variant on vehicle.variant_id = variant.id
	left join tb_company company on vehicle.company_id = company.id
	--left join tb_workflow_status pdistatus on vehicle.pdi_status_id = pdistatus.id
	--left join tb_workflow_status deliverystatus on deliverydetail.workflow_status_id = deliverystatus.id
where
	vehicle.pdi_status_id = 16
	and dlrinvoice.workflow_status_id = 117
	and deliverydetail.workflow_status_id = 62
	and pdiresult.inspection_date is not null
	and location.name is not null
	and vehicle.location_id = 1

select * from tb_vehicle where location_id = 1


-- Option 2
select 
	company.name as company, variant.name as variant, vehicle.chassis_no, vehicle.engine_no, 
	location.name as location, invoice.invoice_date
from 
	tb_vehicle vehicle 
	join tb_sales sales on sales.vehicle_id = vehicle.id
	join tb_invoice invoice on sales.invoice_id = invoice.id
	join tb_delivery_detail deliverydetail on deliverydetail.vehicle_id = vehicle.id
	left join tb_location location on vehicle.location_id = location.id
	left join tb_variant variant on vehicle.variant_id = variant.id
	left join tb_company company on vehicle.company_id = company.id
	--left join tb_workflow_status pdistatus on vehicle.pdi_status_id = pdistatus.id
	--left join tb_workflow_status deliverystatus on deliverydetail.workflow_status_id = deliverystatus.id
where 
	invoice.workflow_status_id = 101
	and vehicle.pdi_status_id = 16
	and deliverydetail.workflow_status_id = 62

	and l.status = 'Delivery Approved'

-- Company Vehicle
select distinct 
		com.coName as [Outlet Name],
		var.variantname+' ( '+var.variantCode+' )' as [Variant Name],
		c.chassisNo as [Chassis No], 
		c.engineNo as [Engine No], 
		co.commercialColourName as [Colour],
		COALESCE(CONVERT(VARCHAR(10), c.uploadDate, 120),'') as [uploadDate],
		c.yearMade,st.typeName,
		isnull(d.departmentName,'') as custodian,c.ownerName
		from 
			tb_vehicle c
			left outer join tb_company com on com.coID=c.ownComOutID
			left outer join tb_variant var on var.id=c.variantID
			left outer join tb_commercialColour co on co.id=c.commercialColorID
			left outer join tb_Model mo on mo.modelID=var.modelID
			left outer join tb_series se on se.id=mo.modelSeriesID
			left outer join tb_brand br on br.id=se.brandID
			left outer join tb_systemType st on st.id=c.systemTypeID
			left join tb_department d on d.id=c.departmentID --add by daniel for sup005
		where c.isCompanyOwn = 1

