-- Master Code
create index idx_status_ref_id on tb_status(ref_id);
create index idx_vehicle_status_ref_id on tb_vehicle_status(ref_id);
create index idx_workflow_status_ref_id on tb_workflow_status(ref_id);
create index idx_usage_type_ref_id on tb_usage_type(ref_id);
create index idx_fuel_type_ref_id on tb_fuel_type(ref_id);
create index idx_gst_tax_ref_id on tb_gst_tax(ref_id);
create index idx_cheque_type_ref_id on tb_cheque_type(ref_id);
create index idx_payment_mode_ref_id on tb_payment_mode(ref_id);
create index idx_vehvar_type_ref_id on tb_vehvar_type(ref_id);
create index idx_city_ref_id on tb_city(ref_id);
create index idx_state_ref_id on tb_state(ref_id);
create index idx_region_ref_id on tb_region(ref_id);
create index idx_country_ref_id on tb_country(ref_id);
create index idx_language_ref_id on tb_language(ref_id);
create index idx_salutation_ref_id on tb_salutation(ref_id);
create index idx_ethnicity_ref_id on tb_ethnicity(ref_id);
create index idx_marriage_status_ref_id on tb_marriage_status(ref_id);
create index idx_manufacturer_colour_ref_id on tb_manufacturer_colour(ref_id);
create index idx_government_colour_ref_id on tb_government_colour(ref_id);
create index idx_commercial_colour_ref_id on tb_commercial_colour(ref_id);
create index idx_customer_category_ref_id on tb_customer_category(ref_id);
create index idx_variant_ref_id on tb_variant(ref_id);
create index idx_variant_type_ref_id on tb_variant_type(ref_id);
create index idx_location_type_ref_id on tb_location_type(ref_id);
create index idx_location_ref_id on tb_location(ref_id);
create index idx_vehicle_body_type_ref_id on tb_vehicle_body_type(ref_id);
create index idx_vehicle_body_style_ref_id on tb_vehicle_body_style(ref_id);
create index idx_vehicle_category_ref_id on tb_vehicle_category(ref_id);
create index idx_vehicle_type_ref_id on tb_vehicle_type(ref_id);
create index idx_trim_package_ref_id on tb_trim_package(ref_id);
create index idx_loose_item_ref_id on tb_loose_item(ref_id);
create index idx_accessory_ref_id on tb_accessory(ref_id);
create index idx_config_field_ref_id on tb_config_field(ref_id);
create index idx_bank_ref_id on tb_bank(ref_id);
create index idx_cost_type_ref_id on tb_cost_type(ref_id);
create index idx_cost_ref_id on tb_cost(ref_id);
create index idx_user_ref_id on tb_user(ref_id);
create index idx_currency_ref_id on tb_currency(ref_id);
create index idx_warranty_plan_ref_id on tb_warranty_plan(ref_id);
create index idx_vehicle_use_ref_id on tb_vehicle_use(code);
create index idx_kastam_reason_ref_id on tb_vehicle_use(code);
create index idx_loan_status_ref_id on tb_loan_status(ref_id);

-- Company
create index idx_company_type_ref_id on tb_company_type(ref_id);
create index idx_company_type_ref_ref_id on tb_company_type_ref(ref_id);
create index idx_company_ref_id on tb_company(ref_id);
create index idx_company_address_ref_id on tb_company_address(ref_id);
create index idx_department_ref_id on tb_department(ref_id);
create index idx_insurance_company_ref_id on tb_insurance_company(ref_id);
create index idx_outlet_type_ref_id on tb_outlet_type(ref_id);
create index idx_outlet_ref_id on tb_outlet(ref_id);

-- Customer
create index idx_customer_ref_id on tb_customer(ref_id);
create index idx_customer_company_ref_id on tb_customer_company(ref_id);
create index idx_customer_person_ref_id on tb_customer_person(ref_id);
create index idx_customer_address_ref_id on tb_customer_address(ref_id);
create index idx_customer_phone_ref_id on tb_customer_phone(ref_id);
create index idx_customer_email_ref_id on tb_customer_email(ref_id);
create index idx_send_email_ref_id on tb_send_email(ref_id);
create index idx_crm_reward_ref_id on tb_crm_reward(ref_id);
create index idx_crm_config_ref_id on tb_crm_config(ref_id);
create index idx_crm_config_code on tb_crm_config(code);
create index idx_crm_status_ref_id on tb_crm_status(ref_id);
create index idx_crm_status_code on tb_crm_status(code);
create index idx_crm_campaign_ref_id on tb_crm_campaign(ref_id);
create index idx_crm_lead_ref_id on tb_crm_lead(ref_id);
create index idx_crm_activity_ref_id on tb_crm_activity(ref_id);
create index idx_crm_product_interest_ref_id on tb_crm_product_interest(ref_id);

-- Vehicle
create index idx_inspection_type_ref_id on tb_inspection_type(ref_id);
create index idx_inspection_level_ref_id on tb_inspection_level(ref_id);
create index idx_inspection_checklist_ref_id on tb_inspection_checklist(ref_id);
create index idx_inspection_ref_id on tb_inspection(ref_id);
create index idx_inspection_checklist_detail_ref_id on tb_inspection_checklist_detail(ref_id);
create index idx_vehicle_accessory_ref_id on tb_vehicle_accessory(ref_id);
create index idx_vehicle_fit_accessory_ref_id on tb_vehicle_fit_accessory(ref_id);
create index idx_vehicle_loose_item_ref_id on tb_vehicle_loose_item(ref_id);
create index idx_vehicle_brand_ref_id on tb_vehicle_brand(ref_id);
create index idx_vehicle_series_ref_id on tb_vehicle_series(ref_id);
create index idx_vehicle_model_ref_id on tb_vehicle_model(ref_id);
create index idx_vehicle_ref_id on tb_vehicle(ref_id);
create index idx_vehicle_detail_ref_id on tb_vehicle_detail(ref_id);
create index idx_variant_loose_item_ref_id on tb_variant_loose_item(ref_id);
create index idx_variant_trim_package_ref_id on tb_variant_trim_package(ref_id);
create index idx_variant_accessory_ref_id on tb_variant_accessory(ref_id, accessory_type);
create index idx_vehicle_pdi_ref_id on tb_vehicle_pdi(ref_id);
create index idx_vehicle_pdi_result_ref_id on tb_vehicle_pdi_result(ref_id);
create index idx_vehicle_pdi_result_detail_ref_id on tb_vehicle_pdi_result_detail(ref_id);
create index idx_vehicle_hold_ref_id on tb_vehicle_hold(ref_id);
create index idx_vehicle_hold_result_ref_id on tb_vehicle_hold_result(ref_id);
create index idx_vehicle_kastam_ref_id on tb_vehicle_kastam(ref_id);
create index idx_vehicle_miti_ref_id on tb_vehicle_miti(ref_id);
create index idx_vehicle_excise_ref_id on tb_vehicle_excise(ref_id);
create index idx_vehicle_kastam_result_ref_id on tb_vehicle_kastam_result(ref_id);
create index idx_vehicle_miti_result_ref_id on tb_vehicle_miti_result(ref_id);
create index idx_vehicle_excise_result_ref_id on tb_vehicle_excise_result(ref_id);
create index idx_vehicle_owned_ref_id on tb_vehicle_owned(ref_id);

-- Sales
create index idx_allocation_ref_id on tb_allocation(ref_id);
create index idx_allocation_result_ref_id on tb_allocation_result(ref_id);
create index idx_trade_in_brand_ref_id on tb_trade_in_brand(ref_id);
create index idx_service_plan_ref_id on tb_service_plan(ref_id);
create index idx_delivery_type_ref_id on tb_delivery_type(ref_id);
create index idx_credit_note_type_ref_id on tb_credit_note_type(ref_id);
create index idx_debit_note_type_ref_id on tb_debit_note_type(ref_id);
create index idx_cust_credit_note_type_ref_id on tb_cust_credit_note_type(ref_id);
create index idx_cust_debit_note_type_ref_id on tb_cust_debit_note_type(ref_id);
create index idx_quotation_ref_id on tb_quotation(ref_id);
create index idx_quotation_price_detail_ref_id on tb_quotation_price_detail(ref_id);
create index idx_quotation_accessory_ref_id on tb_quotation_accessory(ref_id);
create index idx_delivery_ref_id on tb_delivery(ref_id);
create index idx_delivery_detail_ref_id on tb_delivery_detail(ref_id);
create index idx_delivery_result_ref_id on tb_delivery_result(ref_id);
create index idx_dealer_request_order_ref_id on tb_dealer_request_order(ref_id);
create index idx_price_plan_ref_id on tb_price_plan(ref_id);
create index idx_dealer_invoice_ref_id on tb_dealer_invoice(ref_id);
create index idx_dealer_invoice_item_ref_id on tb_dealer_invoice_item(ref_id);
create index idx_dealer_invoice_selected_item_ref_id on tb_dealer_invoice_selected_item(ref_id);
create index idx_trade_in_ref_id on tb_trade_in(ref_id);
create index idx_buyer_order_ref_id on tb_buyer_order(ref_id);
create index idx_request_detail_ref_id on tb_request_detail(ref_id);
create index idx_sales_order_ref_id on tb_sales_order(ref_id);
create index idx_sales_ref_id on tb_sales(ref_id);
create index idx_sales_order_accessory_ref_id on tb_sales_order_accessory(ref_id);
create index idx_sales_order_price_detail_ref_id on tb_sales_order_price_detail(ref_id);
create index idx_register_out_pay_ref_id on tb_register_out_pay(ref_id);
create index idx_register_out_pay_detail_ref_id on tb_register_out_pay_detail(ref_id);
create index idx_excise_ref_id on tb_excise(ref_id);
create index idx_payment_account_ref_id on tb_payment_account(ref_id);
create index idx_vehicle_payment_term_ref_id on tb_vehicle_payment_term(ref_id);
create index idx_payment_ref_id on tb_payment(ref_id);
create index idx_payment_detail_ref_id on tb_payment_detail(ref_id);
create index idx_payment_request_ref_id on tb_payment_request(ref_id);
create index idx_promotion_type_ref_id on tb_promotion_type(ref_id);
create index idx_promotion_ref_id on tb_promotion(ref_id);
create index idx_credit_note_ref_id on tb_credit_note(ref_id);
create index idx_cust_debit_note_ref_id on tb_cust_debit_note(ref_id);
create index idx_cust_credit_note_ref_id on tb_cust_credit_note(ref_id);
create index idx_debit_note_ref_id on tb_debit_note(ref_id);
create index idx_payment_voucher_ref_id on tb_payment_voucher(ref_id);
create index idx_payment_voucher_result_ref_id on tb_payment_voucher_result(ref_id);
create index idx_vehicle_po_ref_id on tb_vehicle_po(ref_id);
create index idx_vehicle_po_detail_ref_id on tb_vehicle_po_detail(ref_id);
create index idx_vehicle_receive_order_ref_id on tb_vehicle_receive_order(ref_id);
create index idx_vehicle_receive_order_detail_ref_id on tb_vehicle_receive_order_detail(ref_id);
create index idx_vehicle_payment_voucher_ref_id on tb_vehicle_payment_voucher(ref_id);
create index idx_vehicle_registration_ref_id on tb_vehicle_registration(ref_id);
create index idx_vehicle_edaftar_ref_id on tb_vehicle_edaftar(ref_id);
create index idx_vehicle_edaftar_vehicle_type_ref_id on tb_vehicle_edaftar(ref_id, vehicle_type);
create index idx_vehicle_edaftar_vehicle_id on tb_vehicle_edaftar(vehicle_id);

create index idx_group_dl_ref_id on tb_group_dl(ref_id);
create index idx_group_dl_info_ref_id on tb_group_dl_info(ref_id);
create index idx_group_dl_info_detail_ref_id on tb_group_dl_info_detail(ref_id);
create index idx_com_invoice_ref_id on tb_com_invoice(ref_id);
create index idx_com_invoice_info_ref_id on tb_com_invoice_info(ref_id);
create index idx_com_invoice_info_detail_ref_id on tb_com_invoice_info_detail(ref_id);

-- Services

-- Optimisation for Tableau
create index idx_dealer_invoice_vehicle_id on tb_dealer_invoice(vehicle_id);
create index idx_buyer_order_vehicle_id on tb_buyer_order(vehicle_id);
create index idx_dealer_invoice_item_dlr_invoice_id on tb_dealer_invoice_item(dealer_invoice_id);
create index idx_vehicle_pdi_vehicle_id on tb_vehicle_pdi(vehicle_id);
create index idx_delivery_detail_vehicle_id on tb_delivery_detail(vehicle_id);
create index idx_vehicle_edaftar_vehicle_id on tb_vehicle_edaftar(vehicle_id);
create index idx_customer_person_cust_id on tb_customer_person(customer_id);
create index idx_customer_company_cust_id on tb_customer_company(customer_id);
create index idx_vehicle_registration_vehicle_id on tb_vehicle_registration(vehicle_id);
create index idx_sales_vehicle_id on tb_sales(vehicle_id);
create index idx_customer_address_cust_id on tb_customer_address(customer_id);
create index idx_customer_email_cust_id on tb_customer_email(customer_id);
create index idx_customer_phone_cust_id on tb_customer_phone(customer_id);

select top 10 a.origin_type from tb_variant a
select top 10 a.ckd_verified, a.st from tb_vehicle a
select top 10 * from tb_vehicle a where a.status = 1
select * from tb_company_type_ref
select * from tb_company_type
select a.company_id from tb_vehicle a

select count(*), workflow_status_id  from tb_vehicle_pdi group by workflow_status_id
select * from tb_workflow_status where module_name = 'PDI';
select * from tb_workflow_status where id in (15, 16, 11,25);

select * from tb_location_type

select * from tb_vehicle_payment_voucher

select * from tb_payment_voucher

select count(*), a.accel_fitted from tb_vehicle a group by a.accel_fitted

