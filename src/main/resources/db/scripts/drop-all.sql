USE [udp_dev]
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order_detail] DROP CONSTRAINT [FKnnqd2s0u1ebe7ho5ycigfiog3]
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order_detail] DROP CONSTRAINT [FK5wi7r555ssta9ubo7mklxovly]
GO
ALTER TABLE [dbo].[tb_vehicle_receive_order] DROP CONSTRAINT [FKiex5di26itta6tskl0bxpuiko]
GO
ALTER TABLE [dbo].[tb_vehicle_po_track] DROP CONSTRAINT [FKtip55wbwt40ahrsl0loyrmjjl]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FKtaon8m3pbwa80duskkcif2doj]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FKr941hs458p854eaucx71xqbsy]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FKcaky2s49oety76kv8xkfhx1vo]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FK9eq175lfsn7scgvn8p399pouw]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FK3awrubs3usdi7ya9cxsx511hx]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FK262eo0vmimbitwq702k1rcqj8]
GO
ALTER TABLE [dbo].[tb_vehicle_po_detail] DROP CONSTRAINT [FK1jhrfmrqeufqf3fk2khbx9ues]
GO
ALTER TABLE [dbo].[tb_vehicle_po] DROP CONSTRAINT [FKod5w4fkrinc6yawqjfgbekvuw]
GO
ALTER TABLE [dbo].[tb_vehicle_po] DROP CONSTRAINT [FKiho2ayh2njnpyrkfk4r5jues]
GO
ALTER TABLE [dbo].[tb_vehicle_po] DROP CONSTRAINT [FKh5eu5korspk89sm9efxo6ddco]
GO
ALTER TABLE [dbo].[tb_vehicle_po] DROP CONSTRAINT [FK37si5wvtxrhp9t5a92lfepamo]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result_detail] DROP CONSTRAINT [FKlohhd4tkkkxv1gqeuahdbcth8]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result_detail] DROP CONSTRAINT [FK746vudapglyoq93an4wccwi1g]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] DROP CONSTRAINT [FKmwpvlaxuoe88ote0kck839mee]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] DROP CONSTRAINT [FKisaw815ij9rt296i2wd8xgdji]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] DROP CONSTRAINT [FKe7do91jswan310d13v3vcsk7c]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] DROP CONSTRAINT [FK7sk7fjpsbhhsgwriodrbqb777]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi_result] DROP CONSTRAINT [FK7e2kg878orbfu3ckh1mrcr8cy]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi] DROP CONSTRAINT [FKgedeb3mdodltgiqb06o3nylxh]
GO
ALTER TABLE [dbo].[tb_vehicle_pdi] DROP CONSTRAINT [FK35i3rjnvw6p39sqsbam5o1svi]
GO
ALTER TABLE [dbo].[tb_vehicle_model] DROP CONSTRAINT [FKlyvo5u7y2qo75u5dkkwh7qd2y]
GO
ALTER TABLE [dbo].[tb_vehicle_model] DROP CONSTRAINT [FKi83c2pj0oab633icm0wdv261p]
GO
ALTER TABLE [dbo].[tb_vehicle_model] DROP CONSTRAINT [FK8f9t3kg3oq1nxu333olltsvnj]
GO
ALTER TABLE [dbo].[tb_vehicle_miti_result] DROP CONSTRAINT [FKj6rgol364y1n13lxo7hfiqh78]
GO
ALTER TABLE [dbo].[tb_vehicle_miti_result] DROP CONSTRAINT [FKcs3jgcmtwscxsrl678q8fhn9u]
GO
ALTER TABLE [dbo].[tb_vehicle_miti] DROP CONSTRAINT [FKscar8464ehwc5l0r5ty5e9kld]
GO
ALTER TABLE [dbo].[tb_vehicle_miti] DROP CONSTRAINT [FKj0xw32pj5bw2bhv7aewpgq954]
GO
ALTER TABLE [dbo].[tb_vehicle_loose_item] DROP CONSTRAINT [FKlu6pkfi88ftalsmlt7pm36bgt]
GO
ALTER TABLE [dbo].[tb_vehicle_loose_item] DROP CONSTRAINT [FKjc2qckv4xrnm7a6kdfhtg0j35]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result] DROP CONSTRAINT [FKgorulbmst6tvwx525j1k2rsa9]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result] DROP CONSTRAINT [FKafbs5pcxos7ddny2bu8pl3c9x]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam_result] DROP CONSTRAINT [FK6yilj0uy6dh26x70u5mtqnhe8]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam] DROP CONSTRAINT [FKljko4jh8cicb8txh2w2cohx64]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam] DROP CONSTRAINT [FKiryv0exqa6uslcd842k4tupll]
GO
ALTER TABLE [dbo].[tb_vehicle_kastam] DROP CONSTRAINT [FKeh6pn5t71nj54et0jgi52rg7o]
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result] DROP CONSTRAINT [FKqxofrygo9m3ef6dx1jjdrj79k]
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result] DROP CONSTRAINT [FKak3isvd5b0et5pjh3i600tmf7]
GO
ALTER TABLE [dbo].[tb_vehicle_hold_result] DROP CONSTRAINT [FK4qj525ebtlw54x5c8dia3e3wi]
GO
ALTER TABLE [dbo].[tb_vehicle_hold] DROP CONSTRAINT [FKoqwbbxidj5iutlp1ae4m0tq28]
GO
ALTER TABLE [dbo].[tb_vehicle_hold] DROP CONSTRAINT [FKdc7s1l2si18l0b5jsg77rf656]
GO
ALTER TABLE [dbo].[tb_vehicle_hold] DROP CONSTRAINT [FKa3rw9sw2kflsx3tgqd5ps67ke]
GO
ALTER TABLE [dbo].[tb_vehicle_hold] DROP CONSTRAINT [FK4pf2g7f1ef43kw55eb43nj12i]
GO
ALTER TABLE [dbo].[tb_vehicle_fit_accessory] DROP CONSTRAINT [FKs1w2hxu4tbgg7jyh7saw93q95]
GO
ALTER TABLE [dbo].[tb_vehicle_fit_accessory] DROP CONSTRAINT [FKjnpuolipsedt6a7ls1sqnyro2]
GO
ALTER TABLE [dbo].[tb_vehicle_excise_result] DROP CONSTRAINT [FKhluw48gtw1kqkx1beb3chun6c]
GO
ALTER TABLE [dbo].[tb_vehicle_excise_result] DROP CONSTRAINT [FKadk5n10vo145ig5cgd08de701]
GO
ALTER TABLE [dbo].[tb_vehicle_excise] DROP CONSTRAINT [FKkt2u0yb3dioj5q6bir4ims2ph]
GO
ALTER TABLE [dbo].[tb_vehicle_edaftar] DROP CONSTRAINT [FKsns6vm8pj46daxbxy5sk4uwtq]
GO
ALTER TABLE [dbo].[tb_vehicle_detail] DROP CONSTRAINT [FKopktgw8nausv7fihxr3hffu3g]
GO
ALTER TABLE [dbo].[tb_vehicle_accessory] DROP CONSTRAINT [FKol4ethy5h46u46ig1b9mm1ni8]
GO
ALTER TABLE [dbo].[tb_vehicle_accessory] DROP CONSTRAINT [FKg2bvj5fho4ni7bysudvoylwyg]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKtqw0riglhc056k53ar2wjghkt]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKs52eru8lirx5p7djisqlg17qt]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKrr9d07m6vpepiuk85ecrgsae3]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKraqe96mts7p5snlvqit4jfl0t]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKpvm0bl7t2j0fyycx07hf5wi05]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKn5ugu24o4vvhmi7v8geumcpyv]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKmghjj5ft4cigt7tabojc4fdbp]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKkew4r1m266colqb495qwabat]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKjmb0w3rrcrx4hg3f4kx3l3aat]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKjcjrerqnj390gsq1oletm87qj]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKj2t8atm9yf8xc2a58gnouuw3i]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKixccg14xfax6s8tp8u34mnki]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKire4q2j44td5l2dltb9wivmlq]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKioiux0ndtw54ecm2b91o8wu39]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKfgu681irsn56oebtwwxkek66e]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKdhqq3da7lbjd2u97dguspo10y]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKcwtvge253ob1hiv3asrnnncbl]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKboarh8ttskmw176yhr024kw0k]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FKayf6xmjjynb20i5ydp69ek6gr]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK9helpdtmtfkmbcjnhl65m24n3]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK6rxe4q1rbibiv17k4tyvm7p38]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK6ky05pki1rn169hcyg2eq2i4r]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK6j134cjbwclfhqtfr5376m8m0]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK5qayntey8vg8ol30fj867xold]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK46et323awotjmwjrjp1wp59kx]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK3c240gyk0jox1r31vgmscjk2e]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK1gyqjggcjo2ouorbl1r6wjp3m]
GO
ALTER TABLE [dbo].[tb_vehicle] DROP CONSTRAINT [FK1g82b50do5nm9k3gq9wqicwld]
GO
ALTER TABLE [dbo].[tb_variant_trim_package] DROP CONSTRAINT [FKdjm27e5bxnur8hsh9sg49gexx]
GO
ALTER TABLE [dbo].[tb_variant_trim_package] DROP CONSTRAINT [FK8rlj6uenaus8a4dwhs183ovub]
GO
ALTER TABLE [dbo].[tb_variant_loose_item] DROP CONSTRAINT [FKgjj9qo1fjimc9ovueoexxbo1m]
GO
ALTER TABLE [dbo].[tb_variant_loose_item] DROP CONSTRAINT [FKaoeopeww95elheqm3nkj4n3mr]
GO
ALTER TABLE [dbo].[tb_variant_colour] DROP CONSTRAINT [FKsdjafa3yqti7s60li3kg2lnur]
GO
ALTER TABLE [dbo].[tb_variant_colour] DROP CONSTRAINT [FK3sl91h7ijd1i27ge812sy5krd]
GO
ALTER TABLE [dbo].[tb_variant_accessory] DROP CONSTRAINT [FKjr0ex00raa5fina9367grsddk]
GO
ALTER TABLE [dbo].[tb_variant_accessory] DROP CONSTRAINT [FKhge0ysx96re7flt5rykvd710y]
GO
ALTER TABLE [dbo].[tb_variant] DROP CONSTRAINT [FKt3mjil1extaw9rfyvcxti765q]
GO
ALTER TABLE [dbo].[tb_variant] DROP CONSTRAINT [FKe7twp918ogusxld53oo0a817s]
GO
ALTER TABLE [dbo].[tb_variant] DROP CONSTRAINT [FKchasoo3mjl2agr2mv0xf10ogj]
GO
ALTER TABLE [dbo].[tb_variant] DROP CONSTRAINT [FK5ao7ngy130qk95huymh491nxc]
GO
ALTER TABLE [dbo].[tb_variant] DROP CONSTRAINT [FK36gbr5vwotfno8luma0c38ms6]
GO
ALTER TABLE [dbo].[tb_trim_package_detail] DROP CONSTRAINT [FKnv3osn03m69onku64u2hoopql]
GO
ALTER TABLE [dbo].[tb_trim_package_detail] DROP CONSTRAINT [FKd22oqr77k6sa3hxt0syi1yj1i]
GO
ALTER TABLE [dbo].[tb_trade_in] DROP CONSTRAINT [FKod8i1bcfketsp7y3didt3kiop]
GO
ALTER TABLE [dbo].[tb_trade_in] DROP CONSTRAINT [FKlkynpmofnberjvn8qkp1ixb1e]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_region] DROP CONSTRAINT [FKdl1ub4o5wo5pvtc4dw1hyyqri]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_region] DROP CONSTRAINT [FKbwvrm90x2cs4b1kw38iqaeigi]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_outlet] DROP CONSTRAINT [FKbisb3v2ndr6rd8c1fome790i]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_country] DROP CONSTRAINT [FKt4x9eaqqdsevauuvisdn8b00m]
GO
ALTER TABLE [dbo].[tb_sublet_contractor_country] DROP CONSTRAINT [FKibrnyfqelnm4nyxwwlp4swxo7]
GO
ALTER TABLE [dbo].[tb_state] DROP CONSTRAINT [FKjln74j2yroa9uytbulswttfi9]
GO
ALTER TABLE [dbo].[tb_service_record] DROP CONSTRAINT [FKs3fwdh432dqu7x7ubn5rxmtlv]
GO
ALTER TABLE [dbo].[tb_service] DROP CONSTRAINT [FKse4fmjipj69r18kxmvxhr2igr]
GO
ALTER TABLE [dbo].[tb_service] DROP CONSTRAINT [FKg55gi409aqlakt3oxclc044v6]
GO
ALTER TABLE [dbo].[tb_sales_order_track] DROP CONSTRAINT [FKr870sksg2h4928b3q9gl7d1g2]
GO
ALTER TABLE [dbo].[tb_sales_order_price_detail] DROP CONSTRAINT [FKpp8cka9m2a2j7d4a98tgijr66]
GO
ALTER TABLE [dbo].[tb_sales_order_price_detail] DROP CONSTRAINT [FKpmchmbukvpfjbi90q7d45f15e]
GO
ALTER TABLE [dbo].[tb_sales_order_accessory] DROP CONSTRAINT [FK7xogayygix5mnp8tspkxmciab]
GO
ALTER TABLE [dbo].[tb_sales_order_accessory] DROP CONSTRAINT [FK14qp8qxbsdkds2ebvnist6t93]
GO
ALTER TABLE [dbo].[tb_sales_order_accessory] DROP CONSTRAINT [FK11l4xcwuveya10qpm2yw46pyj]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FKtr8tks7jhhaf62qnmcc0h0lj7]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FKimybfqfars3qla60wjurilnbl]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FKhx3tkj459vs4c2trih1m4mq3]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FKhj74wg262d5q2a3a42pnv0krq]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FKhbfb0qvff2lg3ldsw0b6qnq52]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FKeqg5nk8du3v6dcxgm8ehg20pc]
GO
ALTER TABLE [dbo].[tb_sales_order] DROP CONSTRAINT [FK529igmrrlnn4bgln7qjrbo7gk]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FKsutj5a4375y2lbt0jxnj4154f]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FKrv0vi1hnwqcyhg5u6vma1q3nk]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FKrbnu8qfv3hfk3kv7xyetnht3c]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FKnr4w7l4swtbxcb1dqoigapsdi]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FKklsub2ltoyr7knehueusegumi]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FKjid9bgqtnk3kqllvmvrnnavib]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FK8xmdvjo9ebvxqsqk3qaw8mhk8]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FK8vrmw2e8n4h8bv71s67p6xjba]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FK6r68tgr8kqmt9webqykj5urwm]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FK5mt95bd2i1if55kvkh4s0ymd2]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FK4wnae50p589susr5hbbacjhe2]
GO
ALTER TABLE [dbo].[tb_sales] DROP CONSTRAINT [FK3m8njcmq8rx3eidg5i6fqgxkj]
GO
ALTER TABLE [dbo].[tb_request_track] DROP CONSTRAINT [FK8ndbfn3lbfrqa623dy0h4ntkx]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FKff332upd3niorxcy0mic1xesp]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FKe2br24kl2d3wf5ju9c1p34k1h]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FKbenbmewxyy71jt7k85rer1lsu]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FK9x49qgswa4eq6hedgoht2eqsy]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FK8dre7ymlbdo5kob0ficbs2wt3]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FK6ikqcqhq216j84hxpkfhtnj1d]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FK5vl7fgph362ex0qt7jic4bucu]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FK1djbs1v9m5tbciwhs4wn4b613]
GO
ALTER TABLE [dbo].[tb_request_detail] DROP CONSTRAINT [FK15seopu2f62yvjiacek51pbar]
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail] DROP CONSTRAINT [FKtc9jqifl96jhmks3pys1dlj4k]
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail] DROP CONSTRAINT [FKrpv66307mc6ytui9ggufskfkj]
GO
ALTER TABLE [dbo].[tb_register_out_pay_detail] DROP CONSTRAINT [FKifyxsl3vl24teodad76sa9q62]
GO
ALTER TABLE [dbo].[tb_register_out_pay] DROP CONSTRAINT [FKrowtilkqdjxjg4o96lvisqtgn]
GO
ALTER TABLE [dbo].[tb_register_out_pay] DROP CONSTRAINT [FKlhsi1w88xcnbgw3yrsnil9443]
GO
ALTER TABLE [dbo].[tb_region] DROP CONSTRAINT [FKax2orrd96ep90vrd6c1soxd83]
GO
ALTER TABLE [dbo].[tb_quotation_track] DROP CONSTRAINT [FKsx2vj2imeiwcu37adkp0up0af]
GO
ALTER TABLE [dbo].[tb_quotation_price_detail] DROP CONSTRAINT [FKpvucs4qyry1478sx57od9kej4]
GO
ALTER TABLE [dbo].[tb_quotation_price_detail] DROP CONSTRAINT [FKkq0blaxoxveaceux7petx2x1a]
GO
ALTER TABLE [dbo].[tb_quotation_accessory] DROP CONSTRAINT [FKhode9y9w1frhhp8ajvm2198d1]
GO
ALTER TABLE [dbo].[tb_quotation_accessory] DROP CONSTRAINT [FKa1192wq73891sj0uq4ccpej71]
GO
ALTER TABLE [dbo].[tb_quotation_accessory] DROP CONSTRAINT [FK3343njtxecd8rbd2yjweo7qc4]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FKqoipr16wf34tq47p1y7u3flbr]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FKoqj8ida2dg3r73ypci1od4ij8]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FKo77d7myeo7n2g9x4yttx0d6gg]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FKo4yn7uv4c7mu734dh2lehgfgb]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FKgilch6hcddiwbo0qna4qtm8ty]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FK7s5phg2d38vs3an95cvob2rm1]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FK7k7qnug6fy3emjaghgtgeywuc]
GO
ALTER TABLE [dbo].[tb_quotation] DROP CONSTRAINT [FK1kcm10nc20hm3d4x314oa5wf4]
GO
ALTER TABLE [dbo].[tb_promotion] DROP CONSTRAINT [FKs95b38k51iq18ijnbyfnoycrw]
GO
ALTER TABLE [dbo].[tb_promotion] DROP CONSTRAINT [FKknub2dq2a4p57l5rapcwr5lll]
GO
ALTER TABLE [dbo].[tb_promotion] DROP CONSTRAINT [FKga38606wl3gtlmbc9gpjwch68]
GO
ALTER TABLE [dbo].[tb_price_plan_variant_detail] DROP CONSTRAINT [FK2nuacsc2br5eydw4nl2098bl9]
GO
ALTER TABLE [dbo].[tb_price_plan_variant_detail] DROP CONSTRAINT [FK18l7754tjsk8q40u3qpy045fh]
GO
ALTER TABLE [dbo].[tb_price_plan_variant] DROP CONSTRAINT [FKfjramdbl0p5i9gsbr0nkyxet2]
GO
ALTER TABLE [dbo].[tb_price_plan_variant] DROP CONSTRAINT [FK8upnqtq4dqhsj2k4ml60rudnw]
GO
ALTER TABLE [dbo].[tb_price_plan] DROP CONSTRAINT [FKk7fkv2a5492m73q5t1d787tvq]
GO
ALTER TABLE [dbo].[tb_price_plan] DROP CONSTRAINT [FKhwoak51g61kqrup8bb8cges8c]
GO
ALTER TABLE [dbo].[tb_price_plan] DROP CONSTRAINT [FK5vab8ltha0vap7r96ysbb3apm]
GO
ALTER TABLE [dbo].[tb_price_plan] DROP CONSTRAINT [FK3irxs6tldu526xwr7urv1sjkr]
GO
ALTER TABLE [dbo].[tb_price_plan] DROP CONSTRAINT [FK1edhvoivoy89s3e93lstpnfdq]
GO
ALTER TABLE [dbo].[tb_payment_voucher_result] DROP CONSTRAINT [FKa79wbrd3oemj58r1rmxaejvg1]
GO
ALTER TABLE [dbo].[tb_payment_voucher_result] DROP CONSTRAINT [FK7bsfyiekdih56b7pfcasycff8]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKq35dt6nqkck7y22gxw6m66764]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKp93x5iq6q2ruqinmcqe1el9xp]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKp2m7op08n5k9lgugxyno4pgny]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKlre0s8x3xfwpvlwqd0obvhr1g]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKk2ndyaguw4u2iwys6s9b6jobj]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKjt8djlsmnne99s0e0ae11ls4g]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FKd3ph1chumq3tk5oo9gww78a5w]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FK6pbgmyg2ok0lni1dlufnbycf4]
GO
ALTER TABLE [dbo].[tb_payment_voucher] DROP CONSTRAINT [FK5ho7il18bd2palrl29wk5wko7]
GO
ALTER TABLE [dbo].[tb_payment_terms_variant] DROP CONSTRAINT [FKqu64pa8cy2y9mntdat2r0j00g]
GO
ALTER TABLE [dbo].[tb_payment_terms_variant] DROP CONSTRAINT [FKc3hxeknve3agri14949ld4yrl]
GO
ALTER TABLE [dbo].[tb_payment_request] DROP CONSTRAINT [FKlr8ejxcn3bksby222y4vr5amb]
GO
ALTER TABLE [dbo].[tb_payment_request] DROP CONSTRAINT [FKbr3myh6hhbs014ngv98srjmgx]
GO
ALTER TABLE [dbo].[tb_payment_request] DROP CONSTRAINT [FKb9kp3fnce3rxcho3e3g5n1nf7]
GO
ALTER TABLE [dbo].[tb_payment_request] DROP CONSTRAINT [FK78831ml6c51e0j8qdbnfofwij]
GO
ALTER TABLE [dbo].[tb_payment_detail] DROP CONSTRAINT [FK83hqb6f2n2x30rbnhdeh28wi3]
GO
ALTER TABLE [dbo].[tb_payment_account] DROP CONSTRAINT [FKb71gdnl5sjqjxur7kfsggf8cc]
GO
ALTER TABLE [dbo].[tb_payment] DROP CONSTRAINT [FKdglrwbkq8i1isp5s55qt1l07t]
GO
ALTER TABLE [dbo].[tb_payment] DROP CONSTRAINT [FK36m9di3jpfx3x836a721r7mb9]
GO
ALTER TABLE [dbo].[tb_pay_voucher_vehicle] DROP CONSTRAINT [FKrpp6moej3ahujxkq1un837a65]
GO
ALTER TABLE [dbo].[tb_pay_voucher_vehicle] DROP CONSTRAINT [FKaipmgorcf620suu6srhkybu6h]
GO
ALTER TABLE [dbo].[tb_outlet] DROP CONSTRAINT [FKhl1jtxxf24druqavwnnd5s2tf]
GO
ALTER TABLE [dbo].[tb_outlet] DROP CONSTRAINT [FKcwdxlhjgsrx6wn1heasqr8ibx]
GO
ALTER TABLE [dbo].[tb_location] DROP CONSTRAINT [FKpfvl97cxef87uv02j38y5wroa]
GO
ALTER TABLE [dbo].[tb_location] DROP CONSTRAINT [FKliubc1niiwxyj45sxnv1fyfqy]
GO
ALTER TABLE [dbo].[tb_location] DROP CONSTRAINT [FK7q78iypopivs92sdgmk8w1e5b]
GO
ALTER TABLE [dbo].[tb_location] DROP CONSTRAINT [FK7ac2o9ey6mw5fhapcwipgay6m]
GO
ALTER TABLE [dbo].[tb_location] DROP CONSTRAINT [FK1ng2o55c7ia0232wtxq7442mo]
GO
ALTER TABLE [dbo].[tb_invoice] DROP CONSTRAINT [FKnq73pwn6xeu30xs01goq8rssm]
GO
ALTER TABLE [dbo].[tb_invoice] DROP CONSTRAINT [FKjngd8j9806y78hp01a8idnvwi]
GO
ALTER TABLE [dbo].[tb_invoice] DROP CONSTRAINT [FK9sbqhmwy75sv2hbehjq9ei90q]
GO
ALTER TABLE [dbo].[tb_invoice] DROP CONSTRAINT [FK598ml90te8h5kkioyrqxr952g]
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail] DROP CONSTRAINT [FKrwxdb2g4b3qc23qpq2r8q4yt4]
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail] DROP CONSTRAINT [FKgg4g705va0ft3ovkbpwv2wxt1]
GO
ALTER TABLE [dbo].[tb_inspection_checklist_detail] DROP CONSTRAINT [FK2ldsejh6vvale5ws0hfqbitxt]
GO
ALTER TABLE [dbo].[tb_inspection_checklist] DROP CONSTRAINT [FK1xvry6vuqt1t31x258l72a1st]
GO
ALTER TABLE [dbo].[tb_inspection] DROP CONSTRAINT [FKmvdai3wujhw9rwp6cxq042fll]
GO
ALTER TABLE [dbo].[tb_incentive_type] DROP CONSTRAINT [FK4ia73rye95t7a6g9ha2bb29na]
GO
ALTER TABLE [dbo].[tb_incentive_detail] DROP CONSTRAINT [FKqonfgtkksl5jruyqth4vcjee6]
GO
ALTER TABLE [dbo].[tb_incentive_detail] DROP CONSTRAINT [FKoofrqk14151t7l8s1schc1h5e]
GO
ALTER TABLE [dbo].[tb_incentive_detail] DROP CONSTRAINT [FKjgu28xkrtcbc5hqy6t8oxmslx]
GO
ALTER TABLE [dbo].[tb_incentive_detail] DROP CONSTRAINT [FK3mv786y3s3u56vsif3rj6km5k]
GO
ALTER TABLE [dbo].[tb_excise] DROP CONSTRAINT [FK22s302l0sqqd9kavrfelpwbmn]
GO
ALTER TABLE [dbo].[tb_department] DROP CONSTRAINT [FKqgm7qmlu0sbewumrniomlo99c]
GO
ALTER TABLE [dbo].[tb_department] DROP CONSTRAINT [FKo6og43gtnakoy583olnbt7elw]
GO
ALTER TABLE [dbo].[tb_department] DROP CONSTRAINT [FK955s6h2hmdqc7weuq8gfpwxbl]
GO
ALTER TABLE [dbo].[tb_department] DROP CONSTRAINT [FK8qsca9vsdo24pruldkoyla688]
GO
ALTER TABLE [dbo].[tb_delivery_result] DROP CONSTRAINT [FK5bwnksr9h0cj54h05bgjsce2l]
GO
ALTER TABLE [dbo].[tb_delivery_result] DROP CONSTRAINT [FK5b4mbc4x05f98m7an9ft1qnnf]
GO
ALTER TABLE [dbo].[tb_delivery_detail] DROP CONSTRAINT [FKsy8dtfnptg9nkbd8nvqtk63jl]
GO
ALTER TABLE [dbo].[tb_delivery_detail] DROP CONSTRAINT [FKsae535hsaehllv6hkavhpki47]
GO
ALTER TABLE [dbo].[tb_delivery_detail] DROP CONSTRAINT [FKdw6q1fgq8086a3werrk18rkqb]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FKsrjfjnys7nttw2twt634rbo98]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FKot9pl56fhnir27kr6ql78iu0f]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FKgmh8vshvwh6nbe3l3dhwfr3y0]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FKdfrsgxwdn1nn01cgr5g44yunr]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FKa8k1g5kb77wktl9guqniwmlds]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FK90llnafi2joegfxx5j8wfkqmt]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FK8pgg90mf20urm1gdpcwurh73b]
GO
ALTER TABLE [dbo].[tb_delivery] DROP CONSTRAINT [FK5n82utna4714h4b65gm9xleaq]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FKi138ax5icvj6d49sb98qjaad]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FKcuwh8gw2yahixawkbiedl7km]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FKck8rdsxl2rxqxj0lluht6g1cd]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FKbsg9nofr116uqexu8qkr4tkub]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FKb2r84bcdnh9knlkb14stv761f]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FK83ambv2su8utcec9u7r2jfxfp]
GO
ALTER TABLE [dbo].[tb_debit_note] DROP CONSTRAINT [FK6sltj5qok2w134bnq2ag999mt]
GO
ALTER TABLE [dbo].[tb_dealer_request_order] DROP CONSTRAINT [FKt2m9ar0tan9iusu1x2mdxb5ty]
GO
ALTER TABLE [dbo].[tb_dealer_request_order] DROP CONSTRAINT [FKonx5h6x4vxak6dk3j2fhnvb59]
GO
ALTER TABLE [dbo].[tb_dealer_request_order] DROP CONSTRAINT [FKnkjfa09sf1byd09jadt5jnmev]
GO
ALTER TABLE [dbo].[tb_dealer_request_order] DROP CONSTRAINT [FKhsaelxta6oddqgdkglpwxi8v]
GO
ALTER TABLE [dbo].[tb_dealer_promotion] DROP CONSTRAINT [FKs7ovlkx8pa6xo9oh7a0x2wkwp]
GO
ALTER TABLE [dbo].[tb_dealer_promotion] DROP CONSTRAINT [FKm9m3tek7i3vhxxsd300n2othl]
GO
ALTER TABLE [dbo].[tb_dealer_promotion] DROP CONSTRAINT [FK9qlg0wy6oae1k7eakf7ry4svq]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_selected_item] DROP CONSTRAINT [FKiq279ft9wexelbhmhtkn39yk]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_item] DROP CONSTRAINT [FKqeyfk9rupp6o861lysy2lyopa]
GO
ALTER TABLE [dbo].[tb_dealer_invoice_item] DROP CONSTRAINT [FK9kjfjg8gsyk70ao85vak6t5d8]
GO
ALTER TABLE [dbo].[tb_dealer_invoice] DROP CONSTRAINT [FKtq5tnkfut9vcdvp5tbar399ct]
GO
ALTER TABLE [dbo].[tb_dealer_invoice] DROP CONSTRAINT [FKtf4bquemfpkwjaifuexksy1hp]
GO
ALTER TABLE [dbo].[tb_dealer_invoice] DROP CONSTRAINT [FKpmgepgy6bm399ek7dgpuktma7]
GO
ALTER TABLE [dbo].[tb_dealer_invoice] DROP CONSTRAINT [FKmd7b4jubfh9k76p53kcm3istw]
GO
ALTER TABLE [dbo].[tb_dealer_invoice] DROP CONSTRAINT [FKgv7fqdfm2lcf0li8gycvwvt09]
GO
ALTER TABLE [dbo].[tb_dealer_invoice] DROP CONSTRAINT [FK6x87lk5vkxcu9xe3uwhitq7da]
GO
ALTER TABLE [dbo].[tb_customer_spouse] DROP CONSTRAINT [FKpwr7sjbdww8wvtt37m3ffwftd]
GO
ALTER TABLE [dbo].[tb_customer_spouse] DROP CONSTRAINT [FKjgeihes10629uchdwchdoatbd]
GO
ALTER TABLE [dbo].[tb_customer_spouse] DROP CONSTRAINT [FK49q9qwpca6slj27ul558t6caw]
GO
ALTER TABLE [dbo].[tb_customer_phone] DROP CONSTRAINT [FKefig1jugfkgo7juhetjj8ug3r]
GO
ALTER TABLE [dbo].[tb_customer_person] DROP CONSTRAINT [FKqxm6q7mv2qt07724i4fgt4faj]
GO
ALTER TABLE [dbo].[tb_customer_person] DROP CONSTRAINT [FKmmax4wq5dd5i42yxys2eyfhn5]
GO
ALTER TABLE [dbo].[tb_customer_person] DROP CONSTRAINT [FKl6j6sgckf49pakuymv4x0sq94]
GO
ALTER TABLE [dbo].[tb_customer_person] DROP CONSTRAINT [FKdaf957opp4psxke7bsymnswys]
GO
ALTER TABLE [dbo].[tb_customer_email] DROP CONSTRAINT [FK5t5ei6l42gmvf2ie275eq7wlv]
GO
ALTER TABLE [dbo].[tb_customer_company] DROP CONSTRAINT [FKbitpjv9ibc70l96wutrdmiyfc]
GO
ALTER TABLE [dbo].[tb_customer_address] DROP CONSTRAINT [FKqm9edwirwkmjgqklw53q1cfi2]
GO
ALTER TABLE [dbo].[tb_customer_address] DROP CONSTRAINT [FKqartxbvygicbxqocav5akcy45]
GO
ALTER TABLE [dbo].[tb_customer_address] DROP CONSTRAINT [FKp9jkml62wn9g90jfsysw77v5]
GO
ALTER TABLE [dbo].[tb_customer_address] DROP CONSTRAINT [FKkewbv5ahj182bx1f170d2wf4s]
GO
ALTER TABLE [dbo].[tb_customer_address] DROP CONSTRAINT [FK367o4kdkp4v36gdun3jc4a8ej]
GO
ALTER TABLE [dbo].[tb_customer] DROP CONSTRAINT [FKsg3nst9sqdex2ob79h0foqia5]
GO
ALTER TABLE [dbo].[tb_customer] DROP CONSTRAINT [FKqhhbdap7ly83ov373bp2e2x6b]
GO
ALTER TABLE [dbo].[tb_customer] DROP CONSTRAINT [FKjgu9uucqrrio3xe5q5i8shvxv]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FKt6l5gv5atevolqi672suv07vd]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FKq5qf1w43a7k8a2p1qbvhrlhns]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FKobxbwid9lljgg44b2iea64rix]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FKlcifrnn0wb7xmqw5b77b4rs6x]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FKgr9vd4nt2fei9cvembnwijuhu]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FKfae32i66t6as8slejjlr9sy9w]
GO
ALTER TABLE [dbo].[tb_cust_debit_note] DROP CONSTRAINT [FK4fk3ryqjwhe5icul2xwhrn77y]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FKr4he1hf8djiw1k46fhhiksbs1]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FKqud1m0xlpde6y92qm0v5cdcdc]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FKp38hv66xs8dn5yh9l7fndl80f]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FKnyqqy06jdhomw4u0d28wx5ryd]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FKfd306ghin7tqhm77qbqefwy32]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FKd67yb8p94iaqr9yjtvtknhh6m]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FK98hxgfemj0hxbdcoec03u92eo]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FK3tgr3yvurynt669ybnpkgpw0t]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FK3g944v3uxst4xsxi1j0gn1esh]
GO
ALTER TABLE [dbo].[tb_cust_credit_note] DROP CONSTRAINT [FK2g2mni66xkq4a3313npy09079]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FKs9ncr5crdgqqn00alb5vrsgu8]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FKs4yjp9h326qy0b4fv02h00608]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FKqwmjxo3mar8g499j57ddcq7u5]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FKoxt45hi7rf7h7i5jnl45ujnlq]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FKh3frhkftcnvupcsvgywrnlssa]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FK7kq4k8mr13qp7urcaybaa83lu]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FK475plg1mv928t1vstrcrcqtjq]
GO
ALTER TABLE [dbo].[tb_credit_note] DROP CONSTRAINT [FK245o7kls027ysrbw5x088irch]
GO
ALTER TABLE [dbo].[tb_company_department] DROP CONSTRAINT [FKhd6mc1snoweibjejwptw280k3]
GO
ALTER TABLE [dbo].[tb_company_department] DROP CONSTRAINT [FK7wtmh8s6ndumvrmlt2lqfyufk]
GO
ALTER TABLE [dbo].[tb_company_address] DROP CONSTRAINT [FKc2ypb6dtjr0h8mhp4fu7y8som]
GO
ALTER TABLE [dbo].[tb_company_address] DROP CONSTRAINT [FK96inuwca6eoepdarhjel3wyt8]
GO
ALTER TABLE [dbo].[tb_company_address] DROP CONSTRAINT [FK5888d74e4c3h7y50hx715fkrx]
GO
ALTER TABLE [dbo].[tb_company_address] DROP CONSTRAINT [FK4gifny6y5a6tiytv952tfg027]
GO
ALTER TABLE [dbo].[tb_company_address] DROP CONSTRAINT [FK130edah2a7padse9vqh1ecb6s]
GO
ALTER TABLE [dbo].[tb_commercial_colour] DROP CONSTRAINT [FKs6djk4lqemc6xfu4st6lwwxq8]
GO
ALTER TABLE [dbo].[tb_commercial_colour] DROP CONSTRAINT [FKa5jqrmule56lek74paw0k2oky]
GO
ALTER TABLE [dbo].[tb_city] DROP CONSTRAINT [FK1rn7oty4mwqviyw8vk67crapo]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKpr4b5x8lugoctoh3ux2d76f20]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKp39bgkjfvvaarwg1t5knkdteu]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKo5yhdlp5r8oavtf15fiwtn1on]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKntatkysd9wn30w5pearih7msc]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKnhcdkwe7q5k6roca178xv3pj3]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKnbm9s4xc6utgfceitj8843qfn]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKltqbwa5siq4xl3oewa1wa4w3q]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKknisaqc3aj3fnqhl0c6d19850]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKibtec8wlcxjh0hmq0kh1orqfb]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKiasfk6px4vvmxgtrce04fuf69]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKhukjrp6dbiwbqgldssts3rkvk]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKhga1r6xkvquqw573ljfkmgjyc]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKg7fnid7s9l21ghxu6cv5ea31c]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKg6f0lhtg6oogtsbfg2nsiw8ih]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKfh392n0msq6or3gjsu38nf6qw]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKec16qdgkiylcuf27u5icvr7ku]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKcp3aigv7h1w1qkrb9u7yam0j6]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FKb9qinw3pmjco9gh8ghm6ajjjb]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK9p5ij9q7ytlfo58higvyao7jr]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK7isdpi6eq5o5i91sb8kdfkibd]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK6ysh4pdykpwpgg6fyqb1mjur8]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK44a402yqc288s9dqgtg7xcax]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK412slcr5vbrbcc0ohhv36mlyt]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK3mtkq0lw1e8p70nit993ckjy5]
GO
ALTER TABLE [dbo].[tb_buyer_order] DROP CONSTRAINT [FK324wl5fy3lo27uf274ghwrv4f]
GO
ALTER TABLE [dbo].[tb_allocation_result] DROP CONSTRAINT [FKos869jqmi87o9q8rv6bi196am]
GO
ALTER TABLE [dbo].[tb_allocation_result] DROP CONSTRAINT [FKmrtv3aq4idov0mjhktl7o7pqf]
GO
ALTER TABLE [dbo].[tb_allocation_result] DROP CONSTRAINT [FKh40v9m96d5syt80hafw3m2sm3]
GO
ALTER TABLE [dbo].[tb_allocation_result] DROP CONSTRAINT [FK3pt0lvxm54foisj0c5emk9a0b]
GO
ALTER TABLE [dbo].[tb_allocation_result] DROP CONSTRAINT [FK35ur4a59p7tqxtjqbvye1eqf0]
GO
ALTER TABLE [dbo].[tb_allocation] DROP CONSTRAINT [FKlu22rxsqcavxmmdgbpsc34mjv]
GO
ALTER TABLE [dbo].[tb_allocation] DROP CONSTRAINT [FK9o58ftm3sqq43cj0t8mwyg5kw]
GO
ALTER TABLE [dbo].[tb_allocation] DROP CONSTRAINT [FK6sql8emsyh14m9bhr5f0qidkg]
GO
ALTER TABLE [dbo].[tb_allocation] DROP CONSTRAINT [FK3xrgmc8paafik64a5qmocb6e6]
GO
/****** Object:  Table [dbo].[tb_workflow_status]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_workflow_status]
GO
/****** Object:  Table [dbo].[tb_warranty_plan]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_warranty_plan]
GO
/****** Object:  Table [dbo].[tb_vehvar_type]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehvar_type]
GO
/****** Object:  Table [dbo].[tb_vehicle_use]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_use]
GO
/****** Object:  Table [dbo].[tb_vehicle_type]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_type]
GO
/****** Object:  Table [dbo].[tb_vehicle_status]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_status]
GO
/****** Object:  Table [dbo].[tb_vehicle_series]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_series]
GO
/****** Object:  Table [dbo].[tb_vehicle_receive_order_detail]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_receive_order_detail]
GO
/****** Object:  Table [dbo].[tb_vehicle_receive_order]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_receive_order]
GO
/****** Object:  Table [dbo].[tb_vehicle_po_track]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_po_track]
GO
/****** Object:  Table [dbo].[tb_vehicle_po_detail]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_po_detail]
GO
/****** Object:  Table [dbo].[tb_vehicle_po]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_po]
GO
/****** Object:  Table [dbo].[tb_vehicle_pdi_result_detail]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_pdi_result_detail]
GO
/****** Object:  Table [dbo].[tb_vehicle_pdi_result]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_pdi_result]
GO
/****** Object:  Table [dbo].[tb_vehicle_pdi]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_pdi]
GO
/****** Object:  Table [dbo].[tb_vehicle_payment_term]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_payment_term]
GO
/****** Object:  Table [dbo].[tb_vehicle_model]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_model]
GO
/****** Object:  Table [dbo].[tb_vehicle_miti_result]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_miti_result]
GO
/****** Object:  Table [dbo].[tb_vehicle_miti]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_miti]
GO
/****** Object:  Table [dbo].[tb_vehicle_loose_item]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_loose_item]
GO
/****** Object:  Table [dbo].[tb_vehicle_kastam_result]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_kastam_result]
GO
/****** Object:  Table [dbo].[tb_vehicle_kastam]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_kastam]
GO
/****** Object:  Table [dbo].[tb_vehicle_hold_result]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_hold_result]
GO
/****** Object:  Table [dbo].[tb_vehicle_hold]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_hold]
GO
/****** Object:  Table [dbo].[tb_vehicle_fit_accessory]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_fit_accessory]
GO
/****** Object:  Table [dbo].[tb_vehicle_excise_result]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_excise_result]
GO
/****** Object:  Table [dbo].[tb_vehicle_excise]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_excise]
GO
/****** Object:  Table [dbo].[tb_vehicle_edaftar]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_edaftar]
GO
/****** Object:  Table [dbo].[tb_vehicle_detail]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_detail]
GO
/****** Object:  Table [dbo].[tb_vehicle_category]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_category]
GO
/****** Object:  Table [dbo].[tb_vehicle_brand]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_brand]
GO
/****** Object:  Table [dbo].[tb_vehicle_body_type]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_body_type]
GO
/****** Object:  Table [dbo].[tb_vehicle_body_style]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_body_style]
GO
/****** Object:  Table [dbo].[tb_vehicle_accessory]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle_accessory]
GO
/****** Object:  Table [dbo].[tb_vehicle]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_vehicle]
GO
/****** Object:  Table [dbo].[tb_variant_type]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_variant_type]
GO
/****** Object:  Table [dbo].[tb_variant_trim_package]    Script Date: 11-Oct-16 10:39:46 AM ******/
DROP TABLE [dbo].[tb_variant_trim_package]
GO
/****** Object:  Table [dbo].[tb_variant_loose_item]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_variant_loose_item]
GO
/****** Object:  Table [dbo].[tb_variant_colour]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_variant_colour]
GO
/****** Object:  Table [dbo].[tb_variant_accessory]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_variant_accessory]
GO
/****** Object:  Table [dbo].[tb_variant]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_variant]
GO
/****** Object:  Table [dbo].[tb_user]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_user]
GO
/****** Object:  Table [dbo].[tb_usage_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_usage_type]
GO
/****** Object:  Table [dbo].[tb_trim_package_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_trim_package_detail]
GO
/****** Object:  Table [dbo].[tb_trim_package_category]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_trim_package_category]
GO
/****** Object:  Table [dbo].[tb_trim_package]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_trim_package]
GO
/****** Object:  Table [dbo].[tb_trade_in_brand]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_trade_in_brand]
GO
/****** Object:  Table [dbo].[tb_trade_in]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_trade_in]
GO
/****** Object:  Table [dbo].[tb_supplier_code]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_supplier_code]
GO
/****** Object:  Table [dbo].[tb_sublet_po_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sublet_po_type]
GO
/****** Object:  Table [dbo].[tb_sublet_po]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sublet_po]
GO
/****** Object:  Table [dbo].[tb_sublet_contractor_region]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sublet_contractor_region]
GO
/****** Object:  Table [dbo].[tb_sublet_contractor_outlet]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sublet_contractor_outlet]
GO
/****** Object:  Table [dbo].[tb_sublet_contractor_country]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sublet_contractor_country]
GO
/****** Object:  Table [dbo].[tb_sublet_contractor]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sublet_contractor]
GO
/****** Object:  Table [dbo].[tb_status]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_status]
GO
/****** Object:  Table [dbo].[tb_state]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_state]
GO
/****** Object:  Table [dbo].[tb_service_record]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_service_record]
GO
/****** Object:  Table [dbo].[tb_service_plan]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_service_plan]
GO
/****** Object:  Table [dbo].[tb_service_package]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_service_package]
GO
/****** Object:  Table [dbo].[tb_service]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_service]
GO
/****** Object:  Table [dbo].[tb_send_email]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_send_email]
GO
/****** Object:  Table [dbo].[tb_salutation]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_salutation]
GO
/****** Object:  Table [dbo].[tb_sales_order_track]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sales_order_track]
GO
/****** Object:  Table [dbo].[tb_sales_order_price_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sales_order_price_detail]
GO
/****** Object:  Table [dbo].[tb_sales_order_accessory]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sales_order_accessory]
GO
/****** Object:  Table [dbo].[tb_sales_order]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sales_order]
GO
/****** Object:  Table [dbo].[tb_sales]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_sales]
GO
/****** Object:  Table [dbo].[tb_request_track]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_request_track]
GO
/****** Object:  Table [dbo].[tb_request_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_request_detail]
GO
/****** Object:  Table [dbo].[tb_reimbursement_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_reimbursement_type]
GO
/****** Object:  Table [dbo].[tb_register_out_pay_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_register_out_pay_detail]
GO
/****** Object:  Table [dbo].[tb_register_out_pay]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_register_out_pay]
GO
/****** Object:  Table [dbo].[tb_region]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_region]
GO
/****** Object:  Table [dbo].[tb_range]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_range]
GO
/****** Object:  Table [dbo].[tb_quotation_track]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_quotation_track]
GO
/****** Object:  Table [dbo].[tb_quotation_price_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_quotation_price_detail]
GO
/****** Object:  Table [dbo].[tb_quotation_accessory]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_quotation_accessory]
GO
/****** Object:  Table [dbo].[tb_quotation]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_quotation]
GO
/****** Object:  Table [dbo].[tb_promotion_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_promotion_type]
GO
/****** Object:  Table [dbo].[tb_promotion]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_promotion]
GO
/****** Object:  Table [dbo].[tb_price_plan_variant_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_price_plan_variant_detail]
GO
/****** Object:  Table [dbo].[tb_price_plan_variant]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_price_plan_variant]
GO
/****** Object:  Table [dbo].[tb_price_plan]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_price_plan]
GO
/****** Object:  Table [dbo].[tb_payment_voucher_result]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_voucher_result]
GO
/****** Object:  Table [dbo].[tb_payment_voucher]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_voucher]
GO
/****** Object:  Table [dbo].[tb_payment_terms_variant]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_terms_variant]
GO
/****** Object:  Table [dbo].[tb_payment_request]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_request]
GO
/****** Object:  Table [dbo].[tb_payment_mode]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_mode]
GO
/****** Object:  Table [dbo].[tb_payment_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_detail]
GO
/****** Object:  Table [dbo].[tb_payment_account]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment_account]
GO
/****** Object:  Table [dbo].[tb_payment]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_payment]
GO
/****** Object:  Table [dbo].[tb_pay_voucher_vehicle]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_pay_voucher_vehicle]
GO
/****** Object:  Table [dbo].[tb_outlet_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_outlet_type]
GO
/****** Object:  Table [dbo].[tb_outlet]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_outlet]
GO
/****** Object:  Table [dbo].[tb_original_status]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_original_status]
GO
/****** Object:  Table [dbo].[tb_origin_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_origin_type]
GO
/****** Object:  Table [dbo].[tb_marriage_status]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_marriage_status]
GO
/****** Object:  Table [dbo].[tb_manufacturer_colour]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_manufacturer_colour]
GO
/****** Object:  Table [dbo].[tb_loose_item]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_loose_item]
GO
/****** Object:  Table [dbo].[tb_location_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_location_type]
GO
/****** Object:  Table [dbo].[tb_location]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_location]
GO
/****** Object:  Table [dbo].[tb_language]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_language]
GO
/****** Object:  Table [dbo].[tb_kastam_reason]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_kastam_reason]
GO
/****** Object:  Table [dbo].[tb_invoice]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_invoice]
GO
/****** Object:  Table [dbo].[tb_insurance_company]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_insurance_company]
GO
/****** Object:  Table [dbo].[tb_inspection_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_inspection_type]
GO
/****** Object:  Table [dbo].[tb_inspection_level]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_inspection_level]
GO
/****** Object:  Table [dbo].[tb_inspection_checklist_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_inspection_checklist_detail]
GO
/****** Object:  Table [dbo].[tb_inspection_checklist]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_inspection_checklist]
GO
/****** Object:  Table [dbo].[tb_inspection]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_inspection]
GO
/****** Object:  Table [dbo].[tb_incentive_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_incentive_type]
GO
/****** Object:  Table [dbo].[tb_incentive_detail_tier]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_incentive_detail_tier]
GO
/****** Object:  Table [dbo].[tb_incentive_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_incentive_detail]
GO
/****** Object:  Table [dbo].[tb_gst_tax]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_gst_tax]
GO
/****** Object:  Table [dbo].[tb_government_colour]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_government_colour]
GO
/****** Object:  Table [dbo].[tb_fuel_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_fuel_type]
GO
/****** Object:  Table [dbo].[tb_excise]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_excise]
GO
/****** Object:  Table [dbo].[tb_ethnicity]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_ethnicity]
GO
/****** Object:  Table [dbo].[tb_department]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_department]
GO
/****** Object:  Table [dbo].[tb_delivery_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_delivery_type]
GO
/****** Object:  Table [dbo].[tb_delivery_result]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_delivery_result]
GO
/****** Object:  Table [dbo].[tb_delivery_detail]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_delivery_detail]
GO
/****** Object:  Table [dbo].[tb_delivery]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_delivery]
GO
/****** Object:  Table [dbo].[tb_debit_note_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_debit_note_type]
GO
/****** Object:  Table [dbo].[tb_debit_note]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_debit_note]
GO
/****** Object:  Table [dbo].[tb_dealer_request_order]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_dealer_request_order]
GO
/****** Object:  Table [dbo].[tb_dealer_promotion]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_dealer_promotion]
GO
/****** Object:  Table [dbo].[tb_dealer_invoice_selected_item]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_dealer_invoice_selected_item]
GO
/****** Object:  Table [dbo].[tb_dealer_invoice_item]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_dealer_invoice_item]
GO
/****** Object:  Table [dbo].[tb_dealer_invoice]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_dealer_invoice]
GO
/****** Object:  Table [dbo].[tb_customer_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_type]
GO
/****** Object:  Table [dbo].[tb_customer_spouse]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_spouse]
GO
/****** Object:  Table [dbo].[tb_customer_phone]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_phone]
GO
/****** Object:  Table [dbo].[tb_customer_person]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_person]
GO
/****** Object:  Table [dbo].[tb_customer_email]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_email]
GO
/****** Object:  Table [dbo].[tb_customer_company]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_company]
GO
/****** Object:  Table [dbo].[tb_customer_category]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_category]
GO
/****** Object:  Table [dbo].[tb_customer_address]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer_address]
GO
/****** Object:  Table [dbo].[tb_customer]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_customer]
GO
/****** Object:  Table [dbo].[tb_cust_debit_note_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cust_debit_note_type]
GO
/****** Object:  Table [dbo].[tb_cust_debit_note_track]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cust_debit_note_track]
GO
/****** Object:  Table [dbo].[tb_cust_debit_note_pay]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cust_debit_note_pay]
GO
/****** Object:  Table [dbo].[tb_cust_debit_note]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cust_debit_note]
GO
/****** Object:  Table [dbo].[tb_cust_credit_note_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cust_credit_note_type]
GO
/****** Object:  Table [dbo].[tb_cust_credit_note]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cust_credit_note]
GO
/****** Object:  Table [dbo].[tb_currency]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_currency]
GO
/****** Object:  Table [dbo].[tb_credit_note_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_credit_note_type]
GO
/****** Object:  Table [dbo].[tb_credit_note]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_credit_note]
GO
/****** Object:  Table [dbo].[tb_country]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_country]
GO
/****** Object:  Table [dbo].[tb_cost_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cost_type]
GO
/****** Object:  Table [dbo].[tb_cost]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cost]
GO
/****** Object:  Table [dbo].[tb_config_field]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_config_field]
GO
/****** Object:  Table [dbo].[tb_company_department]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_company_department]
GO
/****** Object:  Table [dbo].[tb_company_address]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_company_address]
GO
/****** Object:  Table [dbo].[tb_company]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_company]
GO
/****** Object:  Table [dbo].[tb_commercial_colour]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_commercial_colour]
GO
/****** Object:  Table [dbo].[tb_city]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_city]
GO
/****** Object:  Table [dbo].[tb_cheque_type]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cheque_type]
GO
/****** Object:  Table [dbo].[tb_cancel_reason]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_cancel_reason]
GO
/****** Object:  Table [dbo].[tb_buyer_order]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_buyer_order]
GO
/****** Object:  Table [dbo].[tb_bank]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_bank]
GO
/****** Object:  Table [dbo].[tb_allocation_result]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_allocation_result]
GO
/****** Object:  Table [dbo].[tb_allocation]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_allocation]
GO
/****** Object:  Table [dbo].[tb_accessory]    Script Date: 11-Oct-16 10:39:47 AM ******/
DROP TABLE [dbo].[tb_accessory]
GO
