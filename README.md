#UDP - Data Integration Application#
==============================================

The UDP data integration application for pulling data from Genpacx System and Autoline System into UDP database.

#Features Implemented#

##System##
- Require SQL Server database
- Require Java 1.8
- Tomcat 8.0

##Version 1.0 ##

#TODO#
- Build adaptors to pull data from Genpacx System
- Build adaptors to pull data from Autoline System

##Bugs##

##Important##
- Commands to add sql server driver under lib/sqljdbc_6.0/enu folder into the local maven repository

mvn install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc4 -Dversion=6.0 -Dpackaging=jar -DgeneratePom=true -Dfile=sqljdbc4.jar

mvn install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc41 -Dversion=6.0 -Dpackaging=jar -DgeneratePom=true -Dfile=sqljdbc41.jar

mvn install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc42 -Dversion=6.0 -Dpackaging=jar -DgeneratePom=true -Dfile=sqljdbc42.jar

- Add pentaho repository to maven, refer to etc/maven/config/settings.xml. Change Preference->Maven->User Settings to use the modified settings.xml

- Create a new database udp_dev in the SQL Server for UDP

- Run create-all.sql script in the src/main/resources/db/scripts folder to generate the tables for UDP-

#Development Progress#

##[05/11/2016]##
- Added scheduler to run partial data sync every day every hour between 04:00 - 23:00 (5 - 10 min)
- Added scheduler to perform complete sync every day at 01:00 (60 - 120 min)

##[03/11/2016]##
- Split company ref. in the tb_vehicle.location_id to tb_vehicle.company_location_id
- Split company ref. in the tb_delivery.from_location_id to tb_delivery.from_company_location_id
- Split company ref. in the tb_delivery.to_location_id to tb_delivery.to_company_location_id

##[02/11/2016]##
- Update mappings

##[01/09/2016]##
- Integrated SL4J logging
- Added CustomerAdaptor for Genpacx
- Added VehicleAdaptor for Genpacx

##[31/08/2016]##
- Added drop-all.sql and create-all.sql database scripts
- Integrated Flyway for managing database schema migration

##[30/08/2016]##
- Integrated Spring Boot 1.4.0 for implementing business logic components
- Integrated Pentaho DI 6.1 for implementing data integration through ETL
